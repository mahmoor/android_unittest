package com.datasolutions.core.ui.base.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.datasolutions.core.arc.SingleLiveEvent
import com.datasolutions.core.util.NavigationCommand

abstract class BaseViewModel : ViewModel() {

    val loadingEvent = SingleLiveEvent<Boolean>()

    val errorEvent= SingleLiveEvent<String>()

    val navigationCommands = SingleLiveEvent<NavigationCommand>()

    var _navigationEvent = MutableLiveData<NavigationCommand>()
    val navigationEvent: LiveData<NavigationCommand> = _navigationEvent

    fun backButtonClick() {
        navigationCommands.value = NavigationCommand.Back
    }
}