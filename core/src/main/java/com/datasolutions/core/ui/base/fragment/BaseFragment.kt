package com.datasolutions.core.ui.base.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.datasolutions.core.listeners.BackButtonHandlerListener
import com.datasolutions.core.listeners.BackPressListener
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand


import kotlin.reflect.KClass

abstract class BaseFragment<V : ViewModel, B : ViewDataBinding> : Fragment(), BackPressListener {

    lateinit var binding: B

    abstract val viewModelClass: KClass<V>

    abstract val layoutId: Int
    abstract val bindingVariable: Int

    lateinit var viewModel : V

    protected var isBackDisabled: Boolean = false

    private var backButtonHandler: BackButtonHandlerListener? = null

    private var loading: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(bindingVariable, viewModel)
        registerBaseObservers()
        setTitle()
        initViews()
        loading = ProgressDialog(context)
    }

    /**
     * Method to override the back press behaviour on indivitual fragment
     */
    override fun onBackPress(): Boolean {
//        if (!isBackDisabled) {
//            return false
//        }
        return true
    }

    override fun onResume() {
        super.onResume()
       // backButtonHandler?.addBackpressListener(this)
    }

    override fun onPause() {
        super.onPause()
       // backButtonHandler?.removeBackpressListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
//        backButtonHandler = context as BackButtonHandlerListener
    }

    override fun onDetach() {
        super.onDetach()
       // backButtonHandler?.removeBackpressListener(this)
        backButtonHandler = null
    }

    private fun registerBaseObservers() {

        (viewModel as BaseViewModel).loadingEvent.observe(viewLifecycleOwner, {
            loading?.setMessage("Loading....")
            if(it) loading?.show() else loading?.hide()
        })

        (viewModel as BaseViewModel).errorEvent.observe(viewLifecycleOwner, {
            showErrorDialog(it)
        })

        (viewModel as BaseViewModel).navigationCommands.observe(viewLifecycleOwner, {
            when (it) {
                is NavigationCommand.To -> findNavController().navigate(it.direction)
                is NavigationCommand.Back -> findNavController().popBackStack()
                is NavigationCommand.BackTo -> findNavController().popBackStack(
                    it.destinationId,
                    false
                )
            }
        })

        registerOtherObservers()
    }

    abstract fun registerOtherObservers()
    open fun initViews(){}

    private fun showErrorDialog(message: String) {
        val errorDialog =
            AlertDialog.Builder(requireContext()).setMessage(message)
                .setPositiveButton("OK", null)
                .create()
        errorDialog.setCancelable(false)
        errorDialog.show()
    }


    abstract fun viewModel(): V
    open fun setTitle() {}
}