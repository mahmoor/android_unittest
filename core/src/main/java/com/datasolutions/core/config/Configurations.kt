package com.datasolutions.core.config

import com.datasolutions.core.BuildConfig


object Configurations {

    private const val ENV_STG = "staging"
    private const val ENV_PROD = "production"
    //private const val BACK_END_DB = "spaja_penang2"
    public const val BACK_END_DB = "spaja_penang2_api"

    private const val STG_BASE_URL = "http://35.240.200.205:8070/web/"
    private const val PROD_BASE_URL = "prod_base_url"

    val CALL_TIMEOUT: Long = 60
    val CONNECT_TIMEOUT: Long = 60
    val READ_TIMEOUT: Long = 60

    val baseUrl: String
        get() {
            return when (BuildConfig.FLAVOR) {
                ENV_STG -> STG_BASE_URL
                ENV_PROD -> PROD_BASE_URL
                else -> STG_BASE_URL

            }
        }
}