package com.datasolutions.core.interceptor

import com.datasolutions.data.util.SharedPrefHelper
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HeaderInterceptor(val sharedPrefHelper: SharedPrefHelper) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var response: Response?
        var requestBuilder: Request.Builder = chain.request().newBuilder()
        requestBuilder = addCommonHeader(requestBuilder)
        val request = requestBuilder.build()
        try {
            response = chain.proceed(request)
        } catch (ex: Exception) {
            throw ex
        }

       return  response
    }

    private fun addCommonHeader(requestBuilder: Request.Builder): Request.Builder {
        requestBuilder.addHeader("x-openerp", sharedPrefHelper.getToken().orEmpty())
        requestBuilder.addHeader("Cookie", sharedPrefHelper.getToken().orEmpty())
        return requestBuilder
    }
}