package com.datasolutions.core.util

import android.graphics.BitmapFactory
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.datasolutions.core.R
import com.google.android.gms.common.util.Base64Utils

@BindingAdapter("setStyle")
fun setStyle(view: AppCompatEditText, isEdit: Boolean) {
    if (isEdit) {
        view.isEnabled = true
        view.setTextAppearance(R.style.AssetDetailEditTextAppearance)
    } else {
        view.isEnabled = false
        view.setTextAppearance(R.style.AssetDetailGreyTextAppearance)
        view.background = null
    }
}

@BindingAdapter("isEdit", "imageString")
fun setAssetImage(view: AppCompatImageView, isEdit: Boolean, imageString: String?) {

    if (isEdit) {
        if (imageString.isNullOrEmpty() == false) {
            val byteArray = Base64Utils.decode(imageString)
            view.setImageBitmap(
                BitmapFactory.decodeByteArray(
                    byteArray,
                    0,
                    byteArray.size
                )
            )
        } else {
            view.setImageResource(R.drawable.ic_add_photo)
        }
    } else {
        if (imageString.isNullOrEmpty() == false) {
            val byteArray = Base64Utils.decode(imageString)
            view.setImageBitmap(
                BitmapFactory.decodeByteArray(
                    byteArray,
                    0,
                    byteArray.size
                )
            )
        } else {
            view.setImageResource(R.drawable.ic_outline_perm_media_24)
        }
    }
}

@BindingAdapter("isExpanded")
fun setImage(view: AppCompatImageView, isExpanded: Boolean) {

    if (isExpanded) {
        view.setImageResource(R.drawable.ic_arrow_down)
    } else {
        view.setImageResource(R.drawable.ic_arrow_right)
    }
}
