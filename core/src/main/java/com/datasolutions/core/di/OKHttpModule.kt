package com.datasolutions.core.di

import com.datasolutions.core.BuildConfig
import com.datasolutions.core.config.Configurations
import com.datasolutions.core.interceptor.HeaderInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

object OKHttpModule {

    fun load() {
        loadKoinModules(okHttpClient)
    }

    private val okHttpClient = module {
        single {
            val oKhttpModule = OkHttpClient.Builder()
                .addInterceptor(get<HeaderInterceptor>())
                .addInterceptor(get<HttpLoggingInterceptor>())
                .callTimeout(Configurations.CALL_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Configurations.READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Configurations.CONNECT_TIMEOUT, TimeUnit.SECONDS)

            oKhttpModule.build()
        }

        single<HeaderInterceptor> {
            HeaderInterceptor(get())
        }

        single {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG)
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            else
                loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE

            loggingInterceptor
        }
    }
}