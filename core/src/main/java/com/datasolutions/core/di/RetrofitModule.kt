package com.datasolutions.core.di

import com.datasolutions.core.config.Configurations
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitModule {

    fun load() {
        loadKoinModules(retrofitModule)
    }

    private val retrofitModule = module {

        single {
            Retrofit.Builder().client(get())
                .addConverterFactory(gsonConverterFactort())
                .baseUrl(get<String>())
                .build() as Retrofit
        }
        single { Configurations.baseUrl }
    }

    private fun gsonConverterFactort(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }
}