package com.datasolutions.spaja

import android.app.Application
import androidx.core.content.res.FontResourcesParserCompat
import com.datasolutions.spaja.di.ApplicationComponents.loadAllComponents
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

class SpajaApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this);
        startKoin {
            androidContext(this@SpajaApp)
            androidLogger(Level.INFO)
        }

        loadAllComponents()
    }
}