package com.datasolutions.spaja

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController

import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.ui.NavigationUI
import com.datasolutions.core.listeners.BackButtonHandlerListener
import com.datasolutions.core.listeners.BackPressListener
import java.lang.ref.WeakReference
import java.util.ArrayList


class MainActivity : AppCompatActivity() {

//    private lateinit var appBarConfiguration: AppBarConfiguration
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        setContentView(R.layout.activity_main)
//        val toolBar: Toolbar = findViewById(R.id.toolbar)
//        setSupportActionBar(toolBar)
//
//        val navController = findNavController(R.id.nav_host_fragment_content_main)
//        appBarConfiguration = AppBarConfiguration(navController.graph)
//        setupActionBarWithNavController(navController, appBarConfiguration)
//
//    }
//
//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.nav_host_fragment_content_main)
//        return navController.navigateUp(appBarConfiguration)
//                || super.onSupportNavigateUp()
//    }

    private val backClickListenersList = ArrayList<WeakReference<BackPressListener>>()

    private val navController: NavController
        get() = findNavController(R.id.nav_host_fragment_content_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_SpajaPenang_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolBar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        setupActionBarWithNavController(navController)
        supportActionBar?.hide()
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.loginFragment || destination.id == R.id.dashboardFragment) {
               // supportActionBar?.hide()
            } else {
               // supportActionBar?.show()
              //
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    /**
     * Methods which handles the hardware back button / navigation back view
     */
//    override fun onBackPressed() {
//        if (!fragmentsBackKeyIntercept()) {
//            super.onBackPressed()
//        }
//    }

    /**
     * Add the back navigation listener here.
     * Call this method from onAttach of your fragment
     * @param listner - back navigation listener
     */
//    override fun addBackpressListener(listner: BackPressListener) {
//        backClickListenersList.add(WeakReference(listner))
//    }

    /**
     * remove the back navigation listener here.
     * Call this method from onDetach of your fragment
     * @param listner - back navigation listener
     */
//    override fun removeBackpressListener(listner: BackPressListener) {
//        val iterator = backClickListenersList.iterator()
//        while (iterator.hasNext()) {
//            val weakRef = iterator.next()
//            if (weakRef.get() === listner) {
//                iterator.remove()
//            }
//        }
//    }

    /**
     * This method checks if any frgament is overriding the back button behavior or not
     * @return true/false
     */
//    private fun fragmentsBackKeyIntercept(): Boolean {
//        var isIntercept = false
//        for (weakRef in backClickListenersList) {
//            val backpressListner = weakRef.get()
//            if (backpressListner != null) {
//                val isFragmIntercept: Boolean = backpressListner.onBackPress()
//                if (!isIntercept)
//                    isIntercept = isFragmIntercept
//            }
//        }
//        return isIntercept
//    }


}