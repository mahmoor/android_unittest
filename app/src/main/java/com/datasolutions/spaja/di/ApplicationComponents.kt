package com.datasolutions.spaja.di

import com.datasolutions.core.di.OKHttpModule
import com.datasolutions.core.di.RetrofitModule
import com.datasolutions.dashboard.di.DashboardModules
import com.datasolutions.data.di.*
import com.datasolutions.domain.di.UsecaseModules
import com.datasolutions.hdm4.di.Hdm4Modules
import com.datasolutions.maintenancehistory.di.MaintenanceHistoryModules
import com.datasolutions.nearbyasset.di.NearByModules
import com.datasolutions.onboarding.di.OnBoardingModules
import com.datasolutions.pca.di.PcaModules
import com.datasolutions.rais.di.RaiseModules
import com.datasolutions.routelist.di.RouteListModules


object ApplicationComponents {
    fun loadAllComponents() {
        OKHttpModule.load()
        RetrofitModule.load()
        SharedPrefModules.load()
        ApiServiceModule.load()
        RepositoryModule.load()
        UsecaseModules.load()
        CoroutineModule.load()
        OnBoardingModules.load()
        DashboardModules.load()
        RaiseModules.load()
        PcaModules.load()
        Hdm4Modules.load()
        MaintenanceHistoryModules.load()
        RouteListModules.load()
        NearByModules.load()
    }
}