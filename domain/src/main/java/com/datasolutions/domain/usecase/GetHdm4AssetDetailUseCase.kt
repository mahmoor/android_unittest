package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.Hdm4Repository

class GetHdm4AssetDetailUseCase(val hdm4Repository: Hdm4Repository) {

    operator suspend fun invoke(assetListRequest: AssetRequest) =
        hdm4Repository.getHdm4AssetDetail(assetListRequest)

}