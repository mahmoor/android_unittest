package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.Hdm4Repository
import com.datasolutions.domain.repository.MaintenanceHistoryRepository

class GetMaintenanceHistoryDetailUseCase(val maintenanceHistoryRepository: MaintenanceHistoryRepository) {

    operator suspend fun invoke(assetListRequest: AssetRequest) =
        maintenanceHistoryRepository.getMaintenanceHistoryDetail(assetListRequest)

}