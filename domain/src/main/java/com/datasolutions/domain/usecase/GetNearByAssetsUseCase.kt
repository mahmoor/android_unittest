package com.datasolutions.domain.usecase

import com.datasolutions.domain.repository.NearByAssetRepository

class GetNearByAssetsUseCase(val nearByRepository: NearByAssetRepository) {

    operator suspend fun invoke(lat: String, lang: String) =
        nearByRepository.getNearByAsset(lat, lang)

}