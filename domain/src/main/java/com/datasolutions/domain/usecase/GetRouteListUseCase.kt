package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.Hdm4Repository
import com.datasolutions.domain.repository.MaintenanceHistoryRepository
import com.datasolutions.domain.repository.RouteListRepository

class GetRouteListUseCase(val routeListRepository: RouteListRepository) {

    operator suspend fun invoke(assetListRequest: AssetRequest) =
        routeListRepository.getRouteList(assetListRequest)

}