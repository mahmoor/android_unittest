package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository

class GetAssetsUseCase(val assetRepository: AssetRepository) {

    operator suspend fun invoke(assetListRequest: AssetRequest) =
        assetRepository.getAssets(assetListRequest)

    //operator fun invoke(assetListRequest: AssetRequest) = assetRepository.getAssetStream(assetListRequest)
}