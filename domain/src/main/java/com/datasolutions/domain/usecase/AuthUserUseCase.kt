package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.repository.UserRepository

class AuthUserUseCase(val userRepository: UserRepository) {

    operator suspend fun invoke(authRequest: UserAuthRequest) =
        userRepository.authenticateUser(authRequest)
}