package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository

class GetAssetDetailUseCase(val assetRepository: AssetRepository) {

    operator suspend fun invoke(assetListRequest: AssetRequest) =
        assetRepository.getAssetDetail(assetListRequest)
}