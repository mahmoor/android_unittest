package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository

class UpdateAssetUseCase(val assetRepository: AssetRepository) {

    operator suspend fun invoke(assetUpdateRequest: AssetRequest) =
        assetRepository.updateAsset(assetUpdateRequest)
}