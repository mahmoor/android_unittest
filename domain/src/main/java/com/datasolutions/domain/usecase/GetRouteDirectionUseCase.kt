package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.PcaRepository

class GetRouteDirectionUseCase(val pcaRepository: PcaRepository) {

    operator suspend fun invoke(url: String) =
        pcaRepository.getDirections(url)
}