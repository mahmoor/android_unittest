package com.datasolutions.domain.usecase

import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.RoadRepository

class GetRoadsUseCase(val roadRepository: RoadRepository) {

    operator suspend fun invoke(roadRequest: AssetRequest) =
        roadRepository.getRoadList(roadRequest)
}