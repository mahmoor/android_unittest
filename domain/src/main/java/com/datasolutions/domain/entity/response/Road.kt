package com.datasolutions.domain.entity.response

data class Road(val id: Int, val roadId: String, val roadName: String) {
    override fun toString(): String {
        return roadName
    }
}