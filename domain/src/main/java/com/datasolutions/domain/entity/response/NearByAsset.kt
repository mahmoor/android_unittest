package com.datasolutions.domain.entity.response

data class NearByAsset(
    val id: String,
    val type: String? = null,
    val assetId: String? = null,
    val distance: String? = null
)
