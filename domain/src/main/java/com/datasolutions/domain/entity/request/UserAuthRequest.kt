package com.datasolutions.domain.entity.request

data class UserAuthRequest(val params: Params)

data class Params (
    val login: String,
    val password: String,
    val db: String
)
