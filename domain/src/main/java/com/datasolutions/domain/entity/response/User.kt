package com.datasolutions.domain.entity.response

data class User(
    val name: String? = null,
    val partnerDisplayName: String? = null
)
