package com.datasolutions.domain.entity.response

data class PcaAsset(
    var id: Int,
    var assetId: String? = null,
    var roadName: String? = null,
    var roadId: String? = null,
    var routeId: String? = null,
    var district: String? = null,
    var state: String? = null,
    var kmFromAt: String? = null,
    var kmToAt: String? = null,
    var kmAt: String? = null,
    var kmFrom: String? = null,
    var kmTo: String? = null,
    var condition: String? = null,
    var iriAverage: String? = null,
    var image1: String? = null,
    var image2: String? = null,
    var image3: String? = null,
    var image4: String? = null
)
