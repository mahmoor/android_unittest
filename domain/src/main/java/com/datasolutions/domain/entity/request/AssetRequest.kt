package com.datasolutions.domain.entity.request

data class AssetRequest(val params: AssetParams)

data class AssetParams (
    val model: String,
    val method: String,
    val args: List<Any>,
    val kwargs: Any
)

data class KwArgs (
    var limit: Int
)

data class KwArgsUpdateAsset (
    val context: KwArgsUpdateAssetContext = KwArgsUpdateAssetContext()
)

data class KwArgsUpdateAssetContext (
    var through_api: String = "True"
)

class KwDetailArgs (
)
