package com.datasolutions.domain.repository

import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow

interface  UserRepository {

    suspend fun authenticateUser(authRequest: UserAuthRequest): Flow<ResultState<User>>
}