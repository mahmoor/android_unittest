package com.datasolutions.domain.repository

import androidx.paging.PagingData
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.Road
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow

interface Hdm4Repository {

    suspend fun getHdm4Assets(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>>

    suspend fun getHdm4AssetDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>>

}