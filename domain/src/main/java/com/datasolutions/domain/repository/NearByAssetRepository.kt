package com.datasolutions.domain.repository

import androidx.paging.PagingData
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.NearByAsset
import com.datasolutions.domain.entity.response.Road
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow

interface NearByAssetRepository {

    suspend fun getNearByAsset(lat: String, lang: String): Flow<ResultState<List<NearByAsset>>>
}