package com.datasolutions.domain.repository

import androidx.paging.PagingData
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.Road
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow

interface  AssetRepository {

    suspend fun getAssets(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>>

    fun getAssetStream(assetRequest: AssetRequest): Flow<PagingData<Asset>>

    suspend fun getAssetDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>>

    suspend fun updateAsset(assetRequest: AssetRequest): Flow<ResultState<Int>>

    suspend fun addAsset(assetRequest: AssetRequest): Flow<ResultState<Int>>

}