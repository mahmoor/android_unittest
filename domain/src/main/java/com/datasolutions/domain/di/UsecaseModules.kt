package com.datasolutions.domain.di

import com.datasolutions.domain.usecase.*
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object UsecaseModules {

    fun load() {
        loadKoinModules(usecaseModules)
    }

    private val usecaseModules = module {
        factory { AuthUserUseCase(get()) }
        factory { GetAssetDetailUseCase(get()) }
        factory { GetAssetsUseCase(get()) }
        factory { UpdateAssetUseCase(get()) }
        factory { AddAssetUseCase(get()) }
        factory { GetRoadsUseCase(get()) }
        factory { GetPcaAssetsUseCase(get()) }
        factory { GetPcaAssetDetailUseCase(get()) }
        factory { GetRouteDirectionUseCase(get()) }
        factory { GetHdm4AssetsUseCase(get()) }
        factory { GetHdm4AssetDetailUseCase(get()) }
        factory { GetMaintenanceHistoryUseCase(get()) }
        factory { GetMaintenanceHistoryDetailUseCase(get()) }
        factory { GetRouteListUseCase(get()) }
        factory { RouteDetailUseCase(get()) }
        factory { GetNearByAssetsUseCase(get()) }
    }
}