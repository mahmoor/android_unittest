package com.datasolutions.data.datasource.remote.dto.request

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class AssetReqDto(
    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("asset_id")
    var assetId: String? = null,

    @SerializedName("road_name")
    var roadName: String? = null,

    @SerializedName("route_id")
    var routeId: String? = null,

    @SerializedName("road_id")
    var roadId: String? = null,

    @SerializedName("district_id")
    var district: String? = null,

    @SerializedName("state_id")
    var state: String? = null,

    @SerializedName("km_from_at")
    var kmFromAt: Double? = null,

    @SerializedName("km_to_at")
    var kmToAt: Double? = null,

    @SerializedName("km_at")
    var kmAt: Double? = null,

    @SerializedName("km_from")
    var kmFrom: Double? = null,

    @SerializedName("km_to")
    var kmTo: Double? = null,

    @SerializedName("x_coordinate")
    var latitude: Double? = null,

    @SerializedName("x_coordinate_to")
    var latitudeTo: Double? = null,

    @SerializedName("phase")
    var phase: String? = null,

    @SerializedName("offset")
    var offset: Double? = null,

    @SerializedName("length")
    var length: Double? = null,

    @SerializedName("section")
    var section: Int? = null,

    @SerializedName("type")
    var type: String? = null,

    @SerializedName("y_coordinate")
    var longitude: Double? = null,

    @SerializedName("y_coordinate_to")
    var longitudeTo: Double? = null,

    @SerializedName("condition")
    var condition: String? = null,

    @SerializedName("action")
    var action: String? = null,

    @SerializedName("z_altitudee")
    var altitudee: Double? = null,

    @SerializedName("location")
    var location: String? = null,

    @SerializedName("date_install")
    var dateInstall: String? = null,

    @SerializedName("remarks")
    var remarks: String? = null,

    @SerializedName("nos_post")
    var nosPost: String? = null,

    @SerializedName("nos_std_sign")
    var nosStdSign: String? = null,

    @SerializedName("section_from")
    var sectionFrom: String? = null,

    @SerializedName("section_to")
    var sectionTo: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("height")
    var height: String? = null,

    @SerializedName("size")
    var size: String? = null,

    @SerializedName("category")
    var category: String? = null,

    @SerializedName("code")
    var code: String? = null,

    @SerializedName("meas")
    var meas: String? = null,

    @SerializedName("meas_from")
    var measFrom: String? = null,

    @SerializedName("meas_to")
    var measTo: String? = null,

    @SerializedName("events")
    var events: String? = null,

    @SerializedName("diameter")
    var diameter: Double? = null,

    @SerializedName("no_of_cells")
    var noOfCells: String? = null,

    @SerializedName("structure_number")
    var structureNumber: String? = null,

    @SerializedName("bridge_name")
    var bridgeName: String? = null,

    @SerializedName("river")
    var river: String? = null,

    @SerializedName("lane")
    var lane: String? = null,

    @SerializedName("year_build")
    var yearBuild: String? = null,

    @SerializedName("year_repaired")
    var yearRepaired: String? = null,

    @SerializedName("width")
    var width: Double? = null,

    @SerializedName("carriageway_width")
    var carriagewayWidth: Double? = null,

    @SerializedName("span")
    var span: Double? = null,

    @SerializedName("nos_of_span")
    var noOfSpan: Double? = null,

    @SerializedName("type_of_railing")
    var typeOfRailing: String? = null,

    @SerializedName("nos_of_lane")
    var noOfLane: String? = null,

    @SerializedName("direction")
    var direction: String? = null,

    @SerializedName("abutement_type")
    var abutementType: String? = null,

    @SerializedName("system_type")
    var systemType: String? = null,

    @SerializedName("deck_type")
    var deckType: String? = null,

    @SerializedName("pier_type")
    var pierType: String? = null,

    @SerializedName("carriageway")
    var carriageway: String? = null,

    @SerializedName("skew_angle")
    var skewAngle: String? = null,

    @SerializedName("material")
    var material: String? = null,

    @SerializedName("approach_terminal")
    var approachTerminal: String? = null,

    @SerializedName("departing_terminal")
    var departingTerminal: String? = null,

    @SerializedName("ground_clearance")
    var groundClearance: String? = null,

    @SerializedName("no_of_beam")
    var noOfBeam: String? = null,

    @SerializedName("no_of_packer")
    var noOfPacker: String? = null,

    @SerializedName("no_of_post")
    var noOfPost: String? = null,

    @SerializedName("post_spacing")
    var postSpacing: String? = null,

    @SerializedName("rail_heights")
    var railHeights: String? = null,

    @SerializedName("shape")
    var shape: String? = null,

    @SerializedName("braced")
    var braced: String? = null,

    @SerializedName("surface_type")
    var surfaceType: String? = null,

    @SerializedName("image_1")
    var image1: String? = null,

    @SerializedName("image_2")
    var image2: String? = null,

    @SerializedName("image_3")
    var image3: String? = null,

    @SerializedName("image_4")
    var image4: String? = null

)

