package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class GoogleDirectionDto(
    @SerializedName("routes")
    val routes: List<RouteDto>
)

data class RouteDto(
    @SerializedName("legs")
    val legs: List<LegDto>
    )

data class LegDto(
    @SerializedName("steps")
    val steps: List<StepDto>
)

data class StepDto(
    @SerializedName("polyline")
    val polyline: PointDto
    )

data class PointDto(
    @SerializedName("points")
    val points: String
)

