package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName

data class RoadDto(

    val id: Int,
    @SerializedName("road_id")
    val roadId: String? = null,
    @SerializedName("road_name")
    val roadName: String? = null,
)