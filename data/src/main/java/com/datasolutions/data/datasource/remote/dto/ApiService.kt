package com.datasolutions.data.datasource.remote.dto

import com.datasolutions.data.datasource.remote.dto.reponse.*
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @POST("session/authenticate")
    suspend fun authenticateUser(@Body authRequest: UserAuthRequest): Response<BaseResponse<UserDto>>

    @POST("dataset/call_kw")
    suspend fun getAssets(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getAssetDetail(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun updateAsset(@Body assetRequest: AssetRequest): BaseResponse<Int>

    @POST("dataset/call_kw")
    suspend fun addAsset(@Body assetRequest: AssetRequest): BaseResponse<Int>

    @POST("dataset/call_kw")
    suspend fun getRoads(@Body assetRequest: AssetRequest): BaseResponse<List<RoadDto>>

    @POST("dataset/call_kw")
    suspend fun getPcaAssets(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getPcaAssetDetail(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getHdm4Assets(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getHdm4AssetDetail(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getMaintenanceHistory(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getMaintenanceHistoryDetail(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getRouteList(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @POST("dataset/call_kw")
    suspend fun getRouteDetail(@Body assetRequest: AssetRequest): BaseResponse<List<AssetDto>>

    @GET
    suspend fun getNearByAsset(@Url url: String): List<NearByAssetDto>

    @GET
    suspend fun getRoute(@Url direction: String): GoogleDirectionDto

}