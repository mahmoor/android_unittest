package com.datasolutions.data.datasource.remote.dto

data class BaseResponse<T>(val jsonrpc: String, val id: String, val result: T)

