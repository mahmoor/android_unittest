package com.datasolutions.data.datasource.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.datasource.remote.dto.reponse.AssetDto
import com.datasolutions.data.mapper.map
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwArgs
import com.datasolutions.domain.entity.response.Asset
import retrofit2.HttpException
import java.io.IOException

class AssetsPagingSource(private val service: ApiService, private val request: AssetRequest) :
    PagingSource<Int, AssetDto>() {

    override fun getRefreshKey(state: PagingState<Int, AssetDto>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)}
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, AssetDto> {
        val position = params.key ?: 1
        return try {
            (request.params.kwargs as KwArgs).limit = position * 10
            val response = service.getAssets(request)
            val results = response.result
            val nextKey = if (results?.isEmpty() == true) {
                null
            } else {
                // initial load size = 3 * NETWORK_PAGE_SIZE
                // ensure we're not requesting duplicating items, at the 2nd request
                position + (params.loadSize / 10)
            }
            LoadResult.Page(
                data = results!!,
                prevKey = if (position == 1) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}