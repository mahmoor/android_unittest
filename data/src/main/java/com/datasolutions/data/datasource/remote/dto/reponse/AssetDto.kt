package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class AssetDto(
    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("asset_id")
    val assetId: String? = null,

    @SerializedName("road_name")
    val roadName: String? = null,

    @SerializedName("route_id")
    val routeId: List<String>? = null,

//    @SerializedName("road_id")
//    val roadId: List<String>? = null,

    @SerializedName("district_id", alternate = ["region_code_id"])
    val district: List<String>? = null,

    @SerializedName("state_id", alternate = ["state_code_id"])
    val state: List<String>? = null,

    @SerializedName("km_from_at")
    val kmFromAt: Double? = null,

    @SerializedName("km_to_at")
    val kmToAt: Double? = null,

    @SerializedName("km_at")
    val kmAt: Double? = null,

    @SerializedName("km_from")
    val kmFrom: Double? = null,

    @SerializedName("km_to")
    val kmTo: Double? = null,

    @SerializedName("x_coordinate")
    val latitude: Double? = null,

    @SerializedName("x_coordinate_to")
    val latitudeTo: Double? = null,

    @SerializedName("phase")
    val phase: String? = null,

    @SerializedName("offset")
    val offset: Double? = null,

    @SerializedName("length")
    val length: Double? = null,

    @SerializedName("section")
    val section: Int? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("y_coordinate")
    val longitude: Double? = null,

    @SerializedName("y_coordinate_to")
    val longitudeTo: Double? = null,

    @SerializedName("condition")
    val condition: String? = null,

    @SerializedName("action")
    val action: String? = null,

    @SerializedName("z_altitudee")
    val altitudee: Double? = null,

    @SerializedName("location")
    val location: String? = null,

    @SerializedName("date_install")
    val dateInstall: String? = null,

    @SerializedName("remarks")
    val remarks: String? = null,

    @SerializedName("nos_post")
    val nosPost: String? = null,

    @SerializedName("nos_std_sign")
    val nosStdSign: String? = null,

    @SerializedName("section_from")
    val sectionFrom: String? = null,

    @SerializedName("section_to")
    val sectionTo: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("height")
    val height: String? = null,

    @SerializedName("size")
    val size: String? = null,

    @SerializedName("category")
    val category: String? = null,

    @SerializedName("code")
    val code: String? = null,

    @SerializedName("meas")
    val meas: String? = null,

    @SerializedName("meas_from")
    val measFrom: String? = null,

    @SerializedName("meas_to")
    val measTo: String? = null,

    @SerializedName("events")
    val events: String? = null,

    @SerializedName("diameter")
    val diameter: Double? = null,

    @SerializedName("no_of_cells")
    val noOfCells: String? = null,

    @SerializedName("structure_number")
    val structureNumber: String? = null,

    @SerializedName("bridge_name")
    val bridgeName: String? = null,

    @SerializedName("river")
    val river: String? = null,

    @SerializedName("lane")
    val lane: String? = null,

    @SerializedName("year_build")
    val yearBuild: String? = null,

    @SerializedName("year_repaired")
    val yearRepaired: String? = null,

    @SerializedName("width")
    val width: Double? = null,

    @SerializedName("carriageway_width")
    val carriagewayWidth: Double? = null,

    @SerializedName("span")
    val span: Double? = null,

    @SerializedName("nos_of_span")
    val noOfSpan: Double? = null,

    @SerializedName("type_of_railing")
    val typeOfRailing: String? = null,

    @SerializedName("nos_of_lane")
    val noOfLane: String? = null,

    @SerializedName("direction")
    val direction: String? = null,

    @SerializedName("abutement_type")
    val abutementType: String? = null,

    @SerializedName("system_type")
    val systemType: String? = null,

    @SerializedName("deck_type")
    val deckType: String? = null,

    @SerializedName("pier_type")
    val pierType: String? = null,

    @SerializedName("carriageway")
    val carriageway: String? = null,

    @SerializedName("skew_angle")
    val skewAngle: String? = null,

    @SerializedName("material")
    val material: String? = null,

    @SerializedName("approach_terminal")
    val approachTerminal: String? = null,

    @SerializedName("departing_terminal")
    val departingTerminal: String? = null,

    @SerializedName("ground_clearance")
    val groundClearance: String? = null,

    @SerializedName("no_of_beam")
    val noOfBeam: String? = null,

    @SerializedName("no_of_packer")
    val noOfPacker: String? = null,

    @SerializedName("no_of_post")
    val noOfPost: String? = null,

    @SerializedName("post_spacing")
    val postSpacing: String? = null,

    @SerializedName("rail_heights")
    val railHeights: String? = null,

    @SerializedName("shape")
    val shape: String? = null,

    @SerializedName("braced")
    val braced: String? = null,

    @SerializedName("surface_type")
    val surfaceType: String? = null,

    @SerializedName("iri_average")
    val iriAverage: Double? = null,

    @SerializedName("rut_average")
    val rutAverage: Double? = null,

    @SerializedName("bl_thickness_coring")
    val blThicknessCoring: Double? = null,

    @SerializedName("ubl_thickness_dcp")
    val ublThicknessDcp: Double? = null,

    @SerializedName("all_cracks")
    val allCracks: Double? = null,

    @SerializedName("smtd_average")
    val smtdAverage: Double? = null,

    @SerializedName("e1")
    val e1: Double? = null,

    @SerializedName("e2")
    val e2: Double? = null,

    @SerializedName("es")
    val es: Double? = null,

    @SerializedName("agensi_id")
    val agensiId: List<String>? = null,

    @SerializedName("agensi_name_id")
    val agensiNameId: List<String>? = null,

    @SerializedName("iri_left")
    val iriLeft: Double? = null,

    @SerializedName("iri_right")
    val iriRight: Double? = null,

    @SerializedName("iri_lane")
    val iriLane: Double? = null,

    @SerializedName("rut_left")
    val rutLeft: Double? = null,

    @SerializedName("rut_right")
    val rutRight: Double? = null,

    @SerializedName("smtd_left")
    val smtdLeft: Double? = null,

    @SerializedName("smtd_right")
    val smtdRight: Double? = null,

    @SerializedName("crocodile_crack")
    val crocodileCrack: Double? = null,

    @SerializedName("block_crack")
    val blockCrack: Double? = null,

    @SerializedName("longitudinal_crack")
    val longitudinalCrack: Double? = null,

    @SerializedName("tranverse_crack")
    val tranverseCrack: Double? = null,

    @SerializedName("year")
    val year: String? = null,

    @SerializedName("date")
    val date: String? = null,

    @SerializedName("cbr")
    val cbr: Double? = null,

    @SerializedName("d0")
    val d0: Double? = null,

    @SerializedName("bli")
    val bli: Double? = null,

    @SerializedName("mli")
    val mli: Double? = null,

    @SerializedName("lli")
    val lli: Double? = null,

    @SerializedName("road_class")
    val roadClass: String? = null,

    @SerializedName("surface_class")
    val surfaceClass: String? = null,

    @SerializedName("aadt")
    val aadt: Int? = null,

    @SerializedName("npv_cap")
    val npvCap: Double? = null,

    @SerializedName("sub_section")
    val subSection: Double? = null,

    @SerializedName("financial_cost")
    val financialCost: Double? = null,

    @SerializedName("cum_cost")
    val cumCost: Double? = null,

    @SerializedName("type_works")
    val typeWorks: String? = null,

    @SerializedName("project_id")
    val projectId: String? = null,

    @SerializedName("project_name")
    val projectName: String? = null,

    @SerializedName("actual_start_date")
    val startDate: String? = null,

    @SerializedName("actual_complete_date")
    val completeDate: String? = null,

    @SerializedName("actual_section_start")
    val sectionStart: Double? = null,

    @SerializedName("actual_section_end")
    val sectionEnd: Double? = null,

    @SerializedName("chainage_from")
    val chainageFrom: Double? = null,

    @SerializedName("chainage_to")
    val chainageTo: Double? = null,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("marris_id")
    val marrisId: String? = null,

    @SerializedName("image_1")
    val image1: String? = null,

    @SerializedName("image_2")
    val image2: String? = null,

    @SerializedName("image_3")
    val image3: String? = null,

    @SerializedName("image_4")
    val image4: String? = null

)

