package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class PcaAssetDto(
    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("asset_id")
    val assetId: String? = null,

    @SerializedName("road_name")
    val roadName: String? = null,

    @SerializedName("route_id")
    val routeId: List<String>? = null,

    @SerializedName("road_id")
    val roadId: List<String>? = null,

    @SerializedName("district_id")
    val district: List<String>? = null,

    @SerializedName("state_id")
    val state: List<String>? = null,

    @SerializedName("km_from_at")
    val kmFromAt: Double? = null,

    @SerializedName("km_to_at")
    val kmToAt: Double? = null,

    @SerializedName("km_at")
    val kmAt: Double? = null,

    @SerializedName("km_from")
    val kmFrom: Double? = null,

    @SerializedName("km_to")
    val kmTo: Double? = null,

    @SerializedName("x_coordinate")
    val latitude: Double? = null,

    @SerializedName("x_coordinate_to")
    val latitudeTo: Double? = null,

    @SerializedName("phase")
    val phase: String? = null,

    @SerializedName("offset")
    val offset: Double? = null,

    @SerializedName("length")
    val length: Double? = null,

    @SerializedName("section")
    val section: Int? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("y_coordinate")
    val longitude: Double? = null,

    @SerializedName("y_coordinate_to")
    val longitudeTo: Double? = null,

    @SerializedName("condition")
    val condition: String? = null,

    @SerializedName("action")
    val action: String? = null,

    @SerializedName("z_altitudee")
    val altitudee: Double? = null,

    @SerializedName("location")
    val location: String? = null,

    @SerializedName("date_install")
    val dateInstall: String? = null,

    @SerializedName("remarks")
    val remarks: String? = null,

    @SerializedName("nos_post")
    val nosPost: String? = null,

    @SerializedName("nos_std_sign")
    val nosStdSign: String? = null,

    @SerializedName("section_from")
    val sectionFrom: String? = null,

    @SerializedName("section_to")
    val sectionTo: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("height")
    val height: String? = null,

    @SerializedName("size")
    val size: String? = null,

    @SerializedName("category")
    val category: String? = null,

    @SerializedName("code")
    val code: String? = null,

    @SerializedName("meas")
    val meas: String? = null,

    @SerializedName("meas_from")
    val measFrom: String? = null,

    @SerializedName("meas_to")
    val measTo: String? = null,

    @SerializedName("events")
    val events: String? = null,

    @SerializedName("diameter")
    val diameter: Double? = null,

    @SerializedName("no_of_cells")
    val noOfCells: String? = null,

    @SerializedName("structure_number")
    val structureNumber: String? = null,

    @SerializedName("bridge_name")
    val bridgeName: String? = null,

    @SerializedName("river")
    val river: String? = null,

    @SerializedName("lane")
    val lane: String? = null,

    @SerializedName("year_build")
    val yearBuild: String? = null,

    @SerializedName("year_repaired")
    val yearRepaired: String? = null,

    @SerializedName("width")
    val width: Double? = null,

    @SerializedName("carriageway_width")
    val carriagewayWidth: Double? = null,

    @SerializedName("span")
    val span: Double? = null,

    @SerializedName("nos_of_span")
    val noOfSpan: Double? = null,

    @SerializedName("type_of_railing")
    val typeOfRailing: String? = null,

    @SerializedName("nos_of_lane")
    val noOfLane: String? = null,

    @SerializedName("direction")
    val direction: String? = null,

    @SerializedName("abutement_type")
    val abutementType: String? = null,

    @SerializedName("system_type")
    val systemType: String? = null,

    @SerializedName("deck_type")
    val deckType: String? = null,

    @SerializedName("pier_type")
    val pierType: String? = null,

    @SerializedName("carriageway")
    val carriageway: String? = null,

    @SerializedName("skew_angle")
    val skewAngle: String? = null,

    @SerializedName("material")
    val material: String? = null,

    @SerializedName("approach_terminal")
    val approachTerminal: String? = null,

    @SerializedName("departing_terminal")
    val departingTerminal: String? = null,

    @SerializedName("ground_clearance")
    val groundClearance: String? = null,

    @SerializedName("no_of_beam")
    val noOfBeam: String? = null,

    @SerializedName("no_of_packer")
    val noOfPacker: String? = null,

    @SerializedName("no_of_post")
    val noOfPost: String? = null,

    @SerializedName("post_spacing")
    val postSpacing: String? = null,

    @SerializedName("rail_heights")
    val railHeights: String? = null,

    @SerializedName("shape")
    val shape: String? = null,

    @SerializedName("braced")
    val braced: String? = null,

    @SerializedName("surface_type")
    val surfaceType: String? = null,

    @SerializedName("iri_average")
    val iriAverage: Double? = null,

    @SerializedName("image_1")
    val image1: String? = null,

    @SerializedName("image_2")
    val image2: String? = null,

    @SerializedName("image_3")
    val image3: String? = null,

    @SerializedName("image_4")
    val image4: String? = null

)

