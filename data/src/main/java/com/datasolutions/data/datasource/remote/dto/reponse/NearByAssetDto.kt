package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class NearByAssetDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("asset_id")
    val assetId: String? = null,
    @SerializedName("distance")
    val distance: Double? = null
)
