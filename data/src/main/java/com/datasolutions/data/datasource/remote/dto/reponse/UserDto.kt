package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName
import org.koin.core.qualifier.named

data class UserDto(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("partner_display_name")
    val partnerDisplayName: String? = null
)
