package com.datasolutions.data.datasource.remote.dto.reponse

import com.google.gson.annotations.SerializedName

data class ErrorDto(
    @SerializedName("error")
    val error: Error
)

data class Error(
    @SerializedName("code")
    val errorCode: Int,
    @SerializedName("message")
    val errorMessage: String
)
