package com.datasolutions.data.mapper

import com.datasolutions.data.datasource.remote.dto.reponse.*
import com.datasolutions.domain.entity.response.*

fun UserDto.map() = User(
    name = name,
    partnerDisplayName = partnerDisplayName
)

fun List<AssetDto>.map() = this.map {
    Asset(
        id = it.id!!,
        assetId = it.assetId,
        roadName = it.roadName,
        kmFromAt = if (it.kmFromAt == null) it.kmFromAt else it.kmFromAt.toString(),
        kmToAt = if (it.kmToAt == null) it.kmToAt else it.kmToAt.toString(),
        kmFrom = if (it.kmFrom == null) it.kmFrom else it.kmFrom.toString(),
        kmTo = if (it.kmTo == null) it.kmTo else it.kmTo.toString(),
        routeId = it.routeId?.get(1),
        district = it.district?.get(1),
        state = it.state?.get(1),
        kmAt = if (it.kmAt == null) it.kmAt else it.kmAt.toString()
    )
}


fun AssetDto.mapListDtoToDomain() = Asset(
    id = id!!,
    assetId = assetId,
    roadName = roadName,
    kmFromAt = if (kmFromAt == null) kmFromAt else kmFromAt.toString(),
    kmToAt = if (kmToAt == null) kmToAt else kmToAt.toString(),
    kmFrom = if (kmFrom == null) kmFrom else kmFrom.toString(),
    kmTo = if (kmTo == null) kmTo else kmTo.toString(),
    routeId = routeId?.get(1),
    district = district?.get(1),
    state = state?.get(1),
    kmAt = if (kmAt == null) kmAt else kmAt.toString()
)


fun AssetDto.map() = Asset(
    id = id!!,
    assetId = assetId,
    roadName = roadName,
    kmFromAt = if (kmFromAt == null) kmFromAt else kmFromAt.toString(),
    kmToAt = if (kmToAt == null) kmToAt else kmToAt.toString(),
    kmFrom = if (kmFrom == null) kmFrom else kmFrom.toString(),
    kmTo = if (kmTo == null) kmTo else kmTo.toString(),
    routeId = routeId?.get(1),
    district = district?.get(1),
    state = state?.get(1),
    latitude = latitude.toString(),
    latitudeTo = latitudeTo.toString(),
    longitude = longitude.toString(),
    longitudeTo = longitudeTo.toString(),
    latitudeForMap = latitude,
    longitudeForMap = longitude,
    altitude = altitudee.toString(),
    location = performFalseCheck(location),
    event = performFalseCheck(events),
    offSet = offset.toString(),
    type = performFalseCheck(type),
    condition = performFalseCheck(condition),
    dateInstall = performFalseCheck(dateInstall),
    section = section.toString(),
    sectionTo = sectionTo,
    action = performFalseCheck(action),
    nosPost = performFalseCheck(nosPost),
    nosStdSign = performFalseCheck(nosStdSign),
    sectionFrom = sectionFrom,
    description = description,
    height = height,
    length = length.toString(),
    size = performFalseCheck(size),
    category = performFalseCheck(category),
    code = code,
    meas = meas,
    measFrom = measFrom,
    measTo = measTo,
    remarks = remarks,
    diameter = diameter.toString(),
    noOfCells = noOfCells,
    river = river,
    lane = lane,
    direction = direction,
    kmAt = if (kmAt == null) kmAt else kmAt.toString(),
    bridgeName = performFalseCheck(bridgeName),
    structureNumber = performFalseCheck(structureNumber),
    yearBuild = performFalseCheck(yearBuild),
    yearRepaired = performFalseCheck(yearRepaired),
    width = width.toString(),
    carriagewayWidth = carriagewayWidth.toString(),
    span = span.toString(),
    noOfSpan = noOfSpan.toString(),
    typeOfRailing = performFalseCheck(typeOfRailing),
    noOfLane = noOfLane,
    abutementType = performFalseCheck(abutementType),
    systemType = performFalseCheck(systemType),
    deckType = performFalseCheck(deckType),
    pierType = performFalseCheck(pierType),
    carriageway = performFalseCheck(carriageway),
    skewAngle = skewAngle,
    material = performFalseCheck(material),
    approachTerminal = performFalseCheck(approachTerminal),
    departingTerminal = performFalseCheck(departingTerminal),
    groundClearance = performFalseCheck(groundClearance),
    noOfBeam = performFalseCheck(noOfBeam),
    noOfPacker = performFalseCheck(noOfPacker),
    noOfPost = performFalseCheck(noOfPost),
    postSpacing = performFalseCheck(postSpacing),
    railHeights = performFalseCheck(railHeights),
    shape = performFalseCheck(shape),
    braced = performFalseCheck(braced),
    surfaceType = surfaceType,
    chainageFrom = chainageFrom.toString(),
    chainageTo = chainageTo.toString(),
    agensiId = agensiId?.get(1),

    image1 = performFalseCheck(image1),
    image2 = performFalseCheck(image2),
    image3 = performFalseCheck(image3),
    image4 = performFalseCheck(image4)

)

fun List<RoadDto>.mapRoadToEntity() = this.map {
    Road(
        id = it.id,
        roadId = it.roadId.orEmpty(),
        roadName = it.roadName.orEmpty(),
    )
}


/**************** PCA***************/
fun List<AssetDto>.mapPcaToEntity() = this.map {
    Asset(
        id = it.id!!,
        assetId = it.assetId,
        roadName = it.roadName,
        kmFromAt = if (it.kmFromAt == null) it.kmFromAt else it.kmFromAt.toString(),
        kmToAt = if (it.kmToAt == null) it.kmToAt else it.kmToAt.toString(),
        kmFrom = if (it.kmFrom == null) it.kmFrom else it.kmFrom.toString(),
        kmTo = if (it.kmTo == null) it.kmTo else it.kmTo.toString(),
        routeId = it.routeId?.get(1),
        district = it.district?.get(1),
        state = it.state?.get(1),
        kmAt = if (it.kmAt == null) it.kmAt else it.kmAt.toString(),
        iriAverage = it.iriAverage.toString(),
        smtdAverage = it.smtdAverage.toString(),
        condition = it.condition,
        rutAverage = it.rutAverage.toString(),
        allCracks = it.allCracks.toString(),
        blThicknessCoring = it.blThicknessCoring.toString(),
        ublThicknessDcp = it.ublThicknessDcp.toString(),
        e1 = it.e1.toString(),
        e2 = it.e2.toString(),
        es = it.es.toString()

    )
}

fun AssetDto.mapPcaAssetDetail() = Asset(
    id = id!!,
    assetId = assetId,
    roadName = roadName,
    kmFromAt = if (kmFromAt == null) kmFromAt else kmFromAt.toString(),
    kmToAt = if (kmToAt == null) kmToAt else kmToAt.toString(),
    kmFrom = if (kmFrom == null) kmFrom else kmFrom.toString(),
    kmTo = if (kmTo == null) kmTo else kmTo.toString(),
    routeId = routeId?.get(1),
    district = district?.get(1),
    state = state?.get(1),
    latitude = latitude.toString(),
    latitudeTo = latitudeTo.toString(),
    longitude = longitude.toString(),
    longitudeTo = longitudeTo.toString(),
    latitudeForMap = latitude,
    longitudeForMap = longitude,
    altitude = altitudee.toString(),
    location = performFalseCheck(location),
    event = performFalseCheck(events),
    offSet = offset.toString(),
    type = performFalseCheck(type),
    condition = condition,
    dateInstall = performFalseCheck(dateInstall),
    section = section.toString(),
    sectionTo = sectionTo,
    action = performFalseCheck(action),
    nosPost = performFalseCheck(nosPost),
    nosStdSign = performFalseCheck(nosStdSign),
    sectionFrom = sectionFrom,
    description = description,
    height = height,
    length = length.toString(),
    size = performFalseCheck(size),
    category = performFalseCheck(category),
    code = code,
    meas = meas,
    measFrom = measFrom,
    measTo = measTo,
    remarks = remarks,
    diameter = diameter.toString(),
    noOfCells = noOfCells,
    river = river,
    lane = lane,
    direction = direction,
    kmAt = if (kmAt == null) kmAt else kmAt.toString(),
    bridgeName = performFalseCheck(bridgeName),
    structureNumber = performFalseCheck(structureNumber),
    yearBuild = performFalseCheck(yearBuild),
    yearRepaired = performFalseCheck(yearRepaired),
    width = width.toString(),
    carriagewayWidth = carriagewayWidth.toString(),
    span = span.toString(),
    noOfSpan = noOfSpan.toString(),
    typeOfRailing = performFalseCheck(typeOfRailing),
    noOfLane = noOfLane,
    abutementType = performFalseCheck(abutementType),
    systemType = performFalseCheck(systemType),
    deckType = performFalseCheck(deckType),
    pierType = performFalseCheck(pierType),
    carriageway = performFalseCheck(carriageway),
    skewAngle = skewAngle,
    material = performFalseCheck(material),
    approachTerminal = performFalseCheck(approachTerminal),
    departingTerminal = performFalseCheck(departingTerminal),
    groundClearance = performFalseCheck(groundClearance),
    noOfBeam = performFalseCheck(noOfBeam),
    noOfPacker = performFalseCheck(noOfPacker),
    noOfPost = performFalseCheck(noOfPost),
    postSpacing = performFalseCheck(postSpacing),
    railHeights = performFalseCheck(railHeights),
    shape = performFalseCheck(shape),
    braced = performFalseCheck(braced),
    surfaceType = surfaceType,
    agensiId = agensiId?.get(1),
    iriAverage = iriAverage.toString(),
    iriLeft = iriLeft.toString(),
    iriRight = iriRight.toString(),
    iriLane = iriLane.toString(),
    year = performFalseCheck(year),
    rutLeft = rutLeft.toString(),
    rutRight = rutRight.toString(),
    rutAverage = rutAverage.toString(),
    smtdLeft = smtdLeft.toString(),
    smtdRight = smtdRight.toString(),
    allCracks = allCracks.toString(),
    crocodileCrack = crocodileCrack.toString(),
    blockCrack = blockCrack.toString(),
    longitudinalCrack = longitudinalCrack.toString(),
    tranverseCrack = tranverseCrack.toString(),
    cbr = cbr.toString(),
    date = date,
    d0 = d0.toString(),
    mli = mli.toString(),
    bli = bli.toString(),
    lli = lli.toString(),
    blThicknessCoring = blThicknessCoring.toString(),
    ublThicknessDcp = ublThicknessDcp.toString(),
    smtdAverage = smtdAverage.toString(),
    e1 = e1.toString(),
    e2 = e2.toString(),
    es = es.toString(),

)

/**************** HDM4 ***************/
fun List<AssetDto>.mapHdm4ToEntity() = this.map {
    Asset(
        id = it.id!!,
        kmFrom = if (it.kmFrom == null) it.kmFrom else it.kmFrom.toString(),
        kmTo = if (it.kmTo == null) it.kmTo else it.kmTo.toString(),
        routeId = it.routeId?.get(1),
        district = it.district?.get(1),
        roadClass = performFalseCheck(it.roadClass),
        state = it.state?.get(1)

    )
}

fun AssetDto.mapHdm4AssetDetail() = Asset(
    id = id!!,
    assetId = assetId,
    roadName = roadName,
    kmFromAt = if (kmFromAt == null) kmFromAt else kmFromAt.toString(),
    kmToAt = if (kmToAt == null) kmToAt else kmToAt.toString(),
    kmFrom = if (kmFrom == null) kmFrom else kmFrom.toString(),
    kmTo = if (kmTo == null) kmTo else kmTo.toString(),
    routeId = routeId?.get(1),
    district = district?.get(1),
    state = state?.get(1),
    latitude = latitude.toString(),
    latitudeTo = latitudeTo.toString(),
    longitude = longitude.toString(),
    longitudeTo = longitudeTo.toString(),
    latitudeForMap = latitude,
    longitudeForMap = longitude,
    description = description,
    aadt = aadt.toString(),
    npvCap = npvCap.toString(),
    financialCost = financialCost.toString(),
    cumCost = cumCost.toString(),
    roadClass = performFalseCheck(roadClass),
    surfaceClass = performFalseCheck(surfaceClass),
    section = section.toString(),
    subSection = subSection.toString(),
    direction = direction,
    lane = lane,
    year = year,
    agensiId = agensiId?.get(1),
    length = length.toString()

    )


/**************** Maintenance History ***************/
fun List<AssetDto>.mapMaintenanceHistoryToEntity() = this.map {
    Asset(
        id = it.id!!,
        projectName = performFalseCheck(it.projectName),
        projectId = performFalseCheck(it.projectId),
        routeId = it.routeId?.get(1),
        district = it.district?.get(1),
        typeWorks = performFalseCheck(it.typeWorks),
        startDate = it.startDate,
        completeDate = it.completeDate

    )
}

fun AssetDto.mapMaintenanceHistoryDetail() = Asset(
    id = id!!,
    latitude = latitude.toString(),
    latitudeTo = latitudeTo.toString(),
    longitude = longitude.toString(),
    longitudeTo = longitudeTo.toString(),
    latitudeForMap = latitude,
    longitudeForMap = longitude,
    description = description,
    sectionStart = sectionStart.toString(),
    sectionEnd = sectionFrom.toString(),
    phase = performFalseCheck(phase),
    typeWorks = performFalseCheck(typeWorks),
    startDate = startDate,
    completeDate = completeDate,
    routeId = routeId?.get(1),
    district = district?.get(1),
    state = state?.get(1)

)

/**************** Route List ***************/
fun List<AssetDto>.mapRouteListToEntity() = this.map {
    Asset(
        id = it.id!!,
        chainageFrom = it.chainageFrom.toString(),
        chainageTo = it.chainageTo.toString(),
        routeId = it.name,
        district = it.district?.get(1),
        state = it.state?.get(1),
        length = it.length.toString(),
        roadName = performFalseCheck(it.roadName)
    )
}

fun AssetDto.mapRouteListDetail() = Asset(
    id = id!!,
    latitude = latitude.toString(),
    latitudeTo = latitudeTo.toString(),
    longitude = longitude.toString(),
    longitudeTo = longitudeTo.toString(),
    latitudeForMap = latitude,
    longitudeForMap = longitude,
    chainageFrom = chainageFrom.toString(),
    chainageTo = chainageTo.toString(),
    routeId = name,
    district = district?.get(1),
    state = state?.get(1),
    length = length.toString(),
    roadName = performFalseCheck(roadName),
    agensiId = agensiId?.get(1),
    agensiName = agensiNameId?.get(1),
    marrisId = performFalseCheck(marrisId),
    phase = performFalseCheck(phase)

)

/**************** NearBy Asset ***************/
fun List<NearByAssetDto>.mapNearByAsset() = this.map {
    NearByAsset(
        id = it.id,
        type  = it.type,
        assetId  = it.assetId,
        distance  = it.distance.toString()
    )
}

fun performFalseCheck(value: String?): String {
    return if (value == "false" || value == null)
        return ""
    else
        value.toString()
}
