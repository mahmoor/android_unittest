package com.datasolutions.data.util

import android.content.Context
import android.content.SharedPreferences
import com.datasolutions.data.util.PreferenceUtil
import com.datasolutions.data.util.PreferenceUtil.get
import com.datasolutions.data.util.PreferenceUtil.set
import com.datasolutions.data.util.SharedPrefConstants
import com.datasolutions.data.util.SharedPrefConstants.DID_FAVORITE_OFFER
import com.datasolutions.data.util.SharedPrefConstants.IS_USER_LOGGED
import com.datasolutions.data.util.SharedPrefConstants.PREF_USER_NAME
import com.datasolutions.data.util.SharedPrefConstants.WALLET_ONBOARDING_COMPLETED

class SharedPrefHelper(context: Context) {

    private var sharedPreferences: SharedPreferences =
        PreferenceUtil.customPrefs(context, SharedPrefConstants.APP_PREFS)

    fun getToken(): String? {
        return sharedPreferences[SharedPrefConstants.TOKEN]
    }

    fun setToken(token: String?) {
        sharedPreferences[SharedPrefConstants.TOKEN] = token
    }

    fun saveFcmToken(token: String?) {
        sharedPreferences[SharedPrefConstants.PREF_FCM_TOKEN] = token
    }

    fun getFCMToken(): String? {
        return sharedPreferences[SharedPrefConstants.PREF_FCM_TOKEN]
    }

    fun isUserLogged(): Boolean = sharedPreferences[IS_USER_LOGGED] ?: false

    fun setWalletOnBoardingCompleted(completed: Boolean) {
        sharedPreferences[WALLET_ONBOARDING_COMPLETED] = completed
    }

    fun setIsLoggedIn(completed: Boolean) {
        sharedPreferences[IS_USER_LOGGED] = completed
    }

    fun doFavoriteOffer() {
        sharedPreferences[DID_FAVORITE_OFFER] = true
    }

    fun didFavoriteOffer(): Boolean = sharedPreferences[DID_FAVORITE_OFFER] ?: false

    fun isWalletOnBoardingCompleted(): Boolean {
        return sharedPreferences[WALLET_ONBOARDING_COMPLETED] ?: false
    }

    private fun getUserID(): Int {
        return (sharedPreferences[SharedPrefConstants.USER_ID, "0"] ?: "0").toInt()
    }

    fun saveUserDisplayName(name: String?) {
        sharedPreferences[PREF_USER_NAME] = name
    }

    fun getUserDisplayName(): String? {
        return sharedPreferences[PREF_USER_NAME]
    }

}