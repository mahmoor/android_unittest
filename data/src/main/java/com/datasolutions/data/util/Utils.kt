package com.datasolutions.data.util


object Utils {
    public const val GOOGLE_DIRECTION_API_KEY = "AIzaSyDHbG2xE9kpfTrJyp359t6g4kqATkOrKBw"
    fun getDirectionUrl(origin: String, destination: String): String {
        return "https://maps.googleapis.com/maps/api/directions/json?destination=$destination&origin=$origin&key=$GOOGLE_DIRECTION_API_KEY"
    }

    fun getNearByUrl(lat: String, lang: String): String {
        return "http://35.240.200.205:81/spaja_penang/get_coordinate.php?x=$lat&y=$lang"
    }

}