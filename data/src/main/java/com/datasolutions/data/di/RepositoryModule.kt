package com.datasolutions.data.di

import com.datasolutions.data.repository.*
import com.datasolutions.domain.repository.*
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object RepositoryModule {

    fun load() {
        loadKoinModules(repository)
    }

    private val repository = module {
        factory<UserRepository> { AuthUserRepoImpl(get(), get(), get()) }
        factory<RoadRepository> { RoadRepoImpl(get(), get()) }
        factory<AssetRepository> { AssetRepoImpl(get(), get()) }
        factory<PcaRepository> { PcaAssetRepoImpl(get(), get()) }
        factory<Hdm4Repository> { Hdm4AssetRepoImpl(get(), get()) }
        factory<MaintenanceHistoryRepository> { MaintenanceHistoryRepoImpl(get(), get()) }
        factory<RouteListRepository> { RouteListRepoImpl(get(), get()) }
        factory<NearByAssetRepository> { NearByAssetRepoImpl(get(), get()) }

    }
}