package com.datasolutions.data.di

import com.datasolutions.data.util.SharedPrefHelper
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object SharedPrefModules {

    fun load() {
        loadKoinModules(sharePrefModules)
    }

    private val sharePrefModules = module {
        single { SharedPrefHelper(get()) }
    }
}