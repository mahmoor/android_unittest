package com.datasolutions.data.di

import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.coroutine.DefaultCoroutineImpl
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object CoroutineModule {

    fun load() {
        loadKoinModules(coroutineModule)
    }

    private val coroutineModule = module {
        single<DefaultCorotuine> { DefaultCoroutineImpl() }
    }
}