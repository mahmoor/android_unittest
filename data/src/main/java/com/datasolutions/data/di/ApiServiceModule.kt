package com.datasolutions.data.di

import com.datasolutions.data.datasource.remote.dto.ApiService
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

object ApiServiceModule {

    fun load() {
        loadKoinModules(apiService)
    }

    private val apiService = module {

        single { get<Retrofit>().create(ApiService::class.java) }
    }
}