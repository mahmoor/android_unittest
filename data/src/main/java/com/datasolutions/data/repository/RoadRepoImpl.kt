package com.datasolutions.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.paging.AssetsPagingSource
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.map
import com.datasolutions.data.mapper.mapRoadToEntity
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.Road
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.RoadRepository
import com.datasolutions.domain.repository.UserRepository
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import java.sql.ResultSet

class RoadRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    RoadRepository {

    override suspend fun getRoadList(assetRequest: AssetRequest): Flow<ResultState<List<Road>>> = flow {
        emit(ResultState.loading)
        emit( safeApiCall { service.getRoads(assetRequest).result.mapRoadToEntity() })
    }.flowOn(coroutineContext.IO())

}