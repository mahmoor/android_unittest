package com.datasolutions.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.paging.AssetsPagingSource
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.map
import com.datasolutions.data.mapper.mapListDtoToDomain
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.UserRepository
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import java.sql.ResultSet

class AssetRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    AssetRepository {

    override suspend fun getAssets(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>> = flow {
        emit(ResultState.loading)
        emit(safeApiCall {
            service.getAssets(assetRequest).result.map()
        })
    }.flowOn(coroutineContext.IO())

    override fun getAssetStream(assetRequest: AssetRequest): Flow<PagingData<Asset>> {
        return Pager (
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { AssetsPagingSource(service, assetRequest) }
        ).flow.map { pagingData ->
            pagingData.map { it.mapListDtoToDomain() }
        }
    }

    override suspend fun getAssetDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>> = flow {
        emit(ResultState.loading)
        emit( safeApiCall { service.getAssetDetail(assetRequest).result.get(0).map() })
    }.flowOn(coroutineContext.IO())

    override suspend fun updateAsset(assetRequest: AssetRequest): Flow<ResultState<Int>> = flow {
        emit(ResultState.loading)
        emit( safeApiCall { service.updateAsset(assetRequest).result })
    }.flowOn(coroutineContext.IO())

    override suspend fun addAsset(assetRequest: AssetRequest): Flow<ResultState<Int>> = flow {
        emit(ResultState.loading)
        emit( safeApiCall { service.addAsset(assetRequest).result })
    }.flowOn(coroutineContext.IO())


}