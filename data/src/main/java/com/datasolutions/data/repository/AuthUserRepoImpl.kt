package com.datasolutions.data.repository

import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.map
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.data.util.SharedPrefHelper
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.UserRepository
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.sql.ResultSet

class AuthUserRepoImpl(val sharedPref: SharedPrefHelper, val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    UserRepository {

    override suspend fun authenticateUser(authRequest: UserAuthRequest): Flow<ResultState<User>> = flow {
        emit( safeApiCall {
            val response = service.authenticateUser(authRequest)
            val headers = response.headers()
            val sessionId = headers.get("Set-Cookie")?.split(";")?.get(0)
            sharedPref.setToken(sessionId)
            val results = response.body()?.result?.map()
            results!!
            })
    }.flowOn(coroutineContext.IO())

}