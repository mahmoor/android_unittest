package com.datasolutions.data.repository.base

import com.datasolutions.domain.util.ResultState

abstract class BaseRepoImpl {

    protected suspend fun <T : Any> safeApiCall(apiCall: suspend () -> T): ResultState<T> {

        return try {
            val response = apiCall.invoke()
            if (response != null)
                ResultState.Success(response)
            else
                ResultState.Error("Opps, Something went wrong. Please try again")
        } catch (throwable: Throwable) {
            ResultState.Error(handleError(throwable))
        }

    }

    private fun handleError(throwable: Throwable): String {
        return throwable.message.orEmpty()
    }
}