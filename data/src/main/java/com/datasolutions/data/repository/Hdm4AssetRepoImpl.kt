package com.datasolutions.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.paging.AssetsPagingSource
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.*
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.data.util.Utils.getDirectionUrl
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.AssetRepository
import com.datasolutions.domain.repository.Hdm4Repository
import com.datasolutions.domain.repository.PcaRepository
import com.datasolutions.domain.repository.UserRepository
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.*
import java.sql.ResultSet

class Hdm4AssetRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    Hdm4Repository {

    override suspend fun getHdm4Assets(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getHdm4Assets(assetRequest).result.mapHdm4ToEntity(

                )
            })
        }.flowOn(coroutineContext.IO())



    override suspend fun getHdm4AssetDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getHdm4AssetDetail(assetRequest).result.get(0).mapHdm4AssetDetail()
            })
        }.map {
            if (it is ResultState.Success) {
                var polylines = arrayListOf<String>()
                val asset = it.data
                val origin = asset.longitude + ",${asset.latitude}"
                val destination = asset.longitudeTo + ",${asset.latitudeTo}"
                service.getRoute(getDirectionUrl(origin, destination)).routes.map {
                    it.legs.map {
                        it.steps.map {
                            polylines.add(it.polyline.points)
                        }
                    }
                }

                asset.polyLines = polylines
                ResultState.Success(asset).data
            }
            it

        }.flowOn(coroutineContext.IO())


}