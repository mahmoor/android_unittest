package com.datasolutions.data.repository

import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.mapNearByAsset
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.data.util.Utils.getNearByUrl
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.NearByAsset
import com.datasolutions.domain.repository.NearByAssetRepository
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.*

class NearByAssetRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    NearByAssetRepository {

    override suspend fun getNearByAsset(
        lat: String,
        lang: String
    ): Flow<ResultState<List<NearByAsset>>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getNearByAsset(getNearByUrl(lat, lang)).mapNearByAsset()
            })
        }.flowOn(coroutineContext.IO())

}