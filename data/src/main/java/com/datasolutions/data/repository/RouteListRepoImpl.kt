package com.datasolutions.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.paging.AssetsPagingSource
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.*
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.data.util.Utils.getDirectionUrl
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.*
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.*
import java.sql.ResultSet

class RouteListRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    RouteListRepository {


    override suspend fun getRouteList(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getRouteList(assetRequest).result.mapRouteListToEntity(

                )
            })
        }.flowOn(coroutineContext.IO())

    override suspend fun getRouteDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getRouteDetail(assetRequest).result.get(0).mapRouteListDetail()
            })
        }.map {
            if (it is ResultState.Success) {
                var polylines = arrayListOf<String>()
                val asset = it.data
                val origin = asset.latitude + ",${asset.longitudeTo}"
                val destination = asset.latitudeTo + ",${asset.longitudeTo}"
                service.getRoute(getDirectionUrl(origin, destination)).routes.map {
                    it.legs.map {
                        it.steps.map {
                            polylines.add(it.polyline.points)
                        }
                    }
                }

                asset.polyLines = polylines
                ResultState.Success(asset).data
            }
            it

        }.flowOn(coroutineContext.IO())
}