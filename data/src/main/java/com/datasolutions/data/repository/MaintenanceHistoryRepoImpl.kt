package com.datasolutions.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.datasource.paging.AssetsPagingSource
import com.datasolutions.data.datasource.remote.dto.ApiService
import com.datasolutions.data.mapper.*
import com.datasolutions.data.repository.base.BaseRepoImpl
import com.datasolutions.data.util.Utils.getDirectionUrl
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.User
import com.datasolutions.domain.repository.*
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.*
import java.sql.ResultSet

class MaintenanceHistoryRepoImpl(val service: ApiService, val coroutineContext: DefaultCorotuine) :
    BaseRepoImpl(),
    MaintenanceHistoryRepository {


    override suspend fun getMaintenanceHistory(assetRequest: AssetRequest): Flow<ResultState<List<Asset>>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getMaintenanceHistory(assetRequest).result.mapMaintenanceHistoryToEntity(

                )
            })
        }.flowOn(coroutineContext.IO())

    override suspend fun getMaintenanceHistoryDetail(assetRequest: AssetRequest): Flow<ResultState<Asset>> =
        flow {
            emit(ResultState.loading)
            emit(safeApiCall {
                service.getMaintenanceHistoryDetail(assetRequest).result.get(0).mapMaintenanceHistoryDetail()
            })
        }.map {
            if (it is ResultState.Success) {
                var polylines = arrayListOf<String>()
                val asset = it.data
                val origin = asset.longitude + ",${asset.latitude}"
                val destination = asset.longitudeTo + ",${asset.latitudeTo}"
                service.getRoute(getDirectionUrl(origin, destination)).routes.map {
                    it.legs.map {
                        it.steps.map {
                            polylines.add(it.polyline.points)
                        }
                    }
                }

                asset.polyLines = polylines
                ResultState.Success(asset).data
            }
            it

        }.flowOn(coroutineContext.IO())


}