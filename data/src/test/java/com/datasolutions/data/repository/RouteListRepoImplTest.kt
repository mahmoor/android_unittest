package com.datasolutions.data.repository

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class RouteListRepoImplTest {


    @Mock
    lateinit var list: ArrayList<String>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testEmail_Validation() {
        list.add("test")
        `when`(list.size).thenReturn(2)
        assertEquals(1 , list.size)
    }

}