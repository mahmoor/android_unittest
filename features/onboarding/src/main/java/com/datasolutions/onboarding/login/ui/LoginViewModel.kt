package com.datasolutions.onboarding.login.ui

import android.app.Application
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.config.Configurations.BACK_END_DB
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.data.coroutine.DefaultCorotuine
import com.datasolutions.data.util.SharedPrefHelper
import com.datasolutions.domain.entity.request.Params
import com.datasolutions.domain.entity.request.UserAuthRequest
import com.datasolutions.domain.usecase.AuthUserUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.onboarding.R
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class LoginViewModel(val app: Application, val pref: SharedPrefHelper, val authUserUseCase: AuthUserUseCase, val coroutineContext: DefaultCorotuine): BaseViewModel() {

    private val _loginEvent = MutableLiveData<Any>()
    val loginEvent : MutableLiveData<Any> = _loginEvent

    var usernameObservable = ObservableField<String>("")
    var passwordObservable = ObservableField<String>("")

    init {
        if(isUserLoggedIn()) {
            navigationCommands.value = NavigationCommand.To(LoginFragmentDirections.actionLoginToDashboard())
        }
    }
    fun loginClick() {
        login()
    }

    private fun login() {
        if(username().isEmpty() || password().isEmpty()) {
            errorEvent.value = app.getString(R.string.login_invalid_credentials_label)
        } else {
            viewModelScope.launch {
                loadingEvent.value = true
                authUserUseCase(UserAuthRequest(Params(username(), password(), BACK_END_DB))).collect {
                    result ->
                    when (result) {
                        is ResultState.Success -> {
                            loadingEvent.value = false
                            val displayName = result.data.partnerDisplayName
                            //_errorEvent.value = "displayName $displayName"
                            setLoggedIn()
                            saveDisplayName(displayName)
                            navigationCommands.value = NavigationCommand.To(LoginFragmentDirections.actionLoginToDashboard())
                        }

                        is ResultState.Error -> {
                            loadingEvent.value = false
                            errorEvent.value = "Error ${result.message}"
                        }
                    }
                }
            }

        }


    }

    private fun username(): String {
        return usernameObservable.get().orEmpty()
    }

    private fun password(): String {
        return passwordObservable.get().orEmpty()
    }

    private fun setLoggedIn() {
        pref.setIsLoggedIn(true)
    }

    private fun saveDisplayName(name: String?) {
        pref.saveUserDisplayName(name)
    }

    fun isUserLoggedIn(): Boolean {
        return pref.isUserLogged()
    }

}