package com.datasolutions.onboarding.splash.ui

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class SplashViewModel: BaseViewModel() {

    init {
        viewModelScope.launch {
            delay(2000)
            _navigationEvent.value = NavigationCommand.To(SplashFragmentDirections.actionSplashToLogin())
        }
    }
}