package com.datasolutions.onboarding.di

import com.datasolutions.onboarding.login.ui.LoginViewModel
import com.datasolutions.onboarding.splash.ui.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object OnBoardingModules {
    fun load() {
        loadKoinModules(viewModels)
    }

    private val viewModels = module {
        viewModel { SplashViewModel() }
        viewModel { LoginViewModel(get(), get(), get(), get()) }
    }

}