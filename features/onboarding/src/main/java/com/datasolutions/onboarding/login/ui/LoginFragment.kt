package com.datasolutions.onboarding.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.onboarding.R
import com.datasolutions.onboarding.BR
import com.datasolutions.onboarding.databinding.FragmentLoginBinding
import kotlin.reflect.KClass


class LoginFragment : BaseFragment<LoginViewModel, FragmentLoginBinding>() {

    override val layoutId: Int
        get() = R.layout.fragment_login

    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModelClass: KClass<LoginViewModel>
        get() = LoginViewModel::class

    override fun viewModel() = injectViewModel()
    override fun registerOtherObservers() {
    }

}