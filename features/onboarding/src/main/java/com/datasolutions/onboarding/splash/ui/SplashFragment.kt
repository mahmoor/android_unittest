package com.datasolutions.onboarding.splash.ui

import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.onboarding.R
import com.datasolutions.onboarding.BR
import com.datasolutions.onboarding.databinding.FragmentSplashBinding
import kotlin.reflect.KClass


class SplashFragment : BaseFragment<SplashViewModel, FragmentSplashBinding>() {

    override val layoutId: Int
        get() = R.layout.fragment_splash

    override val bindingVariable: Int
        get() = BR.viewModel

    override val viewModelClass: KClass<SplashViewModel>
        get() = SplashViewModel::class

    override fun viewModel() = injectViewModel()
    override fun registerOtherObservers() {
    }
}