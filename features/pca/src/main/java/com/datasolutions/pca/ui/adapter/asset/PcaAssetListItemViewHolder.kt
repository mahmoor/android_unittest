package com.datasolutions.pca.ui.adapter.asset

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.databinding.ViewHolderPcaAssetListItemBinding

class PcaAssetListItemViewHolder(private val binding: ViewHolderPcaAssetListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(model: String, clickListener: PcaAssetListAdapter.OnAssetListItemClickListener? = null, asset: Asset) {
        binding.asset = asset

        binding.kmFromLbl.text = "KM From: "
        binding.kmFromValue.text = asset.kmFrom

        binding.kmToTvLbl.text = "KM To: "
        binding.kmToTvValue.text = asset.kmTo

        when(model) {
            "pca.roughness" -> {
                binding.iriAverageLbl.text = "IRI Average: "
                binding.iriAverageValue.text = asset.iriAverage

                binding.conditionTvLbl.text = "Condition: "
                binding.conditionTvValue.text = asset.condition
            }
            "pca.rutting" -> {
                binding.iriAverageLbl.text = "Rut Average: "
                binding.iriAverageValue.text = asset.rutAverage

                binding.conditionTvLbl.text = "Condition: "
                binding.conditionTvValue.text = asset.condition
            }
            "pca.texture" -> {
                binding.iriAverageLbl.text = "SMTD Average: "
                binding.iriAverageValue.text = asset.smtdAverage

                binding.conditionTvLbl.text = "Condition: "
                binding.conditionTvValue.text = asset.condition
            }
            "pca.cracks" -> {
                binding.iriAverageLbl.text = "All Cracks: "
                binding.iriAverageValue.text = asset.allCracks

                binding.conditionTvLbl.text = "Condition: "
                binding.conditionTvValue.text = asset.condition
            }
            "pca.dcp.coring" -> {
                binding.iriAverageLbl.text = "BL: "
                binding.iriAverageValue.text = asset.blThicknessCoring

                binding.conditionTvLbl.text = "UBL: "
                binding.conditionTvValue.text = asset.ublThicknessDcp
            }
            "pca.emodulus" -> {
                binding.iriAverageLbl.text = "E1: "
                binding.iriAverageValue.text = asset.e1

                val e2 = asset.e2
                val es = asset.es
                binding.conditionTvLbl.text = "E2: "
                binding.conditionTvValue.text = e2
            }
            "pca.axle.load" -> {

            }
        }


        binding.root.setOnClickListener {
            // var id: Int

            clickListener?.showAssetDetail(
             asset.id,
                asset.routeId.orEmpty(),
                asset.roadName.orEmpty(),
                asset.state.orEmpty(),
                asset.district.orEmpty()
            )
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): PcaAssetListItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderPcaAssetListItemBinding.inflate(layoutInflater, parent, false)
            return PcaAssetListItemViewHolder(binding)
        }
    }
}