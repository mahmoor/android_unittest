package com.datasolutions.pca.data

data class PcaDataHolder(
    val id: Int,
    val name: String,
    val model: String
)
