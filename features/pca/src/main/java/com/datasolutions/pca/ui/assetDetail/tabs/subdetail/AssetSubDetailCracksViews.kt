package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailCracksViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder(
                "Agensi",
                asset.agensiId,
                { updatedValue -> asset.agensiId = updatedValue }),
            PcaAssetSubDetailDataObservableHolder(
                "All Cracks",
                asset.allCracks,
                { updatedValue -> asset.allCracks = updatedValue }),
            PcaAssetSubDetailDataObservableHolder(
                "Crocodile Crack",
                asset.crocodileCrack,
                { updatedValue -> asset.crocodileCrack = updatedValue }),
            PcaAssetSubDetailDataObservableHolder(
                "Block Crack",
                asset.blockCrack,
                { updatedValue -> asset.blockCrack = updatedValue }),
            PcaAssetSubDetailDataObservableHolder(
                "Longitudinal Crack",
                asset.longitudinalCrack,
                { updatedValue -> asset.longitudinalCrack = updatedValue }),
            PcaAssetSubDetailDataObservableHolder(
                "Tranverse Crack",
                asset.tranverseCrack,
                { updatedValue -> asset.tranverseCrack = updatedValue }),
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}