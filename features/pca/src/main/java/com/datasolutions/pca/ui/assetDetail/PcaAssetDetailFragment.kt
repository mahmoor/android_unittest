package com.datasolutions.pca.ui.assetDetail

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.pca.R
import com.datasolutions.pca.BR
import com.datasolutions.pca.databinding.FragmentPcaAssetDetailBinding
import com.datasolutions.pca.ui.assetDetail.pager.PcaDetailPagerAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class PcaAssetDetailFragment :
    BaseFragment<PcaAssetDetailViewModel, FragmentPcaAssetDetailBinding>() {

    private val args: PcaAssetDetailFragmentArgs by navArgs()
    private lateinit var adapter: PcaDetailPagerAdapter

    override val viewModelClass: KClass<PcaAssetDetailViewModel>
        get() = PcaAssetDetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_pca_asset_detail
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {

                        val asset = result.data
                        viewModel.setAssetId(asset.assetId, asset.kmFrom, asset.kmTo, asset.sectionFrom, asset.sectionTo)
                        adapter = PcaDetailPagerAdapter(
                            args.model,
                            result.data
                        )
                        if (asset.polyLines?.isNotEmpty() == true) {
                            viewModel.getPoints(asset.polyLines)
                        } else {
                            lifecycleScope.launch {
                                binding.pcaAssetDetailVP.adapter = adapter
                                binding.tabLayout.setupWithViewPager(binding.pcaAssetDetailVP)
                                viewModel.showLoading(false)
                            }
                        }

                        viewModel.showLoading(false)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }

        viewModel.points.observe(viewLifecycleOwner, {
            lifecycleScope.launch {

                adapter.setRoutePoints(it)
                binding.pcaAssetDetailVP.adapter = adapter
                binding.tabLayout.setupWithViewPager(binding.pcaAssetDetailVP)
                viewModel.showLoading(false)
            }
        })
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }
        viewModel.setValues(args.routeId, args.routeName, args.state, args.district)
        viewModel.getAssetDetails(args.id, args.model)
    }
}