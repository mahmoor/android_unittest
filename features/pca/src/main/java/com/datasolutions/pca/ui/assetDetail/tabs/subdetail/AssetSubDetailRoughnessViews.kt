package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailRoughnessViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder("Agensi", asset.agensiId, { updatedValue -> asset.agensiId = updatedValue} ),
            PcaAssetSubDetailDataObservableHolder("IRI Left", asset.iriLeft, { updatedValue -> asset.iriLeft = updatedValue } ),
            PcaAssetSubDetailDataObservableHolder("IRI Right", asset.iriRight, { updatedValue -> asset.iriRight = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("IRI Average", asset.iriAverage, { updatedValue -> asset.iriAverage = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("IRI Lane", asset.iriLane, { updatedValue -> asset.iriLane = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Year", asset.year, { updatedValue -> asset.year = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Condition", asset.condition, { updatedValue -> asset.condition = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}