package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailRuttingViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder("Agensi", asset.agensiId, { updatedValue -> asset.agensiId = updatedValue} ),
            PcaAssetSubDetailDataObservableHolder("RUT Left", asset.rutLeft, { updatedValue -> asset.rutLeft = updatedValue } ),
            PcaAssetSubDetailDataObservableHolder("RUT Right", asset.rutRight, { updatedValue -> asset.rutRight = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("RUT Average", asset.rutAverage, { updatedValue -> asset.rutAverage = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Year", asset.year, { updatedValue -> asset.year = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Condition", asset.condition, { updatedValue -> asset.condition = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}