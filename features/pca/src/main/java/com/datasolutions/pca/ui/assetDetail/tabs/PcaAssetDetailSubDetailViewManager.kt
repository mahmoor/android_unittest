package com.datasolutions.pca.ui.assetDetail.tabs

import android.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.marginTop
import androidx.databinding.ViewDataBinding
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.data.payload.PcaAssetListReqArgs
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.*

class PcaAssetDetailSubDetailViewManager(val isEdit: Boolean, val context: Context, val model: String, val asset: Asset) {

    val inflator = LayoutInflater.from(context)

    fun getDetailViews(): List<View> {
        return if (model == "pca.roughness") {
            AssetSubDetailRoughnessViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.rutting") {
            AssetSubDetailRuttingViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.texture") {
            AssetSubDetailTextureViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.cracks") {
            AssetSubDetailCracksViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.dcp.coring") {
            AssetSubDetailDcpCoringViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.emodulus") {
            AssetSubDetailEmodulusViews(isEdit,false, context, asset).invoke()
        } else if (model == "pca.axle.load") {
           listOf()// AssetSubDetailRoughnessViews(isEdit,false, context, asset).invoke()
        } else listOf()
    }


}