package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.pca.databinding.LayoutPcaAssetSubDetailSubSectionBinding
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder

abstract class AssetSubDetailViewsBase {

    public fun getSubDetailViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForSubDetailViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutPcaAssetSubDetailSubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }


    abstract fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder>
    abstract fun isEditMode(): Boolean
    abstract fun getLayoutInflator(): LayoutInflater
}