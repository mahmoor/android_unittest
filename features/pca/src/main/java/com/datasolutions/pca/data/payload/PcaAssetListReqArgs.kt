package com.datasolutions.pca.data.payload

object PcaAssetListReqArgs {

    fun getArgs(model: String): List<String> {
        return if (model == "pca.roughness") {
            roughness
        } else if (model == "pca.rutting") {
            rutting
        } else if (model == "pca.texture") {
            texture
        } else if (model == "pca.cracks") {
            cracks
        } else if (model == "pca.dcp.coring") {
            dcpCoring
        } else if (model == "pca.emodulus") {
            emodulus
        } else if (model == "pca.axle.load") {
            axleLoad
        } else listOf()
    }

    val roughness = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "iri_average",
        "km_from",
        "km_to",
        "condition"
    )

    val rutting = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "rut_average",
        "km_from",
        "km_to",
        "condition"
    )

    val texture = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "smtd_average",
        "km_from",
        "km_to",
        "condition"
    )

    val cracks = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "km_from",
        "km_to",
        "all_cracks",
        "condition"
    )

    val dcpCoring = mutableListOf(
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "km_from",
        "km_to",
        "bl_thickness_coring",
        "ubl_thickness_dcp"
    )

    val emodulus = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "km_from",
        "km_to",
        "e1",
        "e2",
        "es"
    )

    val axleLoad = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "district_id",
        "state_id",
        "km_from",
        "km_to",
        "condition"
    )
}