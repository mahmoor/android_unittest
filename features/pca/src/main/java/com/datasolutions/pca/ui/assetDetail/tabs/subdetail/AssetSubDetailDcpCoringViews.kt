package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailDcpCoringViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder("Agensi", asset.agensiId, { updatedValue -> asset.agensiId = updatedValue} ),
            PcaAssetSubDetailDataObservableHolder("Date", asset.date, { updatedValue -> asset.date = updatedValue } ),
            PcaAssetSubDetailDataObservableHolder("Year", asset.year, { updatedValue -> asset.year = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("BL Thickness Coring", asset.blThicknessCoring, { updatedValue -> asset.blThicknessCoring = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("UBL Thickness DCP", asset.ublThicknessDcp, { updatedValue -> asset.ublThicknessDcp = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("CBR", asset.cbr, { updatedValue -> asset.cbr = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}