package com.datasolutions.pca.ui

import androidx.lifecycle.LiveData
import com.datasolutions.core.arc.SingleLiveEvent
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.pca.data.PcaDataHolder
import com.datasolutions.pca.data.PcaModelDB

class PcaModelViewModel: BaseViewModel() {

    private val _modlesLiveData = SingleLiveEvent<List<PcaDataHolder>>()
    val pointAssetLiveData: LiveData<List<PcaDataHolder>> = _modlesLiveData

    init {
        initializeModels()
    }

    fun initializeModels() {
        _modlesLiveData.value = PcaModelDB.models
    }

    fun onModelClick(name: String, model: String) {
        navigationCommands.value = NavigationCommand.To(PcaModelFragmentDirections.actionMainToPcaList(name, model))
    }
}