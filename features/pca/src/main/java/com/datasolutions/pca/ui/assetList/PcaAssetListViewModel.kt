package com.datasolutions.pca.ui.assetList

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetPcaAssetsUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.pca.data.payload.PcaAssetListReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class PcaAssetListViewModel(val getPcaAssetsUseCase: GetPcaAssetsUseCase): BaseViewModel() {

    private val _assetsLiveData = MutableStateFlow<ResultState<List<Asset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<Asset>>> = _assetsLiveData

    fun getAssets(model: String) {
        viewModelScope.launch {
            getPcaAssetsUseCase(AssetRequest(AssetParams(model, "search_read", getArgs(model), KwDetailArgs()))).collect {
                    result ->
                _assetsLiveData.value = result
            }
        }
    }

    private fun getArgs(model: String): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, PcaAssetListReqArgs.getArgs(model))
        return list
    }

    fun showAssetDetail(id: Int, model: String, routeId: String, routeName: String, state: String, district: String) {
        navigationCommands.value = NavigationCommand.To(PcaAssetListFragmentDirections.actionListToDetail(
            id, model, routeId, routeName, state, district
        ))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }

}