package com.datasolutions.pca.data

import com.datasolutions.pca.data.payload.PcaAssetListReqArgs


object PcaModelDB {

    val models = listOf(
        PcaDataHolder(1, "Roughness", "pca.roughness"),
        PcaDataHolder(2, "Rutting", "pca.rutting"),
        PcaDataHolder(3, "Texture", "pca.texture"),
        PcaDataHolder(4, "Cracks", "pca.cracks"),
        PcaDataHolder(5, "DCP & Coring", "pca.dcp.coring"),
        PcaDataHolder(6, "E-Modulus", "pca.emodulus"),
        PcaDataHolder(7, "Axle Load", "pca.axle.load"),
    )

}