package com.datasolutions.pca.di

import com.datasolutions.pca.ui.PcaModelViewModel
import com.datasolutions.pca.ui.assetDetail.PcaAssetDetailViewModel
import com.datasolutions.pca.ui.assetList.PcaAssetListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.scope.get
import org.koin.dsl.module

object PcaModules {

    fun load() {
        loadKoinModules(pcaModules)
    }

    private val pcaModules = module {
        viewModel { PcaModelViewModel() }
        viewModel { PcaAssetListViewModel(get()) }
        viewModel { PcaAssetDetailViewModel(get(), get()) }
    }
}