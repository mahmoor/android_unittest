package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailEmodulusViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder("Agensi", asset.agensiId, { updatedValue -> asset.agensiId = updatedValue} ),
            PcaAssetSubDetailDataObservableHolder("E1", asset.e1, { updatedValue -> asset.e1 = updatedValue } ),
            PcaAssetSubDetailDataObservableHolder("E2", asset.e2, { updatedValue -> asset.e2 = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("ES", asset.es, { updatedValue -> asset.es = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("DO", asset.d0, { updatedValue -> asset.d0 = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("BLI", asset.bli, { updatedValue -> asset.bli = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("MLI", asset.mli, { updatedValue -> asset.mli = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("LLI", asset.lli, { updatedValue -> asset.lli = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Year", asset.year, { updatedValue -> asset.year = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}