package com.datasolutions.pca.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.datasolutions.pca.data.PcaDataHolder

class PcaModelAdapter(val clickListener: ModelClickListener): ListAdapter<PcaDataHolder, PcaModelItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<PcaDataHolder>() {
            override fun areItemsTheSame(oldItem: PcaDataHolder, newItem: PcaDataHolder): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PcaDataHolder, newItem: PcaDataHolder): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PcaModelItemViewHolder {
        return PcaModelItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: PcaModelItemViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }

    interface ModelClickListener {
        fun onModelClicked(title: String, model: String)
    }
}