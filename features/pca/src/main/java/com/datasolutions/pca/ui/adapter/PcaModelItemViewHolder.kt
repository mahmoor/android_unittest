package com.datasolutions.pca.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.pca.data.PcaDataHolder
import com.datasolutions.pca.databinding.ViewHolderItemPcaModelBinding


class PcaModelItemViewHolder(private val binding: ViewHolderItemPcaModelBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: PcaModelAdapter.ModelClickListener, model: PcaDataHolder) {
        binding.pcaHolder = model
        binding.root.setOnClickListener {
            clickListener.onModelClicked(model.name, model.model)
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): PcaModelItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderItemPcaModelBinding.inflate(layoutInflater, parent, false)
            return PcaModelItemViewHolder(binding)
        }
    }
}