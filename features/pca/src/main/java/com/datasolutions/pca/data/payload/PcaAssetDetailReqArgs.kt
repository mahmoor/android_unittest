package com.datasolutions.pca.data.payload

object PcaAssetDetailReqArgs {

    fun getArgs(model: String): List<String> {
        return if (model == "pca.roughness") {
            roughness
        } else if (model == "pca.rutting") {
            rutting
        } else if (model == "pca.texture") {
            texture
        } else if (model == "pca.cracks") {
            cracks
        } else if (model == "pca.dcp.coring") {
            dcpCoring
        } else if (model == "pca.emodulus") {
            emodulus
        } else if (model == "pca.axle.load") {
            axleLoad
        } else listOf()
    }

    val roughness = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "km_from",
        "km_to",
        "section_from",
        "section_to",
        "direction",
        "lane",
        "iri_left",
        "iri_right",
        "iri_average",
        "iri_lane",
        "year",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "length",
        "condition",
        "phase"
    )

    val rutting = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "road_name",
        "km_from",
        "km_to",
        "section_from",
        "section_to",
        "direction",
        "lane",
        "rut_left",
        "rut_right",
        "rut_average",
        "year",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "length",
        "phase"
    )

    val texture = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "km_from",
        "km_to",
        "section_from",
        "section_to",
        "direction",
        "lane",
        "smtd_left",
        "smtd_right",
        "smtd_average",
        "year",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "length",
        "phase"
    )

    val cracks = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "km_from",
        "km_to",
        "section_from",
        "section_to",
        "direction",
        "lane",
        "all_cracks",
        "crocodile_crack",
        "block_crack",
        "longitudinal_crack",
        "tranverse_crack",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "length",
        "phase"
    )

    val dcpCoring = mutableListOf(
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "km_from",
        "km_to",
        "direction",
        "lane",
        "date",
        "year",
        "bl_thickness_coring",
        "ubl_thickness_dcp",
        "cbr",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "phase"
    )

    val emodulus = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "road_id",
        "km_from",
        "km_to",
        "section_from",
        "section_to",
        "direction",
        "lane",
        "e1",
        "e2",
        "es",
        "d0",
        "bli",
        "mli",
        "lli",
        "year",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "phase"
    )

    val axleLoad = mutableListOf(
        "asset_id",
        "route_id",
        "road_name",
        "road_id",
        "km_from",
        "km_to",
        "condition"
    )
}