package com.datasolutions.pca.ui.assetDetail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.pca.ui.assetDetail.tabs.subdetail.dataholder.PcaAssetSubDetailDataObservableHolder


class AssetSubDetailTextureViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<PcaAssetSubDetailDataObservableHolder> {

        return listOf(
            PcaAssetSubDetailDataObservableHolder("Agensi", asset.agensiId, { updatedValue -> asset.agensiId = updatedValue} ),
            PcaAssetSubDetailDataObservableHolder("SMTD Left", asset.smtdLeft, { updatedValue -> asset.smtdLeft = updatedValue } ),
            PcaAssetSubDetailDataObservableHolder("SMTD Right", asset.smtdRight, { updatedValue -> asset.smtdRight = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("SMTD Average", asset.smtdAverage, { updatedValue -> asset.smtdAverage = updatedValue }),
            PcaAssetSubDetailDataObservableHolder("Year", asset.year, { updatedValue -> asset.year = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}