package com.datasolutions.pca.ui


import androidx.recyclerview.widget.GridLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.pca.R

import com.datasolutions.pca.databinding.FragmentPcaModelBinding
import com.datasolutions.pca.BR
import com.datasolutions.pca.ui.adapter.PcaModelAdapter
import com.google.android.material.tabs.TabLayout
import kotlin.reflect.KClass

class PcaModelFragment : BaseFragment<PcaModelViewModel, FragmentPcaModelBinding>(),
    PcaModelAdapter.ModelClickListener {

    val adapter: PcaModelAdapter by lazy { PcaModelAdapter(this) }
    override val viewModelClass: KClass<PcaModelViewModel>
        get() = PcaModelViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_pca_model
    override val bindingVariable: Int
        get() = BR.viewModel


    override fun viewModel() = injectViewModel()


    override fun setTitle() {
        binding.title = "PCA"
    }

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        val layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        binding.pcaRecyclerView.layoutManager = layoutManager
        binding.pcaRecyclerView.adapter = adapter

    }


    override fun registerOtherObservers() {
        viewModel.pointAssetLiveData.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })
    }

    override fun onModelClicked(title: String, model: String) {
        viewModel.onModelClick(title, model)
    }
}