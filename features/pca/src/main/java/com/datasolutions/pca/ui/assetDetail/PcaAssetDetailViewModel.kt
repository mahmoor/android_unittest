package com.datasolutions.pca.ui.assetDetail

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwArgsUpdateAsset
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetAssetDetailUseCase
import com.datasolutions.domain.usecase.GetPcaAssetDetailUseCase
import com.datasolutions.domain.usecase.GetRouteDirectionUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.pca.data.payload.PcaAssetDetailReqArgs
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class PcaAssetDetailViewModel(
    val getAssetDetailUseCase: GetPcaAssetDetailUseCase,
    val directionUseCase: GetRouteDirectionUseCase
) : BaseViewModel() {


    private val _points = MutableLiveData<MutableList<List<LatLng>>>()
    val points: LiveData<MutableList<List<LatLng>>> = _points

    private val _assetDetailLiveData = MutableStateFlow<ResultState<Asset>>(ResultState.loading)
    val assetDetailLiveData: StateFlow<ResultState<Asset>> = _assetDetailLiveData


    val routeIdObservable = ObservableField<String>()
    val assetIdObservable = ObservableField<String>()
    val routeNameObservable = ObservableField<String>()
    val stateObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()

    val kmFromObservable = ObservableField<String>()
    val kmToObservable = ObservableField<String>()
    val sectionFromObservable = ObservableField<String>()
    val sectionToObservable = ObservableField<String>()

    fun setValues(
        routeId: String,
        routeName: String,
        state: String,
        district: String
    ) {
        routeIdObservable.set(routeId)
        routeNameObservable.set(routeName)
        stateObservable.set(state)
        districtObservable.set(district)
    }

    fun getAssetDetails(id: Int, model: String) {
        viewModelScope.launch {
            getAssetDetailUseCase(
                AssetRequest(
                    AssetParams(
                        model, "read", getArgs(id, model),
                        KwDetailArgs()
                    )
                )
            ).collect { result ->
                _assetDetailLiveData.value = result
            }
        }
    }


    private fun getArgs(id: Int, model: String): List<Any> {
        val args2 = mutableListOf(id)
        var list: List<Any> = arrayListOf(args2, PcaAssetDetailReqArgs.getArgs(model))
        return list
    }


    fun getPoints(points: List<String>?) {
        viewModelScope.launch {
            _points.value = getRoutePath(points)!!
        }
    }

    suspend  fun getRoutePath(points: List<String>?): MutableList<List<LatLng>> {
        val path: MutableList<List<LatLng>> = ArrayList()
        points?.forEach {
            path.add(PolyUtil.decode(it))
        }

        return path
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }

    fun setAssetId(assetId: String?, kmFrom: String?, kmTo: String?, sectionFrom: String?, sectionTo: String?) {
        assetIdObservable.set(assetId)
        kmFromObservable.set(kmFrom)
        kmToObservable.set(kmTo)
        sectionFromObservable.set(sectionFrom)
        sectionToObservable.set(sectionTo)
    }
}