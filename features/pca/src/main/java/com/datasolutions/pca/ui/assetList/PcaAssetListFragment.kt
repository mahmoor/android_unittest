package com.datasolutions.pca.ui.assetList

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.pca.R
import com.datasolutions.pca.BR
import com.datasolutions.pca.databinding.FragmentPcaAssetListBinding
import com.datasolutions.pca.ui.adapter.asset.PcaAssetListAdapter
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass

class PcaAssetListFragment : BaseFragment<PcaAssetListViewModel, FragmentPcaAssetListBinding>(),
    PcaAssetListAdapter.OnAssetListItemClickListener {

    private val args: PcaAssetListFragmentArgs by navArgs()

    override val viewModelClass: KClass<PcaAssetListViewModel>
        get() = PcaAssetListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_pca_asset_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel() = injectViewModel()

    val adapter: PcaAssetListAdapter by lazy { PcaAssetListAdapter(args.model, this) }

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE

        setupRecyclerView()

        viewModel.getAssets(args.model)
    }

    override fun setTitle() {
        binding.title = args.name
    }

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        adapter.submitList(result.data)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }

            }
        }
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.pcaAssetRecyclerView.layoutManager = layoutManager
        binding.pcaAssetRecyclerView.adapter = adapter

    }

    override fun showAssetDetail(
        id: Int,
        routeId: String,
        routeName: String,
        state: String,
        district: String
    ) {
        viewModel.showAssetDetail(id, args.model, routeId, routeName, state, district)
    }
}