package com.datasolutions.maintenancehistory.ui

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetMaintenanceHistoryUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.maintenancehistory.data.payload.MaintenanceHistoryListReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MaintenanceHistoryListViewModel(private val getMaintenanceHistoryUseCase: GetMaintenanceHistoryUseCase): BaseViewModel() {

    private val _assetsLiveData = MutableStateFlow<ResultState<List<Asset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<Asset>>> = _assetsLiveData

    init {
        getAssets()
    }

    fun getAssets() {
        viewModelScope.launch {
            getMaintenanceHistoryUseCase(AssetRequest(AssetParams("maintenance.history", "search_read", getArgs(), KwDetailArgs()))).collect {
                    result ->
                _assetsLiveData.value = result
            }
        }
    }

    private fun getArgs(): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, MaintenanceHistoryListReqArgs.getArgs())
        return list
    }

    fun showDetail(id: Int, routeId: String, district: String, state: String) {
        navigationCommands.value = NavigationCommand.To(MaintenanceHistoryListFragmentDirections.actionListToDetail(id, "maintenance.history", routeId, state, district))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}