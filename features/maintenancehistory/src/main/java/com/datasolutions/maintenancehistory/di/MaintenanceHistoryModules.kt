package com.datasolutions.maintenancehistory.di

import com.datasolutions.maintenancehistory.ui.MaintenanceHistoryListViewModel
import com.datasolutions.maintenancehistory.ui.detail.MaintenanceHistoryDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object MaintenanceHistoryModules {

    fun load() {
        loadKoinModules(maintenanceHistoryModules)
    }

    private val maintenanceHistoryModules = module {
        viewModel { MaintenanceHistoryDetailViewModel(get()) }
        viewModel { MaintenanceHistoryListViewModel(get()) }
    }
}