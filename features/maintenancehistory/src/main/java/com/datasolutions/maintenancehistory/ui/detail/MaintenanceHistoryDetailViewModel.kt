package com.datasolutions.maintenancehistory.ui.detail

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetMaintenanceHistoryDetailUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.maintenancehistory.data.payload.MaintenanceHistoryDetailReqArgs
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MaintenanceHistoryDetailViewModel(private val getMaintenanceHistoryDetailUseCase: GetMaintenanceHistoryDetailUseCase): BaseViewModel() {

    private val _points = MutableLiveData<MutableList<List<LatLng>>>()
    val points: LiveData<MutableList<List<LatLng>>> = _points

    private val _assetDetailLiveData = MutableStateFlow<ResultState<Asset>>(ResultState.loading)
    val assetDetailLiveData: StateFlow<ResultState<Asset>> = _assetDetailLiveData


    val routeIdObservable = ObservableField<String>()
    val stateObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()

    fun setValues(
        routeId: String,
        state: String,
        district: String
    ) {
        routeIdObservable.set(routeId)
        stateObservable.set(state)
        districtObservable.set(district)
    }

    fun getAssetDetails(id: Int) {
        viewModelScope.launch {
            getMaintenanceHistoryDetailUseCase(
                AssetRequest(
                    AssetParams(
                        "maintenance.history", "read", getArgs(id),
                        KwDetailArgs()
                    )
                )
            ).collect { result ->
                _assetDetailLiveData.value = result
            }
        }
    }


    private fun getArgs(id: Int): List<Any> {
        val args2 = mutableListOf(id)
        var list: List<Any> = arrayListOf(args2, MaintenanceHistoryDetailReqArgs.getArgs())
        return list
    }


    fun getPoints(points: List<String>?) {
        viewModelScope.launch {
            _points.value = getRoutePath(points)!!
        }
    }

    suspend  fun getRoutePath(points: List<String>?): MutableList<List<LatLng>> {
        val path: MutableList<List<LatLng>> = ArrayList()
        points?.forEach {
            path.add(PolyUtil.decode(it))
        }

        return path
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}