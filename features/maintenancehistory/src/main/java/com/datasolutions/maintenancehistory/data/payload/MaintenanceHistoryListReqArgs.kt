package com.datasolutions.maintenancehistory.data.payload

object MaintenanceHistoryListReqArgs {

    fun getArgs(): List<String> {
        return payload
    }

    val payload = mutableListOf(
        "project_id",
        "project_name",
        "state_id",
        "district_id",
        "route_id",
        "route_no",
        "actual_section_start",
        "type_works",
        "actual_complete_date"
    )
}