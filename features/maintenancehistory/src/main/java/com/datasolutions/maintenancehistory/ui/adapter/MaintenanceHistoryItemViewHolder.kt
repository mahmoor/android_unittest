package com.datasolutions.maintenancehistory.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.maintenancehistory.databinding.ViewHolderMaintenanceHistoryListItemBinding


class MaintenanceHistoryItemViewHolder(private val binding: ViewHolderMaintenanceHistoryListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: MaintenanceHistoryListAdapter.OnAssetListItemClickListener? = null, asset: Asset) {
        binding.asset = asset

        binding.root.setOnClickListener {
            // var id: Int

            clickListener?.showAssetDetail(
             asset.id,
                asset.routeId.orEmpty(),
                asset.roadName.orEmpty(),
                asset.state.orEmpty(),
                asset.district.orEmpty()
            )
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): MaintenanceHistoryItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderMaintenanceHistoryListItemBinding.inflate(layoutInflater, parent, false)
            return MaintenanceHistoryItemViewHolder(binding)
        }
    }
}