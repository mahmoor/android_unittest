package com.datasolutions.maintenancehistory.ui

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.maintenancehistory.R
import com.datasolutions.maintenancehistory.BR
import com.datasolutions.maintenancehistory.databinding.FragmentMaintenanceHistoryListBinding
import com.datasolutions.maintenancehistory.ui.adapter.MaintenanceHistoryListAdapter
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass

class MaintenanceHistoryListFragment: BaseFragment<MaintenanceHistoryListViewModel, FragmentMaintenanceHistoryListBinding>(),
    MaintenanceHistoryListAdapter.OnAssetListItemClickListener {


    private val adapter: MaintenanceHistoryListAdapter by lazy { MaintenanceHistoryListAdapter(this) }

    override val viewModelClass: KClass<MaintenanceHistoryListViewModel>
        get() = MaintenanceHistoryListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_maintenance_history_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        if(result.data.isEmpty())
                            viewModel.showError("No record found.")
                        else
                        adapter.submitList(result.data)

                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }

            }
        }
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.historyRecyclerView.layoutManager = layoutManager
        binding.historyRecyclerView.adapter = adapter

    }

    override fun setTitle() {
        binding.title = "Maintenance History"
    }

    override fun showAssetDetail(
        id: Int,
        routeId: String,
        routeName: String,
        state: String,
        district: String
    ) {
        viewModel.showDetail(id, routeId, district, state)
    }

}