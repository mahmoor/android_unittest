package com.datasolutions.maintenancehistory.ui.detail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.maintenancehistory.ui.detail.tabs.subdetail.dataholder.MHSubDetailDataObservableHolder

class MHSubDetail(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : MHSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<MHSubDetailDataObservableHolder> {

        return listOf(
            MHSubDetailDataObservableHolder(
                "Actual Section Start",
                asset.sectionStart,
                { updatedValue -> asset.sectionStart = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Actual Section End",
                asset.sectionEnd,
                { updatedValue -> asset.sectionEnd = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Type Works",
                asset.typeWorks,
                { updatedValue -> asset.typeWorks = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Description",
                asset.description,
                { updatedValue -> asset.description = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Actual Start Date",
                asset.startDate,
                { updatedValue -> asset.startDate = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Actual Complete Date",
                asset.completeDate,
                { updatedValue -> asset.completeDate = updatedValue }),
            MHSubDetailDataObservableHolder(
                "Phase",
                asset.phase,
                { updatedValue -> asset.phase = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}