package com.datasolutions.maintenancehistory.ui.detail.tabs

import android.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.marginTop
import androidx.databinding.ViewDataBinding
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.maintenancehistory.ui.detail.tabs.subdetail.MHSubDetail

class MaintenanceHistoryDetailSubDetailViewManager(val isEdit: Boolean, val context: Context, val model: String, val asset: Asset) {

    val inflator = LayoutInflater.from(context)
    fun getDetailViews(): List<View> {
        return MHSubDetail(isEdit,false, context, asset).invoke()
    }

}