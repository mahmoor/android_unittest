package com.datasolutions.maintenancehistory.ui.detail

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.maintenancehistory.R
import com.datasolutions.maintenancehistory.BR
import com.datasolutions.maintenancehistory.databinding.FragmentMaintenanceHistoryDetailBinding
import com.datasolutions.maintenancehistory.ui.detail.pager.MHDetailPagerAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class MaintenanceHistoryDetailFragment: BaseFragment<MaintenanceHistoryDetailViewModel, FragmentMaintenanceHistoryDetailBinding>() {

    private val args: MaintenanceHistoryDetailFragmentArgs by navArgs()

    private lateinit var adapter: MHDetailPagerAdapter

    override val viewModelClass: KClass<MaintenanceHistoryDetailViewModel>
        get() = MaintenanceHistoryDetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_maintenance_history_detail
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {

                        val asset = result.data

                        adapter = MHDetailPagerAdapter(
                            args.model,
                            result.data
                        )
                        if (asset.polyLines?.isNotEmpty() == true) {
                            viewModel.getPoints(asset.polyLines)
                        } else {
                            lifecycleScope.launch {
                                binding.mhDetailVP.adapter = adapter
                                binding.tabLayout.setupWithViewPager(binding.mhDetailVP)
                                viewModel.showLoading(false)
                            }
                        }

                        viewModel.showLoading(false)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }

        viewModel.points.observe(viewLifecycleOwner, {
            lifecycleScope.launch {

                adapter.setRoutePoints(it)
                binding.mhDetailVP.adapter = adapter
                binding.tabLayout.setupWithViewPager(binding.mhDetailVP)
                viewModel.showLoading(false)
            }
        })
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {

        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        viewModel.setValues(args.routeId, args.state, args.district)

        viewModel.getAssetDetails(args.id)
    }

}