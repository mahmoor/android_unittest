package com.datasolutions.maintenancehistory.ui.detail.tabs.subdetail

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.maintenancehistory.databinding.LayoutMhSubDetailSubSectionBinding
import com.datasolutions.maintenancehistory.ui.detail.tabs.subdetail.dataholder.MHSubDetailDataObservableHolder

abstract class MHSubDetailViewsBase {

    public fun getSubDetailViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForSubDetailViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutMhSubDetailSubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }


    abstract fun getDataForSubDetailViews(): List<MHSubDetailDataObservableHolder>
    abstract fun isEditMode(): Boolean
    abstract fun getLayoutInflator(): LayoutInflater
}