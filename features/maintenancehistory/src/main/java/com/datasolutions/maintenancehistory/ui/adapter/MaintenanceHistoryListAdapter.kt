package com.datasolutions.maintenancehistory.ui.adapter


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.datasolutions.domain.entity.response.Asset

class MaintenanceHistoryListAdapter(val clickListener: OnAssetListItemClickListener): ListAdapter<Asset, MaintenanceHistoryItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<Asset>() {
            override fun areItemsTheSame(oldItem: Asset, newItem: Asset): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Asset, newItem: Asset): Boolean {
               return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaintenanceHistoryItemViewHolder {
        return MaintenanceHistoryItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: MaintenanceHistoryItemViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }

    interface OnAssetListItemClickListener {
        fun showAssetDetail(id: Int, routeId: String, routeName: String, state: String, district: String)
    }
}