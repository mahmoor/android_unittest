package com.datasolutions.maintenancehistory.data.payload

object MaintenanceHistoryDetailReqArgs {

    fun getArgs(): List<String> {
        return detailPayload
    }

    val detailPayload = mutableListOf(
        "project_id",
        "project_name",
        "state_id",
        "district_id",
        "route_id",
        "route_no",
        "actual_section_start",
        "actual_section_end",
        "type_works",
        "description",
        "actual_start_date",
        "actual_complete_date",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "phase"
    )

}