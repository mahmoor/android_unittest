package com.datasolutions.rais.ui.adapter.asset


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.data.AssetDataHolder

class RaisAssetListAdapter(val clickListener: OnAssetListItemClickListener): ListAdapter<Asset, AssetListItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<Asset>() {
            override fun areItemsTheSame(oldItem: Asset, newItem: Asset): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Asset, newItem: Asset): Boolean {
               return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetListItemViewHolder {
        return AssetListItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: AssetListItemViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }

    interface OnAssetListItemClickListener {
        fun showAssetDetail(id: Int, routeId: String, routeName: String, state: String, district: String, width: String, surfaceType: String)
    }
}