package com.datasolutions.rais.ui.assetlist.add

import com.datasolutions.data.datasource.remote.dto.request.AssetReqDto
import com.datasolutions.domain.entity.response.Asset

object AssetAddReqArgs {

    fun getArgs(asset: Asset, model: String): AssetReqDto {
        return if (model == "asset.km.post") {
            kmPost(asset)
        } else if (model == "asset.signboard") {
            signboard(asset)
        } else if (model == "asset.traffic.light") {
            trafficLight(asset)
        } else if (model == "asset.culvert") {
            culvert(asset)
        } else if (model == "asset.traffic.island") {
            trafficIsland(asset)
        } else if (model == "asset.feeder.pillar") {
            feederPillar(asset)
        } else if (model == "asset.street.lighting") {
            streetLighting(asset)
        } else if (model == "asset.junction") {
            junction(asset)
        } else if (model == "asset.road.hump") {
            roadHump(asset)
        } else if (model == "asset.bus.stop") {
            busStop(asset)
        } else if (model == "asset.yellow.box") {
            yellowBox(asset)
        } else if (model == "asset.zebra.crossing") {
            zebraCrossing(asset)
        } else if (model == "asset.bridge") {
            bridge(asset)
        } else if (model == "asset.traffic.safety.barrier") {
            trafficSafteyBarrier(asset)
        } else if (model == "asset.drainage") {
            roadDrainage(asset)
        } else if (model == "asset.road.shoulder") {
            roadShoulder(asset)
        } else if (model == "asset.road.pavement") {
            roadPavement(asset)
        } else if (model == "asset.cycle.lane") {
            cycleLane(asset)
        } else if (model == "asset.transverse.bar") {
            transverseBar(asset)
        } else if (model == "asset.median") {
            median(asset)
        } else  {
            conesSlope(asset)
        }
    }

    private fun kmPost(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        section = asset.section?.toInt(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        location = asset.location,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),
        type = asset.type,
        condition = asset.condition,
        action = asset.action,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun signboard(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        sectionFrom = asset.sectionFrom,
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        location = asset.location,
        meas = asset.meas,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),
        category = asset.category,
        type = asset.type,
        code = asset.code,
        description = asset.description,
        size = asset.size,
        height = asset.height,
        nosPost = asset.nosPost,
        nosStdSign = asset.nosStdSign,
        dateInstall = asset.dateInstall,
        action = asset.action,
        image1 = verifyImage(asset.image1)
    )

    private fun culvert(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        section = asset.section?.toIntOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        location = asset.location,
        meas = asset.meas,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),
        type = asset.type,
        condition = asset.condition,
        size = asset.size,
        diameter = asset.diameter?.toDoubleOrNull(),
        noOfCells = asset.noOfCells,
        dateInstall = asset.dateInstall,
        action = asset.action,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun trafficLight(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun trafficIsland(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun feederPillar(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        location = asset.location,
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun streetLighting(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        location = asset.location,
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun junction(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        type = asset.type,
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun roadHump(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun busStop(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun yellowBox(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )

    private fun zebraCrossing(asset: Asset) = AssetReqDto(
        kmAt = asset.kmAt?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun bridge(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        kmToAt = asset.kmToAt?.toDoubleOrNull(),
        sectionFrom = asset.sectionFrom,
        section = asset.section?.toIntOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        measFrom = asset.measFrom,
        measTo = asset.measTo,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),
        bridgeName = asset.bridgeName,
        structureNumber = asset.structureNumber,
        yearBuild = asset.yearBuild,
        yearRepaired = asset.yearRepaired,
        length = asset.length?.toDoubleOrNull(),
        width = asset.width?.toDoubleOrNull(),
        carriagewayWidth = asset.carriagewayWidth?.toDoubleOrNull(),
        span = asset.span?.toDoubleOrNull(),
        noOfSpan = asset.noOfSpan?.toDoubleOrNull(),
        typeOfRailing = asset.typeOfRailing,
        noOfLane = asset.noOfLane,
        abutementType = asset.abutementType,
        systemType = asset.systemType,
        deckType = asset.deckType,
        pierType = asset.pierType,
        carriageway = asset.carriageway,
        skewAngle = asset.skewAngle,
        condition = asset.condition,
        action = asset.action,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun trafficSafteyBarrier(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        kmToAt = asset.kmToAt?.toDoubleOrNull(),
        sectionFrom = asset.sectionFrom,
        section = asset.section?.toIntOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        measFrom = asset.measFrom,
        measTo = asset.measTo,
        location = asset.location,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),
        type = asset.type,
        length = asset.length?.toDoubleOrNull(),
        material = asset.material,
        approachTerminal = asset.approachTerminal,
        departingTerminal = asset.departingTerminal,
        groundClearance = asset.groundClearance,
        railHeights = asset.railHeights,
        noOfBeam = asset.noOfBeam,
        noOfPacker = asset.noOfPacker,
        noOfPost = asset.noOfPost,
        postSpacing = asset.postSpacing,
        condition = asset.condition,
        dateInstall = asset.dateInstall,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun roadDrainage(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        kmToAt = asset.kmToAt?.toDoubleOrNull(),
        sectionFrom = asset.sectionFrom,
        section = asset.section?.toIntOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        measFrom = asset.measFrom,
        measTo = asset.measTo,
        location = asset.location,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),

        category = asset.category,
        type = asset.type,
        size = asset.size,
        length = asset.length?.toDoubleOrNull(),
        width = asset.width?.toDoubleOrNull(),
        shape = asset.shape,
        braced = asset.braced,
        condition = asset.condition,
        action = asset.action,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun roadShoulder(asset: Asset) = AssetReqDto(
        kmFromAt = asset.kmFromAt?.toDoubleOrNull(),
        kmToAt = asset.kmToAt?.toDoubleOrNull(),
        sectionFrom = asset.sectionFrom,
        section = asset.section?.toIntOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),
        measFrom = asset.measFrom,
        measTo = asset.measTo,
        events = asset.event,
        offset = asset.offSet?.toDoubleOrNull(),

        category = asset.category,
        type = asset.type,
        length = asset.length?.toDoubleOrNull(),
        width = asset.width?.toDoubleOrNull(),
        condition = asset.condition,
        dateInstall = asset.dateInstall,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun roadPavement(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        kmTo = asset.kmTo?.toDoubleOrNull(),
        length = asset.length?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),

        offset = asset.offSet?.toDoubleOrNull(),
        surfaceType = asset.surfaceType,
        width = asset.width?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun cycleLane(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        kmTo = asset.kmTo?.toDoubleOrNull(),
        length = asset.length?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),

        offset = asset.offSet?.toDoubleOrNull(),
        type = asset.type,
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun transverseBar(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        kmTo = asset.kmTo?.toDoubleOrNull(),
        length = asset.length?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),

        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun median(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        kmTo = asset.kmTo?.toDoubleOrNull(),
        length = asset.length?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),

        type = asset.type,
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    private fun conesSlope(asset: Asset) = AssetReqDto(
        kmFrom = asset.kmFrom?.toDoubleOrNull(),
        kmTo = asset.kmTo?.toDoubleOrNull(),
        length = asset.length?.toDoubleOrNull(),
        latitude = asset.latitude?.toDoubleOrNull(),
        longitude = asset.longitude?.toDoubleOrNull(),
        latitudeTo = asset.latitudeTo?.toDoubleOrNull(),
        longitudeTo = asset.longitudeTo?.toDoubleOrNull(),
        altitudee = asset.altitude?.toDoubleOrNull(),

        type = asset.type,
        offset = asset.offSet?.toDoubleOrNull(),
        image1 = verifyImage(asset.image1),
        image2 = verifyImage(asset.image2),
        image3 = verifyImage(asset.image3),
        image4 = verifyImage(asset.image4)
    )


    fun verifyImage(base64: String?): String? {
        if(base64.isNullOrEmpty())
            return null

        return base64
    }
}