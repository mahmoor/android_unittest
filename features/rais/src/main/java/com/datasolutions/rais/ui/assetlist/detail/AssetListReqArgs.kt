package com.datasolutions.rais.ui.assetlist.detail

object AssetListReqArgs {

    fun getArgs(model: String): List<String> {
        return if (model == "asset.km.post") {
            kmPost
        } else if (model == "asset.signboard") {
            signboard
        } else if (model == "asset.traffic.light") {
            trafficLight
        } else if (model == "asset.culvert") {
            culvert
        } else if (model == "asset.traffic.island") {
            trafficIsland
        } else if (model == "asset.feeder.pillar") {
            feederPillar
        } else if (model == "asset.street.lighting") {
            streetLighting
        } else if (model == "asset.junction") {
            junction
        } else if (model == "asset.road.hump") {
            roadHump
        } else if (model == "asset.bus.stop") {
            busStop
        } else if (model == "asset.yellow.box") {
            yellowBox
        } else if (model == "asset.zebra.crossing") {
             zebraCrossing
        } else if (model == "asset.bridge") {
            bridge
        } else if (model == "asset.traffic.safety.barrier") {
            trafficSafteyBarrier
        } else if (model == "asset.drainage") {
            roadDrainage
        } else if (model == "asset.road.shoulder") {
            roadShoulder
        } else if (model == "asset.road.pavement") {
           roadPavement
        } else if (model == "asset.cycle.lane") {
            cycleLane
        } else if (model == "asset.transverse.bar") {
            transverseBar
        } else if (model == "asset.median") {
            median
        } else if (model == "asset.cons.slope") {
            conesSlope
        } else listOf()
    }

    val kmPost = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "road_id"
    )

    val signboard = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "road_id"
    )

    val culvert = mutableListOf(
        "asset_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "lane",
        "km_from_at"
    )

    val trafficLight = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "km_at",
        "road_name"
    )

    val trafficIsland = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "km_at",
        "road_name"
    )

    val feederPillar = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "km_at"
    )

    val streetLighting = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "km_at"
    )

    val junction = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "km_at"
    )

    val roadHump = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "km_at"
    )

    val busStop = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "km_at"
    )

    val yellowBox = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "km_at"
    )

    val zebraCrossing = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "km_at"
     )

    val bridge = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "km_to_at",
        "road_id"
    )

    val trafficSafteyBarrier = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from_at",
        "km_to",
        "road_id"
    )

    val roadDrainage = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from_at",
        "km_to",
        "road_id"
    )

    val roadShoulder = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from_at",
        "km_to",
        "road_id"
    )

    val roadPavement = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from",
        "km_to",
        "road_id"
    )

    val cycleLane = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from",
        "km_to",
        "road_id"
    )

    val transverseBar = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from",
        "km_to",
        "road_id"
    )

    val median = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from",
        "km_to",
        "road_id"
    )

    val conesSlope = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "km_from",
        "km_to",
        "road_id"
    )
}