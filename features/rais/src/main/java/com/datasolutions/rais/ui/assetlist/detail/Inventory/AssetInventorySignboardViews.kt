package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventorySignboardViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    private fun getKmPostDataForViews(): List<AssetInventoryDataObservableHolder> {

        val map = listOf(
            AssetInventoryDataObservableHolder("Km From (at)", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            AssetInventoryDataObservableHolder("Section From", asset.sectionFrom.toString(), { updatedValue -> asset.sectionFrom = updatedValue}),
            AssetInventoryDataObservableHolder("Latitude", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("Longitude", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS", asset.meas.toString(), { updatedValue -> asset.meas = updatedValue}),
            AssetInventoryDataObservableHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue}),
            AssetInventoryDataObservableHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )

        return map
    }


    private fun getKmPostStructureDataForViews(): List<AssetInventoryDataObservableHolder> {
        val map = listOf(
            AssetInventoryDataObservableHolder("Category", asset.category.toString(), { updatedValue -> asset.category = updatedValue}),
            AssetInventoryDataObservableHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            AssetInventoryDataObservableHolder("Code", asset.code.toString(), { updatedValue -> asset.code = updatedValue}),
            AssetInventoryDataObservableHolder("Description", asset.description.toString(), { updatedValue -> asset.description = updatedValue}),
            AssetInventoryDataObservableHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            AssetInventoryDataObservableHolder("Bottom Height", asset.height.toString(), { updatedValue -> asset.height = updatedValue}),
            AssetInventoryDataObservableHolder("NOS Post", asset.nosPost.toString(), { updatedValue -> asset.nosPost = updatedValue}),
            AssetInventoryDataObservableHolder("NOS Std Sign", asset.nosStdSign.toString(), { updatedValue -> asset.nosStdSign = updatedValue}),
            AssetInventoryDataObservableHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            AssetInventoryDataObservableHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
        return map
    }

    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return getKmPostDataForViews()
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return getKmPostStructureDataForViews()
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}