package com.datasolutions.rais.di

import androidx.lifecycle.SavedStateHandle
import com.datasolutions.rais.ui.RaiseMainViewModel
import com.datasolutions.rais.ui.assetlist.AssetListViewModel
import com.datasolutions.rais.ui.assetlist.add.AddAssetViewModel
import com.datasolutions.rais.ui.assetlist.detail.AssetDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object RaiseModules {

    fun load() {
        loadKoinModules(raisModules)
    }

    private val raisModules = module {
        viewModel { RaiseMainViewModel(get()) }
        viewModel { AssetListViewModel(get()) }
        viewModel { AssetDetailViewModel(get(), get()) }
        viewModel { AddAssetViewModel(get(), get()) }
       // factory { SavedStateHandle() }
    }
}