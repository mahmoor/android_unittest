package com.datasolutions.rais.ui.assetlist

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.rais.R
import com.datasolutions.rais.BR
import com.datasolutions.rais.databinding.FragmentAssetListBinding
import com.datasolutions.rais.ui.adapter.asset.RaisAssetListAdapter
import com.datasolutions.rais.ui.adapter.paging.AssetPagingAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class AssetListFragment : BaseFragment<AssetListViewModel, FragmentAssetListBinding>(),
    RaisAssetListAdapter.OnAssetListItemClickListener {
    override val viewModelClass: KClass<AssetListViewModel>
        get() = AssetListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_asset_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel() = injectViewModel()

    private val adapter by lazy { RaisAssetListAdapter(this) }

    private val adapterPaging by lazy { AssetPagingAdapter() }

    private val args: AssetListFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.fab.setOnClickListener {
            viewModel.addAsset(args.model)
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE
        setupRecyclerView()
        viewModel.getAssets(args.model)
    }

    override fun registerOtherObservers() {
        registerObservers()
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.raisAssetRecyclerView.layoutManager = layoutManager
        binding.raisAssetRecyclerView.adapter = adapter

    }

    private fun registerObservers() {
        lifecycleScope.launchWhenCreated {

            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        adapter.submitList(result.data)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }


        }

//        viewLifecycleOwner.lifecycleScope.launch {
//            viewModel.getAssetsWithPaging(args.model).collectLatest {
//                adapterPaging.submitData(it)
//            }
//        }
    }

    override fun showAssetDetail(
        id: Int,
        routeId: String,
        routeName: String,
        state: String,
        district: String,
        width: String,
        surfaceType: String
    ) {
        viewModel.showAssetDetail(
            args.model,
            id,
            routeId,
            routeName,
            state,
            district,
            width,
            surfaceType,
            args.isLinear
        )
    }

    override fun setTitle() {
        binding.title = args.name
    }

}