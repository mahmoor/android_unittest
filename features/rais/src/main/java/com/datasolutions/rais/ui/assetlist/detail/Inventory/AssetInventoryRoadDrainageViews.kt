package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryRoadDrainageViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Km From (at)", asset.kmFromAt.toString()),
            AssetInventoryDataObservableHolder("KM To (at)", asset.kmTo.toString()),
            AssetInventoryDataObservableHolder("Section From", asset.sectionFrom.toString()),
            AssetInventoryDataObservableHolder("Section To", asset.sectionTo.toString()),
            AssetInventoryDataObservableHolder("X-Coordinate From", asset.latitude.toString()),
            AssetInventoryDataObservableHolder("Y-Coordinate From", asset.longitude.toString()),
            AssetInventoryDataObservableHolder("X-Coordinate To", asset.latitudeTo.toString()),
            AssetInventoryDataObservableHolder("Y-Coordinate To", asset.longitudeTo.toString()),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString()),
            AssetInventoryDataObservableHolder("MEAS From", asset.measFrom.toString()),
            AssetInventoryDataObservableHolder("MEAS To", asset.measTo.toString()),
            AssetInventoryDataObservableHolder("Location", asset.location.toString()),
            AssetInventoryDataObservableHolder("Events", asset.event.toString()),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString())
        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Category", asset.category.toString(), { updatedValue -> asset.category = updatedValue}),
            AssetInventoryDataObservableHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            AssetInventoryDataObservableHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            AssetInventoryDataObservableHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            AssetInventoryDataObservableHolder("Width", asset.width.toString(), { updatedValue -> asset.width = updatedValue}),
            AssetInventoryDataObservableHolder("Shape", asset.shape.toString(), { updatedValue -> asset.shape = updatedValue}),
            AssetInventoryDataObservableHolder("Braced", asset.braced.toString(), { updatedValue -> asset.braced = updatedValue}),
            AssetInventoryDataObservableHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            AssetInventoryDataObservableHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}