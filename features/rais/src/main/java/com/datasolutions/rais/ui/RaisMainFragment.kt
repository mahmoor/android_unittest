package com.datasolutions.rais.ui

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.rais.R
import com.datasolutions.rais.BR
import com.datasolutions.rais.databinding.FragmentRaisMainBinding
import com.datasolutions.rais.ui.adapter.RaisAssetAdapter
import com.google.android.material.tabs.TabLayout
import org.koin.androidx.viewmodel.ext.android.stateViewModel
import kotlin.reflect.KClass

class RaisMainFragment : BaseFragment<RaiseMainViewModel, FragmentRaisMainBinding>(),
    RaisAssetAdapter.OnAssetClickListener {
    override val viewModelClass: KClass<RaiseMainViewModel>
        get() = RaiseMainViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_rais_main
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel(): RaiseMainViewModel {
        val vm: RaiseMainViewModel by viewModels()
        return vm
    }

    private val adapter by lazy { RaisAssetAdapter(this) }

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        setupRecyclerView()

    }

    override fun  registerOtherObservers() {
        registerObserver()
    }

    private fun setupRecyclerView() {

        if(viewModel.isLinearSelected() == true) {
            binding.tabLayout.getTabAt(1)?.select()
        } else {
            binding.tabLayout.getTabAt(0)?.select()
        }

        val layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        binding.raisRecyclerView.layoutManager = layoutManager
        binding.raisRecyclerView.adapter = adapter

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 0) {
                    viewModel.getPointAssets()
                } else {
                    viewModel.getLinearAssets()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })


    }

    private fun registerObserver() {
        viewModel.linearAssetLiveData.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })

        viewModel.pointAssetLiveData.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })
    }

    override fun onAssetClicked(title: String, model: String, isLinear: Boolean) {
        viewModel.saveViewState(isLinear)
        if (model.isNotEmpty())
            viewModel.onAssetClickes(title, model, isLinear)
        else
            viewModel.errorEvent.value = "Still in progress!!!"
    }

    override fun setTitle() {
       binding.title = "Rais"
    }

}