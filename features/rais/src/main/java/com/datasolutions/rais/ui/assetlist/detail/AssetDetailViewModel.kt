package com.datasolutions.rais.ui.assetlist.detail

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.*
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetAssetDetailUseCase
import com.datasolutions.domain.usecase.UpdateAssetUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AssetDetailViewModel(
    val getAssetDetailUseCase: GetAssetDetailUseCase,
    val updateAssetUseCase: UpdateAssetUseCase
) : BaseViewModel() {
    val routeIdObservable = ObservableField<String>()
    val assetIdObservable = ObservableField<String>()
    val routeNameObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()
    val startObservable = ObservableField<String>()
    val endObservable = ObservableField<String>()
    val lengthObservable = ObservableField<String>()
    val laneObservable = ObservableField<String>()
    val directionObservable = ObservableField<String>()
    val agensiIdObservable = ObservableField<String>()

    private val _assetDetailLiveData = MutableStateFlow<ResultState<Asset>>(ResultState.loading)
    val assetDetailLiveData: StateFlow<ResultState<Asset>> = _assetDetailLiveData


    private val _assetUpdateLiveData = MutableStateFlow<ResultState<Int>>(ResultState.loading)
    val assetUpdateLiveData: StateFlow<ResultState<Int>> = _assetUpdateLiveData

    fun setValues(
        assetId: String?,
        routeId: String,
        routeName: String,
        district: String,
        start: String?,
        end: String?,
        length: String?,
        agensiId: String?,
        direction: String?,
        lane: String?
    ) {
        assetIdObservable.set(assetId)
        routeIdObservable.set(routeId)
        routeNameObservable.set(routeName)
        districtObservable.set(district)
        startObservable.set(start)
        endObservable.set(end)
        lengthObservable.set(length)
        directionObservable.set(direction)
        laneObservable.set(lane)
        agensiIdObservable.set(agensiId)
    }

    fun getAssetDetails(id: Int, model: String, isLinear: Boolean) {
        viewModelScope.launch {
            getAssetDetailUseCase(
                AssetRequest(
                    AssetParams(
                        model, "read", getArgs(id, model),
                        KwDetailArgs()
                    )
                )
            ).collect { result ->
                _assetDetailLiveData.value = result
            }
        }
    }

    fun updateAsset(asset: Asset, model: String) {

        viewModelScope.launch {

            val request = AssetRequest(
                AssetParams(
                    model, "update_basedon_asset_id", getUpdateArgs(asset, model),
                    KwArgsUpdateAsset()
                )
            )

            updateAssetUseCase(request).collect {
                _assetUpdateLiveData.value = it
            }
        }
    }

    private fun getArgs(id: Int, model: String): List<Any> {
        val args2 = mutableListOf(id)
        var list: List<Any> = arrayListOf(args2, AssetDetailReqArgs.getArgs(model))
        return list
    }

    private fun getUpdateArgs(asset: Asset, model: String): List<Any> {
        val idList = listOf(
            "asset_id", "=", asset.assetId
        )
        val args2 = mutableListOf(idList)
        var list: List<Any> = arrayListOf(args2, AssetUpdateReqArgs.getArgs(asset, model))
        return list
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }


}