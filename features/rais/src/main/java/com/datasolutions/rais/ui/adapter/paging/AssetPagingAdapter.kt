package com.datasolutions.rais.ui.adapter.paging

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.ui.adapter.asset.AssetListItemViewHolder

class AssetPagingAdapter: PagingDataAdapter<Asset, AssetListItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<Asset>() {
            override fun areItemsTheSame(oldItem: Asset, newItem: Asset): Boolean {
                return oldItem.assetId == newItem.assetId
            }

            override fun areContentsTheSame(oldItem: Asset, newItem: Asset): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetListItemViewHolder {
        return AssetListItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: AssetListItemViewHolder, position: Int) {
        holder.bind(null, getItem(position)!!)
    }
}