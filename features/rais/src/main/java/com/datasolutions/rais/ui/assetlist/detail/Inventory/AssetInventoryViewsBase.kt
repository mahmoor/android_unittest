package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

abstract class AssetInventoryViewsBase {

    public fun getInventoryLocationViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForLocationViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutAssetDetailInventorySubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }



    public fun getInventoryStructureViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForStructureViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutAssetDetailInventorySubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }

    abstract fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder>
    abstract fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder>
    abstract fun isEditMode(): Boolean
    abstract fun getLayoutInflator(): LayoutInflater
}