package com.datasolutions.rais.ui.assetlist.add.road

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.Filter.FilterResults
import com.datasolutions.domain.entity.response.Road
import java.util.ArrayList

class RoadListAdapter(private val context: Context, private val dataList: List<Road>) :
    BaseAdapter()  {
    override fun getCount(): Int {
        return dataList.size
    }

    override fun getItem(position: Int): Road {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = getItem(position).roadName
        return view
    }

}