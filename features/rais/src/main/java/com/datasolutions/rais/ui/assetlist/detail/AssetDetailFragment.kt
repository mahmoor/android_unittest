package com.datasolutions.rais.ui.assetlist.detail

import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.util.ResultState
import com.datasolutions.rais.R
import com.datasolutions.rais.BR
import com.datasolutions.rais.databinding.FragmentAssetDetailBinding
import com.datasolutions.rais.ui.assetlist.detail.pager.DetailPagerAdapter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass
import androidx.core.app.ActivityCompat.startActivityForResult

import `in`.mayanknagwanshi.imagepicker.ImageSelectActivity
import android.app.Activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.common.util.Base64Utils
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream


class AssetDetailFragment : BaseFragment<AssetDetailViewModel, FragmentAssetDetailBinding>() {

    override val viewModelClass: KClass<AssetDetailViewModel>
        get() = AssetDetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_asset_detail
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel() = injectViewModel()

    private val args: AssetDetailFragmentArgs by navArgs()

    private lateinit var adapter: DetailPagerAdapter
    private lateinit var activityForResult: ActivityResultLauncher<Intent>
    private var attachImageNo: Int = 0
    private val TAG_EDIT_ENABLED = "edit_enabled"
    private val TAG_EDIT_DISABLED = "edit_disabled"

    override fun initViews() {

        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.apply {
            setImageResource(R.drawable.ic_cross)
            setOnClickListener {
                val adapter = binding.assetDetailVP.adapter as DetailPagerAdapter
                adapter.setEdit(false)
                binding.assetDetailVP.adapter = adapter
                visibility = View.GONE
                binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_edit)
                binding.includeMain.actionBtn2.tag = TAG_EDIT_DISABLED
                viewModel.getAssetDetails(args.id, args.model, args.isLinear)
            }
        }

        binding.includeMain.actionBtn2.apply {
            visibility = View.VISIBLE
            tag = TAG_EDIT_DISABLED
            setImageResource(R.drawable.ic_edit)
            setOnClickListener {

                val adapter = binding.assetDetailVP.adapter as DetailPagerAdapter
                if (TAG_EDIT_DISABLED == tag as String) {
                    Toast.makeText(requireContext(), "Edit mode enabled", Toast.LENGTH_SHORT).show()
                    tag = TAG_EDIT_ENABLED
                    binding.includeMain.actionBtn1.visibility = View.VISIBLE
                    setImageResource(R.drawable.ic_check)
                    adapter.setEdit(true)

                } else {
                    tag = TAG_EDIT_DISABLED
                    binding.includeMain.actionBtn1.visibility = View.GONE
                    setImageResource(R.drawable.ic_edit)
                    adapter.setEdit(false)
                    viewModel.updateAsset(adapter.asset, args.model)
                }

                binding.assetDetailVP.adapter = adapter

            }
        }


        initRegisterActivityForCallbacks()

        viewModel.getAssetDetails(args.id, args.model, args.isLinear)
    }

    override fun setTitle() {
        //binding.title = args.routeId
    }

    override fun registerOtherObservers() {
        registerObservers()
    }

    private fun registerObservers() {

        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        adapter = DetailPagerAdapter(
                            args.model,
                            result.data,
                            { view -> attachImage(view) })
                        binding.assetDetailVP.adapter = adapter
                        binding.tabLayout.setupWithViewPager(binding.assetDetailVP)
                        val asset = result.data
                        viewModel.setValues(asset.assetId, args.routeId, args.routeName, args.district, asset.chainageFrom, asset.chainageTo, asset.length, asset.agensiId, asset.direction, asset.lane)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.assetUpdateLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        Toast.makeText(
                            requireContext(),
                            "Asset updated successfully.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }

        }

    }

    private fun initRegisterActivityForCallbacks() {
        activityForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult(), { results ->
                if (results.resultCode == Activity.RESULT_OK) {
                    val filePath =
                        results.data?.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH)
                    val bitmap = BitmapFactory.decodeFile(filePath)
                    adapter.setImageInEdit(attachImageNo, bitmap)

                    // Use coroutine for base64 conversion
                    lifecycleScope.launch {

                        val byteStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteStream)
                        val base64 = Base64Utils.encode(byteStream.toByteArray())
                        when (attachImageNo) {
                            1 -> {
                                adapter.asset.image1 = base64
                            }
                            2 -> {
                                adapter.asset.image2 = base64
                            }
                            3 -> {
                                adapter.asset.image3 = base64
                            }
                            4 -> {
                                adapter.asset.image4 = base64
                            }
                        }

                        MediaStore.Images.Media.insertImage(context?.contentResolver,
                            bitmap,
                            "asset_image"+System.currentTimeMillis(), ""
                        )
                    }
                }
            })
    }

    private fun attachImage(imageNo: Int) {
        attachImageNo = imageNo
        val intent = Intent(activity, ImageSelectActivity::class.java)
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CROP, false) //default is false
        activityForResult.launch(intent)
    }

}