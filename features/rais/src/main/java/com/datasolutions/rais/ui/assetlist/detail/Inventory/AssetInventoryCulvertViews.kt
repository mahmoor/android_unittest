package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryCulvertViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Km From", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            AssetInventoryDataObservableHolder("Section", asset.section.toString(), { updatedValue -> asset.section = updatedValue}),
            AssetInventoryDataObservableHolder("Latitude", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("Longitude", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS", asset.meas.toString(), { updatedValue -> asset.meas = updatedValue}),
            AssetInventoryDataObservableHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            AssetInventoryDataObservableHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            AssetInventoryDataObservableHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            AssetInventoryDataObservableHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            AssetInventoryDataObservableHolder("Diameter", asset.diameter.toString(), { updatedValue -> asset.diameter = updatedValue}),
            AssetInventoryDataObservableHolder("No. Of Cells", asset.noOfCells.toString(), { updatedValue -> asset.noOfCells = updatedValue}),
            AssetInventoryDataObservableHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            AssetInventoryDataObservableHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}