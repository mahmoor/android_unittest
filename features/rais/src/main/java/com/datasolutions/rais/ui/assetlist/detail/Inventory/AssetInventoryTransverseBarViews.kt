package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryTransverseBarViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Km From", asset.kmFrom.toString(), { updatedValue -> asset.kmFrom = updatedValue}),
            AssetInventoryDataObservableHolder("KM To", asset.kmTo.toString(), { updatedValue -> asset.kmTo = updatedValue}),
            AssetInventoryDataObservableHolder("Length (KM)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),

        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}