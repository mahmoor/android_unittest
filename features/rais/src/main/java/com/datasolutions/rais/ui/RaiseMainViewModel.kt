package com.datasolutions.rais.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.datasolutions.core.arc.SingleLiveEvent
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.rais.data.*

class RaiseMainViewModel(val savedStateHandle: SavedStateHandle): BaseViewModel() {

//    private val _pointAssetsLiveData = MutableLiveData<List<AssetDataHolder>>()
//    val pointAssetLiveData: LiveData<List<AssetDataHolder>> = _pointAssetsLiveData
//
//    private val _linearAssetsLiveData = MutableLiveData<List<AssetDataHolder>>()
//    val linearAssetLiveData: LiveData<List<AssetDataHolder>> = _linearAssetsLiveData

    private val _pointAssetsLiveData = SingleLiveEvent<List<AssetDataHolder>>()
    val pointAssetLiveData: LiveData<List<AssetDataHolder>> = _pointAssetsLiveData

    private val _linearAssetsLiveData = SingleLiveEvent<List<AssetDataHolder>>()
    val linearAssetLiveData: LiveData<List<AssetDataHolder>> = _linearAssetsLiveData

    init {
        getPointAssets()
    }

    fun getLinearAssets() {
        _linearAssetsLiveData.value = LinearAsset.linearAssets
    }

    fun getPointAssets() {
        _pointAssetsLiveData.value = PointAsset.pointAssets
    }

    fun onAssetClickes(name: String, model: String, isLinear: Boolean) {
        navigationCommands.value = NavigationCommand.To(RaisMainFragmentDirections.actionMainToAssetList(name, model, isLinear))
    }

    fun saveViewState(isLinear: Boolean) {
        savedStateHandle["isLinear"] = isLinear
    }

    fun isLinearSelected(): Boolean? {
        return savedStateHandle["isLinear"]
    }

}