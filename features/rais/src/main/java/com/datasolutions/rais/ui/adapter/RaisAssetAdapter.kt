package com.datasolutions.rais.ui.adapter


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.datasolutions.rais.data.AssetDataHolder

class RaisAssetAdapter(val clickListener: OnAssetClickListener): ListAdapter<AssetDataHolder, AssetItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<AssetDataHolder>() {
            override fun areItemsTheSame(oldItem: AssetDataHolder, newItem: AssetDataHolder): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AssetDataHolder, newItem: AssetDataHolder): Boolean {
               return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetItemViewHolder {
        return AssetItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: AssetItemViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }

    interface OnAssetClickListener {
        fun onAssetClicked(title: String, model: String, isLinear: Boolean)
    }
}