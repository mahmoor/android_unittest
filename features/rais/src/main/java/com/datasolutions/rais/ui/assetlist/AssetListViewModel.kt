package com.datasolutions.rais.ui.assetlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.datasolutions.core.arc.SingleLiveEvent
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwArgs
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetAssetsUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.rais.ui.assetlist.detail.AssetDetailReqArgs
import com.datasolutions.rais.ui.assetlist.detail.AssetListReqArgs
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AssetListViewModel(private val getAssetsUseCase: GetAssetsUseCase): BaseViewModel() {

    private val _assetsLiveData = MutableStateFlow<ResultState<List<Asset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<Asset>>> = _assetsLiveData

    fun getAssets(model: String) {
        viewModelScope.launch {
            getAssetsUseCase(AssetRequest(AssetParams(model, "search_read", getArgs(model), KwDetailArgs()))).collect {
                result ->
                _assetsLiveData.value = result
            }
        }
    }

//    fun getAssetsWithPaging(model: String): Flow<PagingData<Asset>> {
//       // return getAssetsUseCase(AssetRequest(AssetParams(model, "search_read", getArgs(model), KwArgs(10)))).cachedIn(viewModelScope)
//    }

    private fun getArgs(model: String): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, AssetListReqArgs.getArgs(model))
        return list
    }

    fun showAssetDetail(model: String, id: Int, routeId: String, routeName: String, state: String, district: String, width: String, surfaceType: String, isLinear: Boolean) {
        navigationCommands.value = NavigationCommand.To(AssetListFragmentDirections.actionAssetListFragmentToAssetDetailFragment(
            id, model, isLinear, routeId, routeName, state, district, width, surfaceType
        ))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }

    fun addAsset(model: String) {
        navigationCommands.value = NavigationCommand.To(AssetListFragmentDirections.actionAssetListFragmentToAssetAddFragment(model))
    }


}