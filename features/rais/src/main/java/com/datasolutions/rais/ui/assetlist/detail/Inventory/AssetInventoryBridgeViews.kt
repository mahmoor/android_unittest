package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryBridgeViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Km From", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            AssetInventoryDataObservableHolder("KM To", asset.kmToAt.toString(), { updatedValue -> asset.kmToAt = updatedValue}),
            AssetInventoryDataObservableHolder("Section From", asset.section.toString(), { updatedValue -> asset.section = updatedValue}),
            AssetInventoryDataObservableHolder("Section To", asset.sectionTo.toString(), { updatedValue -> asset.sectionTo = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS From", asset.measFrom.toString(), { updatedValue -> asset.measFrom = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS To", asset.measTo.toString(), { updatedValue -> asset.measTo = updatedValue}),
            AssetInventoryDataObservableHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Bridge Name", asset.bridgeName.toString(), { updatedValue -> asset.bridgeName = updatedValue}),
            AssetInventoryDataObservableHolder("Structure Number", asset.structureNumber.toString(), { updatedValue -> asset.structureNumber = updatedValue}),
            AssetInventoryDataObservableHolder("Year Built", asset.yearBuild.toString(), { updatedValue -> asset.yearBuild = updatedValue}),
            AssetInventoryDataObservableHolder("Year Repaired", asset.yearRepaired.toString(), { updatedValue -> asset.yearRepaired = updatedValue}),
            AssetInventoryDataObservableHolder("Date Installed", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            AssetInventoryDataObservableHolder("Length (m)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            AssetInventoryDataObservableHolder("Width", asset.width.toString(), { updatedValue -> asset.width = updatedValue}),
            AssetInventoryDataObservableHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            AssetInventoryDataObservableHolder("Span", asset.span.toString(), { updatedValue -> asset.span = updatedValue}),
            AssetInventoryDataObservableHolder("Nos of Span", asset.noOfSpan.toString(), { updatedValue -> asset.noOfSpan = updatedValue}),
            AssetInventoryDataObservableHolder("Type of Railing", asset.typeOfRailing.toString(), { updatedValue -> asset.typeOfRailing = updatedValue}),
            AssetInventoryDataObservableHolder("Nos of Lane", asset.noOfLane.toString(), { updatedValue -> asset.noOfLane = updatedValue}),
            AssetInventoryDataObservableHolder("Abutment Type", asset.abutementType.toString(), { updatedValue -> asset.abutementType = updatedValue}),
            AssetInventoryDataObservableHolder("System Type", asset.systemType.toString(), { updatedValue -> asset.systemType = updatedValue}),
            AssetInventoryDataObservableHolder("Deck Type", asset.deckType.toString(), { updatedValue -> asset.deckType = updatedValue}),
            AssetInventoryDataObservableHolder("Pier Type", asset.pierType.toString(), { updatedValue -> asset.pierType = updatedValue}),
            AssetInventoryDataObservableHolder("Carriageway", asset.carriageway.toString(), { updatedValue -> asset.carriageway = updatedValue}),
            AssetInventoryDataObservableHolder("Skew Angle", asset.skewAngle.toString(), { updatedValue -> asset.skewAngle = updatedValue}),
            AssetInventoryDataObservableHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            AssetInventoryDataObservableHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}