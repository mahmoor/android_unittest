package com.datasolutions.rais.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.rais.data.AssetDataHolder
import com.datasolutions.rais.databinding.ViewHolderItemRaisBinding

class AssetItemViewHolder(private val binding: ViewHolderItemRaisBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: RaisAssetAdapter.OnAssetClickListener, asset: AssetDataHolder) {
        binding.assetHolder = asset
        binding.assetIcon.setImageResource(asset.image)
        binding.root.setOnClickListener {
            clickListener.onAssetClicked(asset.name, asset.model, asset.isLinear)
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): AssetItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderItemRaisBinding.inflate(layoutInflater, parent, false)
            return AssetItemViewHolder(binding)
        }
    }
}