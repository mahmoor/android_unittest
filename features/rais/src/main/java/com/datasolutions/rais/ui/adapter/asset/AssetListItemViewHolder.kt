package com.datasolutions.rais.ui.adapter.asset

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.data.AssetDataHolder
import com.datasolutions.rais.databinding.ViewHolderItemRaisBinding
import com.datasolutions.rais.databinding.ViewHolderRaisAssetListItemBinding

class AssetListItemViewHolder(private val binding: ViewHolderRaisAssetListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: RaisAssetListAdapter.OnAssetListItemClickListener? = null, asset: Asset) {
        binding.asset = asset
        binding.kmFromLbl.visibility = View.GONE
        binding.kmFromValue.visibility = View.GONE
        binding.kmToTvLbl.visibility = View.GONE
        binding.kmToTvValue.visibility = View.GONE


        if (asset.kmToAt.isNullOrEmpty() == false) {
            binding.kmToTvLbl.visibility = View.VISIBLE
            binding.kmToTvValue.visibility = View.VISIBLE
            binding.kmToTvLbl.text = "KM To At: "
            binding.kmToTvValue.text = asset.kmToAt
        }

        if (asset.kmTo.isNullOrEmpty() == false) {
            binding.kmToTvLbl.visibility = View.VISIBLE
            binding.kmToTvValue.visibility = View.VISIBLE
            binding.kmToTvLbl.text = "KM To: "
            binding.kmToTvValue.text = asset.kmTo
        }

        if (asset.kmFromAt.isNullOrEmpty() == false) {
            binding.kmFromLbl.visibility = View.VISIBLE
            binding.kmFromValue.visibility = View.VISIBLE
            binding.kmFromLbl.text = "KM From At: "
            binding.kmFromValue.text = asset.kmFromAt
        }


        if (asset.kmFrom.isNullOrEmpty() == false) {
            binding.kmFromLbl.visibility = View.VISIBLE
            binding.kmFromValue.visibility = View.VISIBLE
            binding.kmFromLbl.text = "KM From: "
            binding.kmFromValue.text = asset.kmFrom
        }

        if(asset.kmAt.isNullOrEmpty() == false) {
            binding.kmFromLbl.visibility = View.VISIBLE
            binding.kmFromValue.visibility = View.VISIBLE
            binding.kmFromLbl.text = "KM At: "
            binding.kmFromValue.text = asset.kmAt
        }

        binding.root.setOnClickListener {
            // var id: Int

            clickListener?.showAssetDetail(
                asset.id,
                asset.routeId.orEmpty(),
                asset.roadName.orEmpty(),
                asset.state.orEmpty(),
                asset.district.orEmpty(),
                "11",
                "type"
            )
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): AssetListItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderRaisAssetListItemBinding.inflate(layoutInflater, parent, false)
            return AssetListItemViewHolder(binding)
        }
    }
}