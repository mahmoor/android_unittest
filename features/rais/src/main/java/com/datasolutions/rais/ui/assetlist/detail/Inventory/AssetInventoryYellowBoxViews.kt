package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryYellowBoxViews(val isEdit: Boolean,val isStructure: Boolean, val context: Context, val asset: Asset):
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() = if(isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("KM At", asset.kmAt.toString(), { updatedValue -> asset.kmAt = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}