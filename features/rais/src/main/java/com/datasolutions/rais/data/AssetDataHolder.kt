package com.datasolutions.rais.data

data class AssetDataHolder(
    val id: Int,
    val name: String,
    val model: String,
    val image: Int,
    val isLinear: Boolean
)
