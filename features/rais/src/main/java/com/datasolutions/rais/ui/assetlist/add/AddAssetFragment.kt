package com.datasolutions.rais.ui.assetlist.add

import `in`.mayanknagwanshi.imagepicker.ImageSelectActivity
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.util.ResultState
import com.datasolutions.rais.BR
import com.datasolutions.rais.R
import com.datasolutions.rais.databinding.FragmentAssetAddBinding
import com.datasolutions.rais.ui.assetlist.add.road.RoadListAdapter
import com.datasolutions.rais.ui.assetlist.detail.pager.DetailPagerAdapter
import com.google.android.gms.common.util.Base64Utils
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import kotlin.random.Random
import kotlin.reflect.KClass

class AddAssetFragment : BaseFragment<AddAssetViewModel, FragmentAssetAddBinding>() {
    override val viewModelClass: KClass<AddAssetViewModel>
        get() = AddAssetViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_asset_add
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel() = injectViewModel()

    private val args: AddAssetFragmentArgs by navArgs()

    private lateinit var adapter: DetailPagerAdapter

    private lateinit var activityForResult: ActivityResultLauncher<Intent>
    private var attachImageNo: Int = 0

    override fun initViews() {

        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_check)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE

        binding.includeMain.actionBtn1.setOnClickListener {
            viewModel.addAsset(args.model, adapter.asset)
        }

        lifecycleScope.launch {
            val asset = Asset(Random.nextInt())
            adapter = DetailPagerAdapter(
                args.model,
                asset,
                { imageNo -> attachImage(imageNo) })
            adapter.setEdit(true)
            binding.assetAddVP.adapter = adapter
            binding.tabLayout.setupWithViewPager(binding.assetAddVP)
        }

        initRegisterActivityForCallbacks()
    }

    override fun registerOtherObservers() {

        lifecycleScope.launchWhenCreated {
            viewModel.resultStateFlow.collect { result ->

                when (result) {
                    is ResultState.loading -> viewModel.loadingEvent.value = true
                    is ResultState.Error -> {
                        viewModel.loadingEvent.value = false
                        viewModel.errorEvent.value = result.message
                    }
                    is ResultState.Success -> {
                        viewModel.loadingEvent.value = false
                        binding.includeMain.actionBtn1.visibility = View.GONE
                        showSuccessDialog("Asset added successfully.", {
                            viewModel.backButtonClick()
                        })

                    }
                    is ResultState.default -> {}
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.roadsFlow.collect { result ->

                when (result) {
                    is ResultState.loading -> viewModel.loadingEvent.value = true
                    is ResultState.Error -> {
                        viewModel.loadingEvent.value = false
                        //  viewModel.errorEvent.value = result.message
                    }
                    is ResultState.Success -> {
                        viewModel.loadingEvent.value = false
                        val roads = result.data
                        binding.roadAutoCompleteTV.setAdapter(
                            ArrayAdapter(
                                requireContext(),
                                android.R.layout.simple_list_item_1,
                                roads
                            )
                        )

//                        binding.roadAutoCompleteTV.setAdapter(
//                            RoadListAdapter(
//                                requireContext(),
//                                roads
//                            )
//                        )


//                        binding.roadAutoCompleteTV.setOnItemClickListener { _, view, position, id ->
//                            val road = roads.find { road -> road.roadId == viewModel.getRoadId() }
//                            viewModel.setRoadName(road?.roadName)
//                        }

                    }
                    is ResultState.default -> {}
                }
            }
        }

    }

    private fun initRegisterActivityForCallbacks() {
        activityForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult(), { results ->
                if (results.resultCode == Activity.RESULT_OK) {
                    val filePath =
                        results.data?.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH)
                    val bitmap = BitmapFactory.decodeFile(filePath)
                    adapter.setImageInEdit(attachImageNo, bitmap)

                    // Use coroutine for base64 conversion
                    lifecycleScope.launch {

                        val byteStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteStream)
                        val base64 = Base64Utils.encode(byteStream.toByteArray())
                        when (attachImageNo) {
                            1 -> {
                                adapter.asset.image1 = base64
                            }
                            2 -> {
                                adapter.asset.image2 = base64
                            }
                            3 -> {
                                adapter.asset.image3 = base64
                            }
                            4 -> {
                                adapter.asset.image4 = base64
                            }
                        }

                        MediaStore.Images.Media.insertImage(
                            context?.contentResolver,
                            bitmap,
                            "asset_image" + System.currentTimeMillis(), ""
                        )
                    }
                }
            })
    }

    private fun attachImage(imageNo: Int) {
        attachImageNo = imageNo
        val intent = Intent(activity, ImageSelectActivity::class.java)
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CROP, false) //default is false
        activityForResult.launch(intent)
    }

    private fun showSuccessDialog(message: String, okClickListener: () -> Unit) {
        val errorDialog =
            AlertDialog.Builder(requireContext()).setTitle("Success").setMessage(message)
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                    okClickListener()
                })
                .create()
        errorDialog.setCancelable(false)
        errorDialog.show()
    }


}