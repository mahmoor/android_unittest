package com.datasolutions.rais.ui.assetlist.add

import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwArgsUpdateAsset
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.Road
import com.datasolutions.domain.usecase.AddAssetUseCase
import com.datasolutions.domain.usecase.GetRoadsUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.rais.ui.assetlist.detail.AssetListReqArgs
import com.datasolutions.rais.ui.assetlist.detail.AssetUpdateReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class AddAssetViewModel(val addAssetUseCase: AddAssetUseCase, val getRoadsUseCase: GetRoadsUseCase): BaseViewModel() {

    val titleObservable = ObservableField<String>("Add Asset")
    val assetIdObservable = ObservableField<String>()
    val roadNameObservable = ObservableField<String>()
    val roadIdObservable = ObservableField<String>()
    val stateObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()
    val laneObservable = ObservableField<String>()
    val directionObservable = ObservableField<String>()

    private val _resultStateFlow = MutableStateFlow<ResultState<Int>>(ResultState.default)
    val resultStateFlow : StateFlow<ResultState<Int>> = _resultStateFlow

    private val _roadsFlow = MutableStateFlow<ResultState<List<Road>>>(ResultState.default)
    val roadsFlow : StateFlow<ResultState<List<Road>>> = _roadsFlow

    init {
        getRoads()
    }

    fun addAsset(model: String, asset: Asset) {
        viewModelScope.launch {

            val request = AssetRequest(
                AssetParams(
                    model, "create_through_api", getAddArgs(asset, model),
                    KwArgsUpdateAsset()
                )
            )

            addAssetUseCase(request).collect { results ->
                _resultStateFlow.value = results
            }
        }
    }

    fun setRoadName(roadName: String?) {
        roadNameObservable.set(roadName)
    }

    fun setRoadId(roadId: String?) {
        roadIdObservable.set(roadId)
    }

    fun getRoadId(): String? {
        return roadIdObservable.get()
    }

    fun getRoadName(): String? {
        return roadNameObservable.get()
    }

    fun getRoads() {
        viewModelScope.launch {
            val request = AssetRequest(
                AssetParams(
                    "roadlist", "search_read", getRoadsArgs(),
                    KwDetailArgs()
                )
            )

            getRoadsUseCase(request).collect {
                _roadsFlow.value = it
            }
        }
    }

    private fun getAddArgs(asset: Asset, model: String): List<Any> {
        asset.assetId = assetIdObservable.get()
        asset.roadName = roadNameObservable.get()
        asset.roadId = roadIdObservable.get()
        asset.lane = laneObservable.get()
        asset.direction = directionObservable.get()
        val assetReqDto = AssetAddReqArgs.getArgs(asset, model)
        assetReqDto.assetId = asset.assetId
        assetReqDto.roadName = asset.roadName
        assetReqDto.roadId = asset.roadId
        assetReqDto.lane = asset.lane
        assetReqDto.direction = asset.direction
        var list: List<Any> = arrayListOf(assetReqDto)
        return list
    }

    private fun getRoadsArgs(): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, listOf("road_id", "road_name"))
        return list
    }
}