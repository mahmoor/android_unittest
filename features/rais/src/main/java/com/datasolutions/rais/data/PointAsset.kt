package com.datasolutions.rais.data

import com.datasolutions.rais.R

object PointAsset {

    val pointAssets = listOf(
        AssetDataHolder(1,"KM Post", "asset.km.post", R.drawable.icon_rais_kkm, false),
        AssetDataHolder(2, "Signboard", "asset.signboard", R.drawable.icon_asset_signboard, false),
        AssetDataHolder(3, "Culvert", "asset.culvert", R.drawable.icon_asset_culvert, false),
        AssetDataHolder(4, "Traffic Light", "asset.traffic.light", R.drawable.icon_asset_traffic_light, false),
        AssetDataHolder(5, "Traffic Island", "asset.traffic.island", R.drawable.icon_asset_traffic_island, false),
        AssetDataHolder(6, "Feeder Pillar", "asset.feeder.pillar", R.drawable.icon_asset_feder_pillar, false),
        AssetDataHolder(7, "Street Lighting", "asset.street.lighting", R.drawable.icon_asset_street_light, false),
        AssetDataHolder(8, "Junction", "asset.junction", R.drawable.icon_asset_junction, false),
        AssetDataHolder(9, "Road Hump", "asset.road.hump", R.drawable.icon_asset_road_hump, false),
        AssetDataHolder(10, "Bus Stop", "asset.bus.stop", R.drawable.icon_asset_bus_stop, false),
        AssetDataHolder(11, "Yellow Box", "asset.yellow.box", R.drawable.icon_asset_yellow_box, false),
        AssetDataHolder(12, "Zebra Crossing", "asset.zebra.crossing", R.drawable.icon_asset_zebra_crossing, false),


    )
}