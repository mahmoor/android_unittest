package com.datasolutions.rais.ui.assetlist.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.rais.databinding.LayoutAssetDetailInventorySubSectionBinding
import com.datasolutions.rais.ui.assetlist.detail.AssetInventoryDataObservableHolder

class AssetInventoryTrafficSafetyBarrierViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Km From (at)", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            AssetInventoryDataObservableHolder("KM To (at)", asset.kmTo.toString(), { updatedValue -> asset.kmTo = updatedValue}),
            AssetInventoryDataObservableHolder("Section From", asset.sectionFrom.toString(), { updatedValue -> asset.sectionFrom = updatedValue}),
            AssetInventoryDataObservableHolder("Section To", asset.sectionTo.toString(), { updatedValue -> asset.sectionTo = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            AssetInventoryDataObservableHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            AssetInventoryDataObservableHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS From", asset.measFrom.toString(), { updatedValue -> asset.measFrom = updatedValue}),
            AssetInventoryDataObservableHolder("MEAS To", asset.measTo.toString(), { updatedValue -> asset.measTo = updatedValue}),
            AssetInventoryDataObservableHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue}),
            AssetInventoryDataObservableHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            AssetInventoryDataObservableHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<AssetInventoryDataObservableHolder> {
        return listOf(
            AssetInventoryDataObservableHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            AssetInventoryDataObservableHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            AssetInventoryDataObservableHolder("Material", asset.material.toString(), { updatedValue -> asset.material = updatedValue}),
            AssetInventoryDataObservableHolder("Approach Terminal", asset.approachTerminal.toString(), { updatedValue -> asset.approachTerminal = updatedValue}),
            AssetInventoryDataObservableHolder("Departing Terminal", asset.departingTerminal.toString(), { updatedValue -> asset.departingTerminal = updatedValue}),
            AssetInventoryDataObservableHolder("Ground Clearance", asset.groundClearance.toString(), { updatedValue -> asset.groundClearance = updatedValue}),
            AssetInventoryDataObservableHolder("Rail Heights", asset.railHeights.toString(), { updatedValue -> asset.railHeights = updatedValue}),
            AssetInventoryDataObservableHolder("Nos Of Beam", asset.noOfBeam.toString(), { updatedValue -> asset.noOfBeam = updatedValue}),
            AssetInventoryDataObservableHolder("Nos of Packer", asset.noOfPacker.toString(), { updatedValue -> asset.noOfPacker = updatedValue}),
            AssetInventoryDataObservableHolder("Nos of Post", asset.nosPost.toString(), { updatedValue -> asset.nosPost = updatedValue}),
            AssetInventoryDataObservableHolder("Post Spacing", asset.postSpacing.toString(), { updatedValue -> asset.postSpacing = updatedValue}),
            AssetInventoryDataObservableHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            AssetInventoryDataObservableHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}