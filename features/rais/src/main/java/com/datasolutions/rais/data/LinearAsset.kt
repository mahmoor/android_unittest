package com.datasolutions.rais.data

import com.datasolutions.rais.R

object LinearAsset {

    val linearAssets = listOf(
        AssetDataHolder(13, "Bridge", "asset.bridge", R.drawable.icon_asset_bridge, true),
        AssetDataHolder(
            14,
            "Traffic Safety Barrier",
            "asset.traffic.safety.barrier",
            R.drawable.icon_asset_guardrail,
            true
        ),
        AssetDataHolder(15, "Road Drainage", "asset.drainage", R.drawable.icon_asset_drainage, true),
        AssetDataHolder(16, "Road Shoulder", "asset.road.shoulder", R.drawable.icon_asset_roadshoulder, true),
        AssetDataHolder(17, "Road Pavement", "asset.road.pavement", R.drawable.icon_asset_road, true),
        AssetDataHolder(18, "Cycle Lane", "asset.cycle.lane", R.drawable.icon_asset_cycle, true),
        AssetDataHolder(19, "Transverse Bar", "asset.transverse.bar", R.drawable.icon_asset_street_light, true),
        AssetDataHolder(20, "Median", "asset.median", R.drawable.icon_asset_junction, true),
        AssetDataHolder(21, "Cons Slope", "asset.cons.slope", R.drawable.icon_asset_road_hump, true)
    )
}