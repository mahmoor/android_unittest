package com.datasolutions.dashboard.ui

import androidx.databinding.ObservableField
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.data.util.SharedPrefHelper

class DashboardViewModel(val pref: SharedPrefHelper) : BaseViewModel() {

    val displayNameObservable = ObservableField<String>()

    init {

        displayNameObservable.set(pref.getUserDisplayName())
    }

    fun showRais() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToRais())
    }

    fun showRoute() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToRouteList())
    }

    fun showPca() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToPca())
    }

    fun showHdm4() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToHdm4())
    }

    fun showNod() {
        errorEvent.value = "showNod"
    }

    fun showMaintenanceHistory() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToMaintenanceHistory())
    }

    fun showAssetNearby() {
        navigationCommands.value =
            NavigationCommand.To(DashboardFragmentDirections.actionDashToNearbyAsset())
    }

    fun showSummary() {
        errorEvent.value = "showSummary"
    }
}