package com.datasolutions.dashboard.di

import com.datasolutions.dashboard.ui.DashboardViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object DashboardModules {

    fun load() {
        loadKoinModules(viewModel)
    }

    private val viewModel = module {
        viewModel { DashboardViewModel(get()) }
    }
}