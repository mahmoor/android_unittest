package com.datasolutions.dashboard.ui

import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.dashboard.BR
import com.datasolutions.dashboard.R
import com.datasolutions.dashboard.databinding.FragmentDashboardBinding
import kotlin.reflect.KClass

class DashboardFragment: BaseFragment<DashboardViewModel, FragmentDashboardBinding>() {
    override val viewModelClass: KClass<DashboardViewModel>
        get() = DashboardViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_dashboard
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun viewModel() = injectViewModel()


    override fun initViews() {
        binding.profileImage.setActualImageResource(R.drawable.profile_place_holder_4)
    }

    override fun registerOtherObservers() {
    }

}