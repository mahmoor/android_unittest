package com.datasolutions.nearbyasset.ui.detail.payload

object NearbyAssetDetailReqArgs {

    fun getArgs(model: String): List<String> {
        return if (model == "asset.km.post") {

            kmPost.addAll(images)
            kmPost.addAll(chainageParams)
            kmPost
        } else if (model == "asset.signboard") {

            signboard.add("image_1")
            signboard.addAll(chainageParams)
            signboard
        } else if (model == "asset.traffic.light") {

            trafficLight.addAll(images)
            trafficLight.addAll(chainageParams)
            trafficLight
        } else if (model == "asset.culvert") {

            culvert.addAll(images)
            culvert.addAll(chainageParams)
            culvert
        } else if (model == "asset.traffic.island") {

            trafficIsland.addAll(images)
            trafficIsland.addAll(chainageParams)
            trafficIsland
        } else if (model == "asset.feeder.pillar") {


            feederPillar.addAll(images)
            feederPillar.addAll(chainageParams)
            feederPillar
        } else if (model == "asset.street.lighting") {

            streetLighting.addAll(images)
            streetLighting.addAll(chainageParams)
            streetLighting
        } else if (model == "asset.junction") {

            junction.addAll(images)
            junction.addAll(chainageParams)
            junction
        } else if (model == "asset.road.hump") {

            roadHump.addAll(images)
            roadHump.addAll(chainageParams)
            roadHump
        } else if (model == "asset.bus.stop") {

            busStop.addAll(images)
            busStop.addAll(chainageParams)
            busStop
        } else if (model == "asset.yellow.box") {

            yellowBox.addAll(images)
            yellowBox.addAll(chainageParams)
            yellowBox
        } else if (model == "asset.zebra.crossing") {

            zebraCrossing.addAll(images)
            zebraCrossing.addAll(chainageParams)
            zebraCrossing
        } else if (model == "asset.bridge") {

            bridge.addAll(images)
            bridge.addAll(chainageParams)
            bridge
        } else if (model == "asset.traffic.safety.barrier") {

            trafficSafteyBarrier.addAll(images)
            trafficSafteyBarrier.addAll(chainageParams)
            trafficSafteyBarrier
        } else if (model == "asset.drainage") {


            roadDrainage.addAll(images)
            roadDrainage.addAll(chainageParams)
            roadDrainage
        } else if (model == "asset.road.shoulder") {


            roadShoulder.addAll(images)
            roadShoulder.addAll(chainageParams)
            roadShoulder
        } else if (model == "asset.road.pavement") {

            roadPavement.addAll(images)
            roadPavement.addAll(chainageParams)
            roadPavement
        } else if (model == "asset.cycle.lane") {


            cycleLane.addAll(images)
            cycleLane.addAll(chainageParams)
            cycleLane
        } else if (model == "asset.transverse.bar") {


            transverseBar.addAll(images)
            transverseBar.addAll(chainageParams)
            transverseBar
        } else if (model == "asset.median") {


            median.addAll(images)
            median.addAll(chainageParams)
            median
        } else if (model == "asset.cons.slope") {


            conesSlope.addAll(images)
            conesSlope.addAll(chainageParams)
            conesSlope
        } else listOf()
    }

    val kmPost = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "road_id",
        "remarks",
        "section",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "location",
        "events",
        "offset",
        "date_install",
        "length",
        "type",
        "condition",
        "action",
        "phase"
    )

    val signboard = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "road_id",
        "remarks",
        "section_from",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "location",
        "meas",
        "events",
        "offset",
        "category",
        "type",
        "code",
        "description",
        "size",
        "height",
        "nos_post",
        "nos_std_sign",
        "date_install",
        "action",
        "phase"
    )

    val culvert = mutableListOf(
        "asset_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "lane",
        "km_from_at",
        "remarks",
        "section",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "meas",
        "events",
        "offset",
        "type",
        "condition",
        "length",
        "size",
        "diameter",
        "no_of_cells",
        "date_install",
        "action",
        "phase",
        "structure_number",
        "bridge_name",
        "river"
    )

    val trafficLight = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val trafficIsland = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val feederPillar = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "location",
        "phase"
    )

    val streetLighting = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "location",
        "phase"
    )

    val junction = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "type",
        "phase"
    )

    val roadHump = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val busStop = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "direction",
        "route_id",
        "road_name",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val yellowBox = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val zebraCrossing = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "road_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_at",
        "x_coordinate",
        "y_coordinate",
        "z_altitudee",
        "offset",
        "phase"
    )

    val bridge = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "km_to_at",
        "road_id",
        "remarks",
        "section",
        "section_to",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "meas_from",
        "meas_to",
        "events",
        "offset",
        "river_name",
        "structure_number",
        "bridge_name",
        "year_build",
        "year_repaired",
        "date_install",
        "length",
        "width",
        "type",
        "carriageway_width",
        "span",
        "nos_of_span",
        "type_of_railing",
        "nos_of_lane",
        "abutement_type",
        "system_type",
        "deck_type",
        "pier_type",
        "carriageway",
        "skew_angle",
        "condition",
        "action",
        "phase"
    )

    val trafficSafteyBarrier = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "km_to",
        "road_id",
        "remarks",
        "section_from",
        "section_to",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "meas_from",
        "meas_to",
        "location",
        "events",
        "offset",
        "type",
        "length",
        "material",
        "approach_terminal",
        "date_install",
        "departing_terminal",
        "ground_clearance",
        "rail_heights",
        "no_of_beam",
        "no_of_packer",
        "no_of_post",
        "post_spacing",
        "condition",
        "phase"
    )

    val roadDrainage = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "km_to",
        "road_id",
        "remarks",
        "section_from",
        "section_to",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "meas_from",
        "meas_to",
        "location",
        "events",
        "offset",
        "category",
        "type",
        "length",
        "size",
        "width",
        "date_install",
        "shape",
        "braced",
        "condition",
        "phase"
    )

    val roadShoulder = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from_at",
        "km_to",
        "road_id",
        "remarks",
        "section_from",
        "section_to",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "meas_from",
        "meas_to",
        "events",
        "offset",
        "category",
        "type",
        "length",
        "width",
        "date_install",
        "condition",
        "phase"
    )

    val roadPavement = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from",
        "km_to",
        "length",
        "road_id",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "offset",
        "width",
        "surface_type",
        "phase"
    )

    val cycleLane = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from",
        "km_to",
        "road_id",
        "length",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "offset",
        "type",
        "phase"
    )

    val transverseBar = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from",
        "km_to",
        "road_id",
        "length",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "offset",
        "phase"
    )

    val median = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from",
        "km_to",
        "road_id",
        "length",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "offset",
        "type",
        "phase"
    )

    val conesSlope = mutableListOf(
        "asset_id",
        "state_id",
        "district_id",
        "route_id",
        "road_name",
        "direction",
        "lane",
        "km_from",
        "km_to",
        "road_id",
        "length",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "z_altitudee",
        "offset",
        "type",
        "phase"
    )

    val images = listOf(
        "image_1",
        "image_2",
        "image_3",
        "image_4"
    )

    val chainageParams = listOf(
        "chainage_from",
        "chainage_to",
        "agensi_id"
    )
}