package com.datasolutions.nearbyasset.ui.detail

import android.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.marginTop
import androidx.databinding.ViewDataBinding
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.Inventory.*


class NearbyAssetDetailInventoryViewManager(val isEdit: Boolean, val context: Context, val model: String, val asset: Asset) {

    val inflator = LayoutInflater.from(context)

//    fun getLocationBinding() : List<View> {
//       return  AssetInventoryKMPostViews(isEdit,false, context, asset).invoke()
//    }

    fun getLocationViews(): List<View> {
        return if (model == "asset.km.post") {
            AssetInventoryKMPostViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.signboard") {
            AssetInventorySignboardViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.traffic.light") {
            AssetInventoryTrafficLightViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.culvert") {
            AssetInventoryCulvertViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.traffic.island") {
            AssetInventoryTrafficIslandViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.feeder.pillar") {
            AssetInventoryFeedarPillarViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.street.lighting") {
            AssetInventoryStreetLightingViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.junction") {
            AssetInventoryJunctionViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.road.hump") {
            AssetInventoryRoadHumpViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.bus.stop") {
            AssetInventoryBusStopViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.yellow.box") {
            AssetInventoryYellowBoxViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.zebra.crossing") {
            AssetInventoryZebraCrossingViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.bridge") {
            AssetInventoryBridgeViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.traffic.safety.barrier") {
            AssetInventoryTrafficSafetyBarrierViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.drainage") {
            AssetInventoryRoadDrainageViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.road.shoulder") {
            AssetInventoryRoadShoulderViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.road.pavement") {
            AssetInventoryRoadPavementViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.cycle.lane") {
            AssetInventoryCycleLaneViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.transverse.bar") {
            AssetInventoryTransverseBarViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.median") {
            AssetInventoryMedianViews(isEdit,false, context, asset).invoke()
        } else if (model == "asset.cons.slope") {
            AssetInventoryConsSlopeViews(isEdit,false, context, asset).invoke()
        } else AssetInventorySignboardViews(isEdit,false, context, asset).invoke()
    }

    fun getStructureViews(): List<View> {
        return if (model == "asset.km.post") {
            AssetInventoryKMPostViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.signboard") {
            AssetInventorySignboardViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.traffic.light") {
            AssetInventoryTrafficLightViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.culvert") {
            AssetInventoryCulvertViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.traffic.island") {
            AssetInventoryTrafficIslandViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.feeder.pillar") {
            AssetInventoryFeedarPillarViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.street.lighting") {
            AssetInventoryStreetLightingViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.junction") {
            AssetInventoryJunctionViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.road.hump") {
            AssetInventoryRoadHumpViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.bus.stop") {
            AssetInventoryBusStopViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.yellow.box") {
            AssetInventoryYellowBoxViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.zebra.crossing") {
            AssetInventoryZebraCrossingViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.bridge") {
            AssetInventoryBridgeViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.traffic.safety.barrier") {
            AssetInventoryTrafficSafetyBarrierViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.drainage") {
            AssetInventoryRoadDrainageViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.road.shoulder") {
            AssetInventoryRoadShoulderViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.road.pavement") {
            AssetInventoryRoadPavementViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.cycle.lane") {
            AssetInventoryCycleLaneViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.transverse.bar") {
            AssetInventoryTransverseBarViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.median") {
            AssetInventoryMedianViews(isEdit,true, context, asset).invoke()
        } else if (model == "asset.cons.slope") {
            AssetInventoryConsSlopeViews(isEdit,true, context, asset).invoke()
        } else AssetInventorySignboardViews(isEdit,true, context, asset).invoke()
    }

}