package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventorySignboardViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    private fun getKmPostDataForViews(): List<NearByAssetInventoryDataHolder> {

        val map = listOf(
            NearByAssetInventoryDataHolder("Km From (at)", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            NearByAssetInventoryDataHolder("Section From", asset.sectionFrom.toString(), { updatedValue -> asset.sectionFrom = updatedValue}),
            NearByAssetInventoryDataHolder("Latitude", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("Longitude", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS", asset.meas.toString(), { updatedValue -> asset.meas = updatedValue}),
            NearByAssetInventoryDataHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue}),
            NearByAssetInventoryDataHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )

        return map
    }


    private fun getKmPostStructureDataForViews(): List<NearByAssetInventoryDataHolder> {
        val map = listOf(
            NearByAssetInventoryDataHolder("Category", asset.category.toString(), { updatedValue -> asset.category = updatedValue}),
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Code", asset.code.toString(), { updatedValue -> asset.code = updatedValue}),
            NearByAssetInventoryDataHolder("Description", asset.description.toString(), { updatedValue -> asset.description = updatedValue}),
            NearByAssetInventoryDataHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            NearByAssetInventoryDataHolder("Bottom Height", asset.height.toString(), { updatedValue -> asset.height = updatedValue}),
            NearByAssetInventoryDataHolder("NOS Post", asset.nosPost.toString(), { updatedValue -> asset.nosPost = updatedValue}),
            NearByAssetInventoryDataHolder("NOS Std Sign", asset.nosStdSign.toString(), { updatedValue -> asset.nosStdSign = updatedValue}),
            NearByAssetInventoryDataHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            NearByAssetInventoryDataHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
        return map
    }

    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return getKmPostDataForViews()
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return getKmPostStructureDataForViews()
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}