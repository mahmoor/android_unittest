package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryBridgeViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Km From", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            NearByAssetInventoryDataHolder("KM To", asset.kmToAt.toString(), { updatedValue -> asset.kmToAt = updatedValue}),
            NearByAssetInventoryDataHolder("Section From", asset.section.toString(), { updatedValue -> asset.section = updatedValue}),
            NearByAssetInventoryDataHolder("Section To", asset.sectionTo.toString(), { updatedValue -> asset.sectionTo = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS From", asset.measFrom.toString(), { updatedValue -> asset.measFrom = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS To", asset.measTo.toString(), { updatedValue -> asset.measTo = updatedValue}),
            NearByAssetInventoryDataHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Bridge Name", asset.bridgeName.toString(), { updatedValue -> asset.bridgeName = updatedValue}),
            NearByAssetInventoryDataHolder("Structure Number", asset.structureNumber.toString(), { updatedValue -> asset.structureNumber = updatedValue}),
            NearByAssetInventoryDataHolder("Year Built", asset.yearBuild.toString(), { updatedValue -> asset.yearBuild = updatedValue}),
            NearByAssetInventoryDataHolder("Year Repaired", asset.yearRepaired.toString(), { updatedValue -> asset.yearRepaired = updatedValue}),
            NearByAssetInventoryDataHolder("Date Installed", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            NearByAssetInventoryDataHolder("Length (m)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            NearByAssetInventoryDataHolder("Width", asset.width.toString(), { updatedValue -> asset.width = updatedValue}),
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Span", asset.span.toString(), { updatedValue -> asset.span = updatedValue}),
            NearByAssetInventoryDataHolder("Nos of Span", asset.noOfSpan.toString(), { updatedValue -> asset.noOfSpan = updatedValue}),
            NearByAssetInventoryDataHolder("Type of Railing", asset.typeOfRailing.toString(), { updatedValue -> asset.typeOfRailing = updatedValue}),
            NearByAssetInventoryDataHolder("Nos of Lane", asset.noOfLane.toString(), { updatedValue -> asset.noOfLane = updatedValue}),
            NearByAssetInventoryDataHolder("Abutment Type", asset.abutementType.toString(), { updatedValue -> asset.abutementType = updatedValue}),
            NearByAssetInventoryDataHolder("System Type", asset.systemType.toString(), { updatedValue -> asset.systemType = updatedValue}),
            NearByAssetInventoryDataHolder("Deck Type", asset.deckType.toString(), { updatedValue -> asset.deckType = updatedValue}),
            NearByAssetInventoryDataHolder("Pier Type", asset.pierType.toString(), { updatedValue -> asset.pierType = updatedValue}),
            NearByAssetInventoryDataHolder("Carriageway", asset.carriageway.toString(), { updatedValue -> asset.carriageway = updatedValue}),
            NearByAssetInventoryDataHolder("Skew Angle", asset.skewAngle.toString(), { updatedValue -> asset.skewAngle = updatedValue}),
            NearByAssetInventoryDataHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            NearByAssetInventoryDataHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}