package com.datasolutions.nearbyasset.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.drawable.Animatable
import android.location.Location
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.entity.response.NearByAsset
import com.datasolutions.domain.util.ResultState
import com.datasolutions.nearbyasset.R
import com.datasolutions.nearbyasset.BR
import com.datasolutions.nearbyasset.databinding.FragmentNearbyAssetListBinding
import com.datasolutions.nearbyasset.databinding.ViewHolderNearbyAssetListItemBinding
import com.datasolutions.nearbyasset.model.ExpandableHeader
import com.datasolutions.nearbyasset.model.NearbyListItem
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.google.android.gms.tasks.Task
import com.xwray.groupie.*
import com.xwray.groupie.databinding.BindableItem
import kotlinx.coroutines.flow.collect
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.reflect.KClass

class NearByAssetListFragment :
    BaseFragment<NearByAssetListViewModel, FragmentNearbyAssetListBinding>() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>

    override val viewModelClass: KClass<NearByAssetListViewModel>
        get() = NearByAssetListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_nearby_asset_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        if (result.data.isEmpty())
                            viewModel.showError("No asset detect within 50 meters.")
                        else
                            setupRecyclerView(viewModel.getDataForAdapter(result.data))

                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }

            }
        }
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE

        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestMultiplePermissions()
            ) { permissions ->
                var grantedCount = 0
                permissions.entries.forEach {
                    val permissionName = it.key
                    val isGranted = it.value
                    if (isGranted) {
                        grantedCount++
                    } else {

                    }
                }

                if (grantedCount < 2) {
                    viewModel.showLoading(false)
                    viewModel.showError("All Location permissions are required for this feature!!!")
                } else {
                    getLocationAndAssets()
                }
            }

        createLocationRequest()
    }

    private fun setupRecyclerView(map: HashMap<String, List<NearByAsset>>) {

        val adapter = GroupieAdapter()

        map.forEach { (key, value) ->
            val keySplist = key.split(".")
            var type = ""
            if (keySplist.size > 2)
                type = keySplist[1] + " " + keySplist[2]
            else type = keySplist[1]

            type = type.uppercase(Locale.getDefault())
            val expandableHeaderItem = ExpandableHeader(type)
            val section = ExpandableGroup(expandableHeaderItem)
            section.apply {
                value.forEach {
                    add(NearbyListItem(it.assetId!!, it.distance!!, key, it.id, { key, assetId ->
                        viewModel.showDetail(key, assetId)
                    }))
                }
            }
            adapter.add(section)
        }

        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.nearByRecyclerView.layoutManager = layoutManager
        binding.nearByRecyclerView.adapter = adapter

    }

    override fun setTitle() {
        binding.title = "NearBy Assets"
    }

    fun createLocationRequest() {
        val locationRequest = LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->

            val states = locationSettingsResponse.locationSettingsStates
            if (states?.isLocationPresent == true) {
                checkPermission()
            }
        }

        var launcher =
            registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    checkPermission()
                } else {
                    viewModel.showLoading(false)
                    viewModel.showError("Please turn on location from your phone settings.")
                }
            }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    val intentSenderRequest =
                        IntentSenderRequest.Builder(exception.status.resolution!!).build()
                    launcher.launch(intentSenderRequest)

                } catch (sendEx: IntentSender.SendIntentException) {
                    viewModel.showLoading(false)
                    viewModel.showError("Please turn on location from your phone settings.")
                }
            }
        }

    }

    fun checkPermission() {

        val arrayOfPermissions = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!hasPermissions(arrayOfPermissions.toList())) {
            requestPermissionLauncher.launch(arrayOfPermissions)
        } else {
            getLocationAndAssets()
        }

    }

    fun hasPermissions(permissions: List<String>): Boolean = permissions.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    fun getLocationAndAssets() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        val locationRequest = LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, object : LocationCallback() {
            override fun onLocationResult(location: LocationResult) {
                viewModel.getNearByAsset(
                    location.lastLocation.latitude.toString(),
                    location.lastLocation.longitude.toString())

                fusedLocationClient.removeLocationUpdates(this)
            }
        }, Looper.getMainLooper())

    }
}
