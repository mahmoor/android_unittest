package com.datasolutions.nearbyasset.ui

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.entity.response.NearByAsset
import com.datasolutions.domain.usecase.GetNearByAssetsUseCase
import com.datasolutions.domain.util.ResultState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NearByAssetListViewModel(val getNearByAssetsUseCase: GetNearByAssetsUseCase) :
    BaseViewModel() {


    private val _assetsLiveData =
        MutableStateFlow<ResultState<List<NearByAsset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<NearByAsset>>> = _assetsLiveData


    fun getNearByAsset(lat: String, lang: String) {
        viewModelScope.launch {
            getNearByAssetsUseCase(lat, lang).collect { result ->
                _assetsLiveData.value = result
            }
        }
    }

    fun getDataForAdapter(assets: List<NearByAsset>): HashMap<String, List<NearByAsset>> {
        val map = HashMap<String, List<NearByAsset>>()
        var assetTypes = arrayListOf<String>()
        assets.forEach { asset ->
            assetTypes.add(asset.type!!)
        }

        assetTypes.forEach { assetType ->
            map.put(assetType, assets.filter { it -> it.type == assetType })
        }

        return map
    }

    fun showDetail(type: String, assetId: String) {

        navigationCommands.value = NavigationCommand.To(NearByAssetListFragmentDirections.actionListToDetail(type, assetId))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}