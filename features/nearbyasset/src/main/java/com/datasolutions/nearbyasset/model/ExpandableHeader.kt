package com.datasolutions.nearbyasset.model

import com.datasolutions.nearbyasset.databinding.ViewHolderNearbyAssetHeaderItemBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem

class ExpandableHeader(titleStringResId: String): NearbyHeaderItem(titleStringResId), ExpandableItem {

    var clickListener: ((ExpandableHeader) -> Unit)? = null

    private lateinit var expandableGroup: ExpandableGroup

    override fun bind(viewHolder: ViewHolderNearbyAssetHeaderItemBinding, position: Int) {
        super.bind(viewHolder, position)

        viewHolder.llItem.setOnClickListener {
            expandableGroup.onToggleExpanded()
            if(expandableGroup.isExpanded) {
                isExpandedObserveable.set(true)
            } else isExpandedObserveable.set(false)
        }
    }

    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        this.expandableGroup = onToggleListener
    }

}