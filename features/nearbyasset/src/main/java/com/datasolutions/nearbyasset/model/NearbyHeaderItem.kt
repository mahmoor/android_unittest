package com.datasolutions.nearbyasset.model

import androidx.databinding.ObservableBoolean
import com.datasolutions.nearbyasset.R
import com.datasolutions.nearbyasset.databinding.ViewHolderNearbyAssetHeaderItemBinding
import com.datasolutions.nearbyasset.databinding.ViewHolderNearbyAssetListItemBinding
import com.xwray.groupie.databinding.BindableItem

open class NearbyHeaderItem(private val title: String) :
    BindableItem<ViewHolderNearbyAssetHeaderItemBinding>() {

    var isExpandedObserveable = ObservableBoolean(false)
    var titles= title
    override fun bind(binding: ViewHolderNearbyAssetHeaderItemBinding, p1: Int) {
        binding.model = this
    }

    override fun getLayout() = R.layout.view_holder_nearby_asset_header_item
}