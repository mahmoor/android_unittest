package com.datasolutions.nearbyasset.ui.detail.pager

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.viewpager.widget.PagerAdapter
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.databinding.LayoutNearbyAssetDetailInventoryBinding
import com.datasolutions.nearbyasset.databinding.LayoutNearbyAssetImagesBinding
import com.datasolutions.nearbyasset.databinding.LayoutNearbyAssetMapBinding
import com.datasolutions.nearbyasset.ui.detail.NearbyAssetDetailInventoryViewManager
import com.google.android.gms.common.util.Base64Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class DetailPagerAdapter(val model: String, val asset: Asset, val attachImage: (Int) -> Unit) :
    PagerAdapter(), OnMapReadyCallback {

    private var isEdit: Boolean = false
    lateinit var inflator: LayoutInflater
    lateinit var bindingMap: LayoutNearbyAssetMapBinding
    lateinit var bindingImages: LayoutNearbyAssetImagesBinding

    override fun getCount(): Int {
        return 3
    }

    fun setEdit(isEdit: Boolean) {
        this.isEdit = isEdit
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as View?
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflator = LayoutInflater.from(container.context)
        when (position) {
            0 -> {
               val bindingInventory =
                    LayoutNearbyAssetDetailInventoryBinding.inflate(inflator, container, false)
                bindingInventory.asset = asset
                val assetInventory =
                    NearbyAssetDetailInventoryViewManager(isEdit, container.context, model, asset)
                assetInventory.getLocationViews()
                    .forEach { bindingInventory.layoutLocationInfo.addView(it) }

                if (assetInventory.getStructureViews().isEmpty()) {
                    bindingInventory.structureInfoTitle.visibility = View.GONE
                } else
                    assetInventory.getStructureViews()
                        .forEach { bindingInventory.layoutStructureInfo.addView(it) }

                container.addView(bindingInventory.root)
                return bindingInventory.root
            }

            1 -> {

                bindingMap = LayoutNearbyAssetMapBinding.inflate(inflator, container, false)
                bindingMap.title = ""
                bindingMap.mapView.onCreate(null);
                bindingMap.mapView.onResume();
                bindingMap.mapView.getMapAsync(this)
                container.addView(bindingMap.root)
                return bindingMap.root

            }

            else -> {

                bindingImages = LayoutNearbyAssetImagesBinding.inflate(inflator, container, false)
                bindingImages.assetItem = asset
                bindingImages.isEdit = isEdit

                if (model == "asset.signboard") {
                    bindingImages.image2.visibility = View.GONE
                    bindingImages.image3.visibility = View.GONE
                    bindingImages.image4.visibility = View.GONE
                }

                if (isEdit) {
                    bindingImages.image1.setOnClickListener {
                        attachImage(1)
                    }

                    bindingImages.image2.setOnClickListener {
                        attachImage(2)
                    }

                    bindingImages.image3.setOnClickListener {
                        attachImage(3)
                    }

                    bindingImages.image4.setOnClickListener {
                        attachImage(4)
                    }
                }

                container.addView(bindingImages.root)
                return bindingImages.root
            }
        }

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Inventory"
            1 -> "Map"
            else -> "Images"
        }
    }

    override fun onMapReady(mMap: GoogleMap) {
        val position = asset.latitudeForMap?.let { lat ->
            asset.longitudeForMap?.let { long ->
                LatLng(
                    lat,
                    long
                )
            }
        }
        if (position != null) {
            val markerOptions = MarkerOptions()
            position.let { markerOptions.position(it) }

            mMap.addMarker(markerOptions)
            val cameraFactory = position.let { CameraUpdateFactory.newLatLngZoom(it, 12f) }
            cameraFactory.let { mMap.moveCamera(it) }
        }
    }

    fun setImageInEdit(imageNo: Int, bitmap: Bitmap) {
        when (imageNo) {
            1 -> {
//                val byteArray = Base64Utils.decode(asset.image1)
                bindingImages.image1.setImageBitmap(bitmap)
            }
            2 -> {
                // val byteArray = Base64Utils.decode(asset.image2)
                bindingImages.image2.setImageBitmap(bitmap)

            }
            3 -> {
                //val byteArray = Base64Utils.decode(asset.image3)
                bindingImages.image3.setImageBitmap(
                    bitmap
                )
            }
            4 -> {
                // val byteArray = Base64Utils.decode(asset.image4)
                bindingImages.image4.setImageBitmap(bitmap)
            }
        }
    }

}