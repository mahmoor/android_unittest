package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryConsSlopeViews (
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Km From", asset.kmFrom.toString(), { updatedValue -> asset.kmFrom = updatedValue}),
            NearByAssetInventoryDataHolder("KM To", asset.kmTo.toString(), { updatedValue -> asset.kmTo = updatedValue}),
            NearByAssetInventoryDataHolder("Length (KM)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),

        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}