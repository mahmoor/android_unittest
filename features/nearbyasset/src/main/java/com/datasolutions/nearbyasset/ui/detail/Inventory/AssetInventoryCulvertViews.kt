package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryCulvertViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Km From", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            NearByAssetInventoryDataHolder("Section", asset.section.toString(), { updatedValue -> asset.section = updatedValue}),
            NearByAssetInventoryDataHolder("Latitude", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("Longitude", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS", asset.meas.toString(), { updatedValue -> asset.meas = updatedValue}),
            NearByAssetInventoryDataHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            NearByAssetInventoryDataHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            NearByAssetInventoryDataHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            NearByAssetInventoryDataHolder("Diameter", asset.diameter.toString(), { updatedValue -> asset.diameter = updatedValue}),
            NearByAssetInventoryDataHolder("No. Of Cells", asset.noOfCells.toString(), { updatedValue -> asset.noOfCells = updatedValue}),
            NearByAssetInventoryDataHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            NearByAssetInventoryDataHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}