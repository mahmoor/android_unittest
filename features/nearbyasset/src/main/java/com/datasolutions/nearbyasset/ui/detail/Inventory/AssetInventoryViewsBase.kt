package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.nearbyasset.databinding.LayoutNearbyAssetDetailInventorySubSectionBinding
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

abstract class AssetInventoryViewsBase {

    public fun getInventoryLocationViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForLocationViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutNearbyAssetDetailInventorySubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }



    public fun getInventoryStructureViews(): List<View> {
        val listOfViews: ArrayList<View> = arrayListOf()
        val data = getDataForStructureViews()
        data.forEach { it ->
            val childViewBinding =
                LayoutNearbyAssetDetailInventorySubSectionBinding.inflate(getLayoutInflator())
            childViewBinding.dataHolder = it
            childViewBinding.isEdit = isEditMode()
            (childViewBinding.assetSubViewLayout.layoutParams as LinearLayout.LayoutParams).apply {
                topMargin = 16
            }
            listOfViews.add(childViewBinding.root)
        }
        return listOfViews
    }

    abstract fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder>
    abstract fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder>
    abstract fun isEditMode(): Boolean
    abstract fun getLayoutInflator(): LayoutInflater
}