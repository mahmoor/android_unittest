package com.datasolutions.nearbyasset.di

import com.datasolutions.nearbyasset.ui.NearByAssetListViewModel
import com.datasolutions.nearbyasset.ui.detail.NearbyAssetDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object NearByModules {

    fun load() {
        loadKoinModules(nearbyModules)
    }

    private val nearbyModules = module {
        viewModel { NearByAssetListViewModel(get()) }
        viewModel { NearbyAssetDetailViewModel(get()) }
    }
}