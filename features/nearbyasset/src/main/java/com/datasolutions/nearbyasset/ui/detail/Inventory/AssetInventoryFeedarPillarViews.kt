package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryFeedarPillarViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)

    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("KM At", asset.kmAt.toString(), { updatedValue -> asset.kmAt = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            NearByAssetInventoryDataHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}