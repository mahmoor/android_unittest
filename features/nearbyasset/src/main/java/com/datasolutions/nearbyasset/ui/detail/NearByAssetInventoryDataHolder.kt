package com.datasolutions.nearbyasset.ui.detail

import android.util.Log
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import com.datasolutions.domain.entity.response.Asset

//data class NearByAssetInventoryDataHolder(val title: String, var value: String)

class NearByAssetInventoryDataHolder(val title: String, var valueOfAsset: String, val updateAsset: (String?) -> Unit = {}) {
    var valueObservable: ObservableField<String>
    init {
        if(valueOfAsset == "null")
            valueOfAsset = ""

        valueObservable = ObservableField(valueOfAsset)
        valueObservable.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                val updatedValue = sender as ObservableField<String>
                updateAsset(updatedValue.get())
            }
        })
    }

}

