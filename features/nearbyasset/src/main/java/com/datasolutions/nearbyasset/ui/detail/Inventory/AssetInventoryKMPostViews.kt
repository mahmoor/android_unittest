package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.ObservableField
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder
import com.datasolutions.nearbyasset.ui.detail.Inventory.AssetInventoryKMPostViews.*

class AssetInventoryKMPostViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()


    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {

        return listOf(
            NearByAssetInventoryDataHolder("Km From (at)", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue} ),
            NearByAssetInventoryDataHolder("Section", asset.section.toString(), { updatedValue -> asset.section = updatedValue } ),
            NearByAssetInventoryDataHolder("Latitude", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue }),
            NearByAssetInventoryDataHolder("Longitude", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue }),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue }),
            NearByAssetInventoryDataHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue }),
            NearByAssetInventoryDataHolder("Event", asset.event.toString(), { updatedValue -> asset.event = updatedValue }),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue })
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            NearByAssetInventoryDataHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue}),
            NearByAssetInventoryDataHolder("Action", asset.action.toString(), { updatedValue -> asset.action = updatedValue}),
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}