package com.datasolutions.nearbyasset.model

import com.datasolutions.nearbyasset.R
import com.datasolutions.nearbyasset.databinding.ViewHolderNearbyAssetListItemBinding
import com.xwray.groupie.databinding.BindableItem

class NearbyListItem(private val title: String, private val distance: String, private val type: String, private val assetId: String, val onItemClick:(String, String) -> Unit) :
    BindableItem<ViewHolderNearbyAssetListItemBinding>() {

    override fun getLayout() = R.layout.view_holder_nearby_asset_list_item


    override fun bind(binding: ViewHolderNearbyAssetListItemBinding, position: Int) {
        binding.title = title
        binding.distance = distance
        binding.llItem.setOnClickListener {
            onItemClick(type, assetId)
        }
        binding.executePendingBindings()
    }
}
