package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryRoadDrainageViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Km From (at)", asset.kmFromAt.toString()),
            NearByAssetInventoryDataHolder("KM To (at)", asset.kmTo.toString()),
            NearByAssetInventoryDataHolder("Section From", asset.sectionFrom.toString()),
            NearByAssetInventoryDataHolder("Section To", asset.sectionTo.toString()),
            NearByAssetInventoryDataHolder("X-Coordinate From", asset.latitude.toString()),
            NearByAssetInventoryDataHolder("Y-Coordinate From", asset.longitude.toString()),
            NearByAssetInventoryDataHolder("X-Coordinate To", asset.latitudeTo.toString()),
            NearByAssetInventoryDataHolder("Y-Coordinate To", asset.longitudeTo.toString()),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString()),
            NearByAssetInventoryDataHolder("MEAS From", asset.measFrom.toString()),
            NearByAssetInventoryDataHolder("MEAS To", asset.measTo.toString()),
            NearByAssetInventoryDataHolder("Location", asset.location.toString()),
            NearByAssetInventoryDataHolder("Events", asset.event.toString()),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString())
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Category", asset.category.toString(), { updatedValue -> asset.category = updatedValue}),
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Size", asset.size.toString(), { updatedValue -> asset.size = updatedValue}),
            NearByAssetInventoryDataHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            NearByAssetInventoryDataHolder("Width", asset.width.toString(), { updatedValue -> asset.width = updatedValue}),
            NearByAssetInventoryDataHolder("Shape", asset.shape.toString(), { updatedValue -> asset.shape = updatedValue}),
            NearByAssetInventoryDataHolder("Braced", asset.braced.toString(), { updatedValue -> asset.braced = updatedValue}),
            NearByAssetInventoryDataHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            NearByAssetInventoryDataHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}