package com.datasolutions.nearbyasset.ui.detail

import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetAssetDetailUseCase
import com.datasolutions.domain.usecase.UpdateAssetUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.nearbyasset.ui.detail.payload.NearbyAssetDetailReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NearbyAssetDetailViewModel(val getAssetDetailUseCase: GetAssetDetailUseCase): BaseViewModel() {

    private val _assetDetailLiveData = MutableStateFlow<ResultState<Asset>>(ResultState.loading)
    val assetDetailLiveData: StateFlow<ResultState<Asset>> = _assetDetailLiveData


    val routeIdObservable = ObservableField<String>()
    val assetIdObservable = ObservableField<String>()
    val routeNameObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()
    val startObservable = ObservableField<String>()
    val endObservable = ObservableField<String>()
    val lengthObservable = ObservableField<String>()
    val laneObservable = ObservableField<String>()
    val directionObservable = ObservableField<String>()
    val agensiIdObservable = ObservableField<String>()

    fun setValues(
        assetId: String? ="",
        routeId: String? ="",
        routeName: String? ="",
        district: String? ="",
        start: String? ="",
        end: String? ="",
        length: String? ="",
        agensiId: String? ="",
        direction: String? ="",
        lane: String? =""
    ) {
        assetIdObservable.set(assetId)
        routeIdObservable.set(routeId)
        routeNameObservable.set(routeName)
        districtObservable.set(district)
        startObservable.set(start)
        endObservable.set(end)
        lengthObservable.set(length)
        directionObservable.set(direction)
        laneObservable.set(lane)
        agensiIdObservable.set(agensiId)
    }

    fun getAssetDetails(id: Int, model: String) {
        viewModelScope.launch {
            getAssetDetailUseCase(
                AssetRequest(
                    AssetParams(
                        model, "read", getArgs(id, model),
                        KwDetailArgs()
                    )
                )
            ).collect { result ->
                _assetDetailLiveData.value = result
            }
        }
    }

    private fun getArgs(id: Int, model: String): List<Any> {
        val args2 = mutableListOf(id)
        var list: List<Any> = arrayListOf(args2, NearbyAssetDetailReqArgs.getArgs(model))
        return list
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}