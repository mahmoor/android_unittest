package com.datasolutions.nearbyasset.ui.detail

import `in`.mayanknagwanshi.imagepicker.ImageSelectActivity
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.nearbyasset.R
import com.datasolutions.nearbyasset.BR
import com.datasolutions.nearbyasset.databinding.FragmentNearbyAssetDetailBinding
import com.datasolutions.nearbyasset.ui.detail.pager.DetailPagerAdapter
import com.google.android.gms.common.util.Base64Utils
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import kotlin.reflect.KClass

class NearbyAssetDetailFragment: BaseFragment<NearbyAssetDetailViewModel, FragmentNearbyAssetDetailBinding>() {

    private val args: NearbyAssetDetailFragmentArgs by navArgs()

    override val viewModelClass: KClass<NearbyAssetDetailViewModel>
        get() = NearbyAssetDetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_nearby_asset_detail
    override val bindingVariable: Int
        get() = BR.viewModel


    private lateinit var adapter: DetailPagerAdapter
    private lateinit var activityForResult: ActivityResultLauncher<Intent>
    private var attachImageNo: Int = 0
    private val TAG_EDIT_ENABLED = "edit_enabled"
    private val TAG_EDIT_DISABLED = "edit_disabled"

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        adapter = DetailPagerAdapter(
                            args.type,
                            result.data,
                            { view -> attachImage(view) })
                        binding.assetDetailVP.adapter = adapter
                        binding.tabLayout.setupWithViewPager(binding.assetDetailVP)
                        val asset = result.data
                        viewModel.setValues(asset.assetId, asset.routeId, asset.roadName, asset.district, asset.chainageFrom, asset.chainageTo, asset.length, asset.agensiId, asset.direction, asset.lane)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        initRegisterActivityForCallbacks()
        viewModel.getAssetDetails(args.assetId.toInt(), args.type)
    }

    private fun initRegisterActivityForCallbacks() {
        activityForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult(), { results ->
                if (results.resultCode == Activity.RESULT_OK) {
                    val filePath =
                        results.data?.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH)
                    val bitmap = BitmapFactory.decodeFile(filePath)
                    adapter.setImageInEdit(attachImageNo, bitmap)

                    // Use coroutine for base64 conversion
                    lifecycleScope.launch {

                        val byteStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteStream)
                        val base64 = Base64Utils.encode(byteStream.toByteArray())
                        when (attachImageNo) {
                            1 -> {
                                adapter.asset.image1 = base64
                            }
                            2 -> {
                                adapter.asset.image2 = base64
                            }
                            3 -> {
                                adapter.asset.image3 = base64
                            }
                            4 -> {
                                adapter.asset.image4 = base64
                            }
                        }

                        MediaStore.Images.Media.insertImage(context?.contentResolver,
                            bitmap,
                            "asset_image"+System.currentTimeMillis(), ""
                        )
                    }
                }
            })
    }

    private fun attachImage(imageNo: Int) {
        attachImageNo = imageNo
        val intent = Intent(activity, ImageSelectActivity::class.java)
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true) //default is true
        intent.putExtra(ImageSelectActivity.FLAG_CROP, false) //default is false
        activityForResult.launch(intent)
    }

    override fun setTitle() {

    }
}