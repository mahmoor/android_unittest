package com.datasolutions.nearbyasset.ui.detail.Inventory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.nearbyasset.ui.detail.NearByAssetInventoryDataHolder

class AssetInventoryTrafficSafetyBarrierViews(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) :
    AssetInventoryViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() =
        if (isStructure) getInventoryStructureViews() else getInventoryLocationViews()

    override fun getDataForLocationViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Km From (at)", asset.kmFromAt.toString(), { updatedValue -> asset.kmFromAt = updatedValue}),
            NearByAssetInventoryDataHolder("KM To (at)", asset.kmTo.toString(), { updatedValue -> asset.kmTo = updatedValue}),
            NearByAssetInventoryDataHolder("Section From", asset.sectionFrom.toString(), { updatedValue -> asset.sectionFrom = updatedValue}),
            NearByAssetInventoryDataHolder("Section To", asset.sectionTo.toString(), { updatedValue -> asset.sectionTo = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate From", asset.latitude.toString(), { updatedValue -> asset.latitude = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate From", asset.longitude.toString(), { updatedValue -> asset.longitude = updatedValue}),
            NearByAssetInventoryDataHolder("X-Coordinate To", asset.latitudeTo.toString(), { updatedValue -> asset.latitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Y-Coordinate To", asset.longitudeTo.toString(), { updatedValue -> asset.longitudeTo = updatedValue}),
            NearByAssetInventoryDataHolder("Altitude", asset.altitude.toString(), { updatedValue -> asset.altitude = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS From", asset.measFrom.toString(), { updatedValue -> asset.measFrom = updatedValue}),
            NearByAssetInventoryDataHolder("MEAS To", asset.measTo.toString(), { updatedValue -> asset.measTo = updatedValue}),
            NearByAssetInventoryDataHolder("Location", asset.location.toString(), { updatedValue -> asset.location = updatedValue}),
            NearByAssetInventoryDataHolder("Events", asset.event.toString(), { updatedValue -> asset.event = updatedValue}),
            NearByAssetInventoryDataHolder("Offset", asset.offSet.toString(), { updatedValue -> asset.offSet = updatedValue})
        )
    }

    override fun getDataForStructureViews(): List<NearByAssetInventoryDataHolder> {
        return listOf(
            NearByAssetInventoryDataHolder("Type", asset.type.toString(), { updatedValue -> asset.type = updatedValue}),
            NearByAssetInventoryDataHolder("Length KM(Asset)", asset.length.toString(), { updatedValue -> asset.length = updatedValue}),
            NearByAssetInventoryDataHolder("Material", asset.material.toString(), { updatedValue -> asset.material = updatedValue}),
            NearByAssetInventoryDataHolder("Approach Terminal", asset.approachTerminal.toString(), { updatedValue -> asset.approachTerminal = updatedValue}),
            NearByAssetInventoryDataHolder("Departing Terminal", asset.departingTerminal.toString(), { updatedValue -> asset.departingTerminal = updatedValue}),
            NearByAssetInventoryDataHolder("Ground Clearance", asset.groundClearance.toString(), { updatedValue -> asset.groundClearance = updatedValue}),
            NearByAssetInventoryDataHolder("Rail Heights", asset.railHeights.toString(), { updatedValue -> asset.railHeights = updatedValue}),
            NearByAssetInventoryDataHolder("Nos Of Beam", asset.noOfBeam.toString(), { updatedValue -> asset.noOfBeam = updatedValue}),
            NearByAssetInventoryDataHolder("Nos of Packer", asset.noOfPacker.toString(), { updatedValue -> asset.noOfPacker = updatedValue}),
            NearByAssetInventoryDataHolder("Nos of Post", asset.nosPost.toString(), { updatedValue -> asset.nosPost = updatedValue}),
            NearByAssetInventoryDataHolder("Post Spacing", asset.postSpacing.toString(), { updatedValue -> asset.postSpacing = updatedValue}),
            NearByAssetInventoryDataHolder("Condition", asset.condition.toString(), { updatedValue -> asset.condition = updatedValue}),
            NearByAssetInventoryDataHolder("Date Install", asset.dateInstall.toString(), { updatedValue -> asset.dateInstall = updatedValue})
        )
    }

    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}