package com.datasolutions.routelist.data.payload

object RouteListReqArgs {

    fun getArgs(): List<String> {
        return payload
    }

    val payload = mutableListOf(
        "name",
        "chainage_from",
        "chainage_to",
        "length",
        "state_code_id",
        "region_code_id",
        "road_id",
        "road_name",
        "marris_id"
    )
}