package com.datasolutions.routelist.ui

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetRouteListUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.routelist.data.payload.RouteListReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class RouteListViewModel(val getRouteListUseCase: GetRouteListUseCase): BaseViewModel() {

    private val _assetsLiveData = MutableStateFlow<ResultState<List<Asset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<Asset>>> = _assetsLiveData

    init {
        getAssets()
    }

    fun getAssets() {
        viewModelScope.launch {
            getRouteListUseCase(AssetRequest(AssetParams("road.route", "search_read", getArgs(), KwDetailArgs()))).collect {
                    result ->
                _assetsLiveData.value = result
            }
        }
    }

    private fun getArgs(): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, RouteListReqArgs.getArgs())
        return list
    }

    fun showDetail(id: Int, routeId: String, district: String, state: String, roadName: String) {
        navigationCommands.value = NavigationCommand.To(RouteListFragmentDirections.actionListToDetail(id, "maintenance.history", routeId, state, district, roadName))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}