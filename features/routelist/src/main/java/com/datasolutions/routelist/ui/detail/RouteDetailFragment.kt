package com.datasolutions.routelist.ui.detail

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.routelist.R
import com.datasolutions.routelist.BR
import com.datasolutions.routelist.databinding.FragmentRouteDetailBinding
import com.datasolutions.routelist.ui.detail.pager.RouteDetailPagerAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class RouteDetailFragment: BaseFragment<RouteDetailViewModel, FragmentRouteDetailBinding>() {

    private val args: RouteDetailFragmentArgs by navArgs()

    private lateinit var adapter: RouteDetailPagerAdapter

    override val viewModelClass: KClass<RouteDetailViewModel>
        get() = RouteDetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_route_detail
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {

                        val asset = result.data
                        viewModel.setValues(args.routeId, args.roadName, args.state, args.district, asset.chainageFrom, asset.chainageTo, asset.length, asset.agensiName, asset.agensiId, asset.marrisId, asset.phase)
                        adapter = RouteDetailPagerAdapter(
                            args.model,
                            result.data
                        )
                        if (asset.polyLines?.isNotEmpty() == true) {
                            viewModel.getPoints(asset.polyLines)
                        } else {
                            lifecycleScope.launch {
                                binding.routeDetailVP.adapter = adapter
                                binding.tabLayout.setupWithViewPager(binding.routeDetailVP)
                                viewModel.showLoading(false)
                            }
                        }

                        viewModel.showLoading(false)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }

        viewModel.points.observe(viewLifecycleOwner, {
            lifecycleScope.launch {

                adapter.setRoutePoints(it)
                binding.routeDetailVP.adapter = adapter
                binding.tabLayout.setupWithViewPager(binding.routeDetailVP)
                viewModel.showLoading(false)
            }
        })
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {

        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        viewModel.getAssetDetails(args.id)
    }
}