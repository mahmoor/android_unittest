package com.datasolutions.routelist.ui

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.routelist.R
import com.datasolutions.routelist.BR
import com.datasolutions.routelist.databinding.FragmentRouteListBinding
import com.datasolutions.routelist.ui.adapter.RouteListAdapter
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass

class RouteListFragment: BaseFragment<RouteListViewModel, FragmentRouteListBinding>(),
    RouteListAdapter.OnAssetListItemClickListener {

    private val adapter: RouteListAdapter by lazy { RouteListAdapter(this) }


    override val viewModelClass: KClass<RouteListViewModel>
        get() = RouteListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_route_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        if(result.data.isEmpty())
                            viewModel.showError("No record found.")
                        else
                            adapter.submitList(result.data)

                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }

            }
        }
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.routeRecyclerView.layoutManager = layoutManager
        binding.routeRecyclerView.adapter = adapter

    }

    override fun setTitle() {
        binding.title = "Route List"
    }

    override fun showAssetDetail(
        id: Int,
        routeId: String,
        roadName: String,
        state: String,
        district: String
    ) {
        viewModel.showDetail(id, routeId, district, state, roadName)
    }
}