package com.datasolutions.routelist.ui.detail.pager

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.viewpager.widget.PagerAdapter
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.routelist.databinding.LayoutRouteMapBinding
import com.google.android.gms.common.util.Base64Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions


class RouteDetailPagerAdapter(val model: String, val asset: Asset) :
    PagerAdapter(), OnMapReadyCallback {

    private lateinit var route: MutableList<List<LatLng>>
    private var isEdit: Boolean = false
    lateinit var inflator: LayoutInflater

    fun setRoutePoints(points: MutableList<List<LatLng>>) {
        route = points
    }

    override fun getCount(): Int {
        return 1
    }

    fun setEdit(isEdit: Boolean) {
        this.isEdit = isEdit
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as View?
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflator = LayoutInflater.from(container.context)


        val bindingMap = LayoutRouteMapBinding.inflate(inflator, container, false)
        bindingMap.title = ""
        bindingMap.mapView.onCreate(null);
        bindingMap.mapView.onResume();
        bindingMap.mapView.getMapAsync(this)
        container.addView(bindingMap.root)
        return bindingMap.root


    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Map"
    }

    override fun onMapReady(mMap: GoogleMap) {


        val markerOptions = MarkerOptions()
        asset.longitudeForMap?.let { asset.latitudeForMap?.let { it1 -> LatLng(it1, it) } }
            ?.let { markerOptions.position(it) }


        val markerOptions1 = MarkerOptions()
        asset.longitudeTo?.let {
            asset.latitudeTo?.let { it1 ->
                LatLng(
                    it1.toDouble(),
                    it.toDouble()
                )
            }
        }
            ?.let { markerOptions1.position(it) }

        mMap.addMarker(markerOptions)
        mMap.addMarker(markerOptions1)



        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(markerOptions.position, 12f))

        for (i in 0 until route.size) {
            mMap.addPolyline(PolylineOptions().addAll(route[i]).color(Color.RED))
        }
        mMap.uiSettings.isZoomControlsEnabled = true

    }

}