package com.datasolutions.routelist.di

import com.datasolutions.routelist.ui.RouteListViewModel
import com.datasolutions.routelist.ui.detail.RouteDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object RouteListModules {

    fun load() {
        loadKoinModules(routeListModules)
    }

    private val routeListModules = module {
        viewModel { RouteListViewModel(get()) }
        viewModel { RouteDetailViewModel(get()) }
    }
}