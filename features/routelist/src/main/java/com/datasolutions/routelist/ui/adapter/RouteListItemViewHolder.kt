package com.datasolutions.routelist.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.routelist.databinding.ViewHolderRouteListItemBinding


class RouteListItemViewHolder(private val binding: ViewHolderRouteListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: RouteListAdapter.OnAssetListItemClickListener? = null, asset: Asset) {
        binding.asset = asset

        binding.root.setOnClickListener {
            // var id: Int

            clickListener?.showAssetDetail(
             asset.id,
                asset.routeId.orEmpty(),
                asset.roadName.orEmpty(),
                asset.state.orEmpty(),
                asset.district.orEmpty()
            )
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): RouteListItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderRouteListItemBinding.inflate(layoutInflater, parent, false)
            return RouteListItemViewHolder(binding)
        }
    }
}