package com.datasolutions.routelist.data.payload

object RouteDetailReqArgs {

    fun getArgs(): List<String> {
        return detailPayload
    }

    val detailPayload = mutableListOf(
        "name",
        "chainage_from",
        "chainage_to",
        "length",
        "state_code_id",
        "region_code_id",
        "road_id",
        "road_name",
        "marris_id",
        "agensi_name_id",
        "agensi_id",
        "direction",
        "lane",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "remarks",
        "phase"
    )

}