package com.datasolutions.hdm4.ui

import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.core.util.NavigationCommand
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetHdm4AssetsUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.hdm4.data.payload.Hdm4AssetListReqArgs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class Hdm4ListViewModel(private val getHdm4AssetsUseCase: GetHdm4AssetsUseCase): BaseViewModel() {

    private val _assetsLiveData = MutableStateFlow<ResultState<List<Asset>>>(ResultState.loading)
    val assetsLiveData: StateFlow<ResultState<List<Asset>>> = _assetsLiveData

    init {
        getAssets()
    }

    fun getAssets() {
        viewModelScope.launch {
            getHdm4AssetsUseCase(AssetRequest(AssetParams("periodic.maintenance.work", "search_read", getArgs(), KwDetailArgs()))).collect {
                    result ->
                _assetsLiveData.value = result
            }
        }
    }

    private fun getArgs(): MutableList<List<String>> {
        val args2 = listOf<String>()
        val list = mutableListOf(args2, Hdm4AssetListReqArgs.getArgs())
        return list
    }

    fun showDetail(id: Int, routeId: String, district: String, state: String) {
        navigationCommands.value = NavigationCommand.To(Hdm4ListFragmentDirections.actionListToDetail(id, "periodic.maintenance.work", routeId, state, district))
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}