package com.datasolutions.hdm4.di

import com.datasolutions.hdm4.ui.Hdm4ListViewModel
import com.datasolutions.hdm4.ui.detail.Hdm4DetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.scope.get
import org.koin.dsl.module

object Hdm4Modules {

    fun load() {
        loadKoinModules(hdm4Modules)
    }

    private val hdm4Modules = module {
        viewModel { Hdm4ListViewModel(get()) }
        viewModel { Hdm4DetailViewModel(get()) }
    }
}