package com.datasolutions.hdm4.data.payload

object Hdm4AssetListReqArgs {

    fun getArgs(): List<String> {
        return payload
    }

    val payload = mutableListOf(
        "road_class",
        "route_id",
        "district_id",
        "state_id",
        "km_from",
        "km_to"
    )
}