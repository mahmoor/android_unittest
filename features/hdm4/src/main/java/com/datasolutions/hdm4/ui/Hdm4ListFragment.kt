package com.datasolutions.hdm4.ui

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.hdm4.R
import com.datasolutions.hdm4.BR
import com.datasolutions.hdm4.databinding.FragmentHdm4ListBinding
import com.datasolutions.hdm4.ui.adapter.Hdm4AssetListAdapter
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass

class Hdm4ListFragment : BaseFragment<Hdm4ListViewModel, FragmentHdm4ListBinding>(),
    Hdm4AssetListAdapter.OnAssetListItemClickListener {

    private val adapter: Hdm4AssetListAdapter by lazy { Hdm4AssetListAdapter(this) }

    override val viewModelClass: KClass<Hdm4ListViewModel>
        get() = Hdm4ListViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_hdm4_list
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {

        lifecycleScope.launchWhenCreated {
            viewModel.assetsLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        adapter.submitList(result.data)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }

            }
        }
    }

    override fun viewModel() = injectViewModel()

    override fun initViews() {
        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        binding.includeMain.actionBtn1.setImageResource(R.drawable.ic_search)
        binding.includeMain.actionBtn1.visibility = View.VISIBLE
        binding.includeMain.actionBtn2.setImageResource(R.drawable.ic_filter)
        binding.includeMain.actionBtn2.visibility = View.VISIBLE

        setupRecyclerView()
    }

    override fun setTitle() {
        binding.title = "HDM4"
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.hdm4RecyclerView.layoutManager = layoutManager
        binding.hdm4RecyclerView.adapter = adapter

    }

    override fun showAssetDetail(
        id: Int,
        routeId: String,
        routeName: String,
        state: String,
        district: String
    ) {
        viewModel.showDetail(id, routeId, district, state)
    }
}