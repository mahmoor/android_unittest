package com.datasolutions.hdm4.data.payload

object Hdm4AssetDetailReqArgs {

    fun getArgs(): List<String> {
        return detailPayload
    }

    val detailPayload = mutableListOf(
        "state_id",
        "district_id",
        "agensi_id",
        "route_id",
        "km_from",
        "km_to",
        "section",
        "direction",
        "lane",
        "year",
        "description",
        "financial_cost",
        "cum_cost",
        "x_coordinate",
        "y_coordinate",
        "x_coordinate_to",
        "y_coordinate_to",
        "length",
        "aadt",
        "npv_cap",
        "sub_section",
        "road_class",
        "surface_class"
    )

}