package com.datasolutions.hdm4.ui.detail

import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.datasolutions.core.extension.injectViewModel
import com.datasolutions.core.ui.base.fragment.BaseFragment
import com.datasolutions.domain.util.ResultState
import com.datasolutions.hdm4.R
import com.datasolutions.hdm4.BR
import com.datasolutions.hdm4.databinding.FragmentHdm4AssetDetailBinding
import com.datasolutions.hdm4.ui.detail.pager.Hdm4DetailPagerAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class Hdm4DetailFragment: BaseFragment<Hdm4DetailViewModel, FragmentHdm4AssetDetailBinding>() {

    private val args: Hdm4DetailFragmentArgs by navArgs()

    private lateinit var adapter: Hdm4DetailPagerAdapter

    override val viewModelClass: KClass<Hdm4DetailViewModel>
        get() = Hdm4DetailViewModel::class
    override val layoutId: Int
        get() = R.layout.fragment_hdm4_asset_detail
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.assetDetailLiveData.collect { result ->
                when (result) {
                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }
                    is ResultState.Success -> {

                        val asset = result.data

                        adapter = Hdm4DetailPagerAdapter(
                            args.model,
                            result.data
                        )
                        if (asset.polyLines?.isNotEmpty() == true) {
                            viewModel.getPoints(asset.polyLines)
                        } else {
                            lifecycleScope.launch {
                                binding.hdm4AssetDetailVP.adapter = adapter
                                binding.tabLayout.setupWithViewPager(binding.hdm4AssetDetailVP)
                                viewModel.showLoading(false)
                            }
                        }

                        viewModel.setValues(args.routeId, args.state, args.district, asset.kmFrom, asset.kmTo, asset.agensiId, asset.length)
                        viewModel.showLoading(false)
                    }
                    ResultState.loading -> viewModel.showLoading(true)
                }
            }
        }

        viewModel.points.observe(viewLifecycleOwner, {
            lifecycleScope.launch {

                adapter.setRoutePoints(it)
                binding.hdm4AssetDetailVP.adapter = adapter
                binding.tabLayout.setupWithViewPager(binding.hdm4AssetDetailVP)
                viewModel.showLoading(false)
            }
        })
    }


    override fun viewModel() = injectViewModel()

    override fun initViews() {

        binding.includeMain.backIV.setOnClickListener {
            viewModel.backButtonClick()
        }

        viewModel.getAssetDetails(args.id)
    }

}