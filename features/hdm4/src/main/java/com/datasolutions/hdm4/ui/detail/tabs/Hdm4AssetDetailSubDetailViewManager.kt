package com.datasolutions.hdm4.ui.detail.tabs

import android.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.marginTop
import androidx.databinding.ViewDataBinding
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.hdm4.ui.detail.tabs.subdetail.Hdm4AssetSubDetail

class Hdm4AssetDetailSubDetailViewManager(val isEdit: Boolean, val context: Context, val model: String, val asset: Asset) {

    val inflator = LayoutInflater.from(context)
    fun getDetailViews(): List<View> {
        return Hdm4AssetSubDetail(isEdit,false, context, asset).invoke()
    }

}