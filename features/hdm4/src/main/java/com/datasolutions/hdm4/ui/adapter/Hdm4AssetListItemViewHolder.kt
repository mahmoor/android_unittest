package com.datasolutions.hdm4.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.hdm4.databinding.ViewHolderHdm4AssetListItemBinding


class Hdm4AssetListItemViewHolder(private val binding: ViewHolderHdm4AssetListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(clickListener: Hdm4AssetListAdapter.OnAssetListItemClickListener? = null, asset: Asset) {
        binding.asset = asset

        binding.root.setOnClickListener {
            // var id: Int

            clickListener?.showAssetDetail(
             asset.id,
                asset.routeId.orEmpty(),
                asset.roadName.orEmpty(),
                asset.state.orEmpty(),
                asset.district.orEmpty()
            )
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): Hdm4AssetListItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderHdm4AssetListItemBinding.inflate(layoutInflater, parent, false)
            return Hdm4AssetListItemViewHolder(binding)
        }
    }
}