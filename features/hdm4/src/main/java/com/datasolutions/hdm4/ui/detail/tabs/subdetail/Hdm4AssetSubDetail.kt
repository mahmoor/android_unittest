package com.datasolutions.hdm4.ui.detail.tabs.subdetail

import android.content.Context
import android.view.LayoutInflater
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.hdm4.ui.detail.tabs.subdetail.dataholder.Hdm4AssetSubDetailDataObservableHolder

class Hdm4AssetSubDetail(
    val isEdit: Boolean,
    val isStructure: Boolean,
    val context: Context,
    val asset: Asset
) : Hdm4AssetSubDetailViewsBase() {

    val inflator = LayoutInflater.from(context)
    operator fun invoke() = getSubDetailViews()


    override fun getDataForSubDetailViews(): List<Hdm4AssetSubDetailDataObservableHolder> {

        return listOf(
            Hdm4AssetSubDetailDataObservableHolder(
                "AADT",
                asset.aadt,
                { updatedValue -> asset.aadt = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "NPV CAP",
                asset.npvCap,
                { updatedValue -> asset.npvCap = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Financial Cost",
                asset.financialCost,
                { updatedValue -> asset.financialCost = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "CUM Cost",
                asset.cumCost,
                { updatedValue -> asset.cumCost = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Road Class",
                asset.roadClass,
                { updatedValue -> asset.roadClass = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Surface Class",
                asset.surfaceClass,
                { updatedValue -> asset.surfaceClass = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Section",
                asset.section,
                { updatedValue -> asset.section = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "SubSection",
                asset.subSection,
                { updatedValue -> asset.subSection = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Direction",
                asset.direction,
                { updatedValue -> asset.direction = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Lane",
                asset.lane,
                { updatedValue -> asset.lane = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Year",
                asset.year,
                { updatedValue -> asset.year = updatedValue }),
            Hdm4AssetSubDetailDataObservableHolder(
                "Work Description",
                asset.description,
                { updatedValue -> asset.description = updatedValue })
        )
    }


    override fun isEditMode() = isEdit

    override fun getLayoutInflator(): LayoutInflater {
        return inflator
    }

}