package com.datasolutions.hdm4.ui.detail

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.datasolutions.core.ui.base.viewmodel.BaseViewModel
import com.datasolutions.domain.entity.request.AssetParams
import com.datasolutions.domain.entity.request.AssetRequest
import com.datasolutions.domain.entity.request.KwDetailArgs
import com.datasolutions.domain.entity.response.Asset
import com.datasolutions.domain.usecase.GetHdm4AssetDetailUseCase
import com.datasolutions.domain.util.ResultState
import com.datasolutions.hdm4.data.payload.Hdm4AssetDetailReqArgs
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class Hdm4DetailViewModel(private val getHdm4AssetDetailUseCase: GetHdm4AssetDetailUseCase): BaseViewModel() {

    private val _points = MutableLiveData<MutableList<List<LatLng>>>()
    val points: LiveData<MutableList<List<LatLng>>> = _points

    private val _assetDetailLiveData = MutableStateFlow<ResultState<Asset>>(ResultState.loading)
    val assetDetailLiveData: StateFlow<ResultState<Asset>> = _assetDetailLiveData


    val routeIdObservable = ObservableField<String>()
    val stateObservable = ObservableField<String>()
    val districtObservable = ObservableField<String>()
    val lengthObservable = ObservableField<String>()
    val kmFromObservable = ObservableField<String>()
    val kmToObservable = ObservableField<String>()
    val agensiIdObservable = ObservableField<String>()

    fun setValues(
        routeId: String,
        state: String,
        district: String,
        kmFrom: String?,
        kmTo: String?,
        agensiId: String?,
        length: String?
    ) {
        routeIdObservable.set(routeId)
        stateObservable.set(state)
        districtObservable.set(district)
        agensiIdObservable.set(agensiId)
        kmFromObservable.set(kmFrom)
        kmToObservable.set(kmTo)
        lengthObservable.set(length)
    }

    fun getAssetDetails(id: Int) {
        viewModelScope.launch {
            getHdm4AssetDetailUseCase(
                AssetRequest(
                    AssetParams(
                        "periodic.maintenance.work", "read", getArgs(id),
                        KwDetailArgs()
                    )
                )
            ).collect { result ->
                _assetDetailLiveData.value = result
            }
        }
    }


    private fun getArgs(id: Int): List<Any> {
        val args2 = mutableListOf(id)
        var list: List<Any> = arrayListOf(args2, Hdm4AssetDetailReqArgs.getArgs())
        return list
    }


    fun getPoints(points: List<String>?) {
        viewModelScope.launch {
            _points.value = getRoutePath(points)!!
        }
    }

    suspend  fun getRoutePath(points: List<String>?): MutableList<List<LatLng>> {
        val path: MutableList<List<LatLng>> = ArrayList()
        points?.forEach {
            path.add(PolyUtil.decode(it))
        }

        return path
    }

    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}