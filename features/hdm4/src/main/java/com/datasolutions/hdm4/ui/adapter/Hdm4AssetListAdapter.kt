package com.datasolutions.hdm4.ui.adapter


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.datasolutions.domain.entity.response.Asset

class Hdm4AssetListAdapter(val clickListener: OnAssetListItemClickListener): ListAdapter<Asset, Hdm4AssetListItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<Asset>() {
            override fun areItemsTheSame(oldItem: Asset, newItem: Asset): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Asset, newItem: Asset): Boolean {
               return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Hdm4AssetListItemViewHolder {
        return Hdm4AssetListItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: Hdm4AssetListItemViewHolder, position: Int) {
        holder.bind(clickListener, getItem(position))
    }

    interface OnAssetListItemClickListener {
        fun showAssetDetail(id: Int, routeId: String, routeName: String, state: String, district: String)
    }
}