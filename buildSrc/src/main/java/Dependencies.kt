object Versions {
    val compileSdkVersion = 30
    val targetSdkVersion = 30
    val minSdkVersion = 21

    val kotlin_version = "1.4.32"
    val material_version = "1.4.0"
    val constraint_version = "2.1.1"
    val appcompat_version = "1.3.1"
    val lifecycle_version = "2.4.0"
    val fragment_version = "1.1.0"
    val ktx_nav_version = "2.3.5"
    val ktx_core_version = "1.6.0"
    val timberkt_version = "1.5.1"
    val navigation_version = "2.3.5"
    val koin_version = "3.1.2"
    val retrofit_version = "2.9.0"
    val logging_version = "4.2.1"
    val map_sdk_version = "17.0.1"
    val glide_version = "4.9.0"
    val swiperefresh_layout = "1.1.0"
    val realm_adapter = "4.0.0"

    val fresco_version = "2.6.0"
    val room_version = "2.2.3"
    val coroutine_version = "1.3.9"
    val paging3_version = "3.0.0"
    val compose_version = "1.0.5"
    val maps_version = "18.0.0"
    val image_picker_version = "1.0.13"

    // Testing dependencies
    val mockito_version = "2.28.2"
    val mockito_ui_version = "2.24.5"
    val coroutines_test_version = "1.4.2"
    val arch_test_version = "2.1.0"
    val expresso_version = "3.4.0"
    val fragment_test_version = "1.3.6"
    val test_rule_version = "1.1.1"
    val ext_junit_version = "1.1.3"
}

object Libraries {
    val coreKtx =  "androidx.core:core-ktx:${Versions.ktx_core_version}"
    val kotlinStlb =  "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin_version}"
    val appCompat = "androidx.appcompat:appcompat:${Versions.appcompat_version}"
    val material = "com.google.android.material:material:${Versions.material_version}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraint_version}"
    val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.ktx_nav_version}"
    val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"

    val koin = "io.insert-koin:koin-android:${Versions.koin_version}"
    val koinCore = "io.insert-koin:koin-core:${Versions.koin_version}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    val retrofitIntercepter = "com.squareup.okhttp3:logging-interceptor:${Versions.logging_version}"

    val glide = "com.github.bumptech.glide:glide:${Versions.glide_version}"
    val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide_version}"

    val fresco = "com.facebook.fresco:fresco:${Versions.fresco_version}"

    val room = "androidx.room:room-ktx:${Versions.room_version}"
    val roomComplier = "androidx.room:room-compiler:${Versions.room_version}"
    val roomRuntime = "androidx.room:room-runtime:${Versions.room_version}"

    val paging3 = "androidx.paging:paging-runtime:${Versions.paging3_version}"
    val paging3Common = "androidx.paging:paging-common:${Versions.paging3_version}"
    val compose = "androidx.compose.ui:ui:${Versions.compose_version}"
    val composeMaterial = "androidx.compose.material:material:${Versions.compose_version}"
    val composeFoundation = "androidx.compose.foundation:foundation:${Versions.compose_version}"
    val lifeCycle = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle_version}"
    val lifeCycleVM = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_version}"

    val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine_version}"
    val composePager = "com.google.accompanist:accompanist-pager:0.20.0"
    val googleMaps = "com.google.android.gms:play-services-maps:${Versions.maps_version}"

    val imagePicker = "com.github.maayyaannkk:ImagePicker:${Versions.image_picker_version}"

}

object TestLibraries {

    val jUnit  = "junit:junit:4.12"
    val mockitio = "org.mockito:mockito-core:${Versions.mockito_version}"
    val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines_test_version}"
    val arch =  "androidx.arch.core:core-testing:${Versions.arch_test_version}"
    val jUnitExt = "androidx.test.ext:junit:${Versions.ext_junit_version}"
    val expresso = "androidx.test.espresso:espresso-core:${Versions.expresso_version}"
}